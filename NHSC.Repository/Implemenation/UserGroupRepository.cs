﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHSC.Models.Entities;
using System.Data.Entity;
using NHSC.Models.Common;
using System.Data;
using System.Data.Sql;
using MHSC.Repository;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Data.Common;

namespace NHSC.Repository
{
    public class UserGroupRepository : Repository<UserGroupModel>, IUserGroupRepository
    {
        public UserGroupRepository(DbContext _db) : base(_db)
        {

        }
        public List<UserGroupModel> GetList(CommonModel objPaging)
        {
            List<UserGroupModel> objList = new List<UserGroupModel>();
            DataSet ds;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@SearchValue", CommonFunctions.ValidateString(objPaging.searchValue));
                Parameters.Add("@OrderBy", CommonFunctions.ValidateString(objPaging.ShortColumnName));
                Parameters.Add("@Start", objPaging.Skip);
                Parameters.Add("@End", objPaging.PageSize);
                ds = GetDataSetUsingSP("sp_GetUserGroup", Parameters);

                DataTable dt = ds.Tables[0];

                if (CommonFunctions.IsValidDataTable(dt))
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        UserGroupModel objvalue = new UserGroupModel();
                        objvalue.UserGroupId = CommonFunctions.ValidIntValue(dt.Rows[i]["UserGroupId"]?.ToString() ?? "0");
                        objvalue.RowNumber = CommonFunctions.ValidlongValue(dt.Rows[i]["RowNumber"]?.ToString() ?? "0");
                        objvalue.UserGroupName = dt.Rows[i]["UserGroupName"]?.ToString() ?? "";
                        objvalue.Date = dt.Rows[i]["CreatedDate"]?.ToString() ?? "";
                        objList.Add(objvalue);
                    }
                }
                dt = ds.Tables[1];

                if (CommonFunctions.IsValidDataTable(dt))
                {
                    objPaging.TotalCount = CommonFunctions.ValidlongValue(dt.Rows[0]["TotalCount"].ToString());
                }
            }
            catch (Exception)
            {
                throw;
            }
            return objList;
        }
        public bool ExistName(UserGroupModel objUser)
        {
            bool IsExists = false;
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@UserGroupName", CommonFunctions.ValidateString(objUser.UserGroupName));
                Parameters.Add("@UserGroupId", objUser.UserGroupId);
                dt = GetDataTableUsingSP("sp_CheckUserGroupExist", Parameters);
                if (CommonFunctions.IsValidDataTable(dt))
                {
                    IsExists = true;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return IsExists;
        }
        public bool InsertData(UserGroupModel objUser, ref string strErrorMsg)
        {
            bool IsValid = true;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            SqlTransaction trans = null;
            int Value = 0;
            try
            {
                using (var con = db.Database.Connection)
                {
                    try
                    {
                        con.Open();
                        if (trans == null)
                        {
                            trans = ((SqlConnection)db.Database.Connection).BeginTransaction();
                        }
                        using (var cmd = con.CreateCommand())
                        {

                            Parameters.Add("@UserGroupId", objUser.UserGroupId);
                            Parameters.Add("@UserGroupName", CommonFunctions.ValidateString(objUser.UserGroupName));
                            Parameters.Add("@ModifyBy", objUser.ModifyBy);
                            Parameters.Add("@IsDepartmentAdmin", objUser.IsDepartmentAdmin);
                            Parameters.Add("@ModifyDate", objUser.ModifyDate);

                            cmd.CommandText = "sp_InsertUserGroup";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Transaction = trans;
                            foreach (KeyValuePair<string, object> param in Parameters)
                            {
                                DbParameter dbParameter = cmd.CreateParameter();
                                dbParameter.ParameterName = param.Key;
                                dbParameter.Value = param.Value;
                                cmd.Parameters.Add(dbParameter);
                            }
                            Value = CommonFunctions.ValidIntValue(cmd.ExecuteScalar().ToString());

                            if (objUser.RightsList != null && objUser.RightsList.Count > 0)
                            {
                                foreach (var item in objUser.RightsList)
                                {
                                    cmd.Parameters.Clear();
                                    Parameters = new Dictionary<string, object>();
                                    Parameters.Add("@RightId", item.RightId);
                                    Parameters.Add("@UserGroupId", Value);
                                    Parameters.Add("@MenuId", item.MenuId);
                                    Parameters.Add("@Add", item.Add);
                                    Parameters.Add("@Edit", item.Edit);
                                    Parameters.Add("@Delete", item.Delete);
                                    Parameters.Add("@View", item.View);
                                    Parameters.Add("@Print", item.Print);
                                    Parameters.Add("@IsShowAll", item.IsShowAll);
                                    Parameters.Add("@ModifyBy", objUser.ModifyBy);
                                    Parameters.Add("@ModifyDate", objUser.ModifyDate);
                                    cmd.CommandText = "sp_InsertRights";
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    foreach (KeyValuePair<string, object> param in Parameters)
                                    {
                                        DbParameter dbParameter = cmd.CreateParameter();
                                        dbParameter.ParameterName = param.Key;
                                        dbParameter.Value = param.Value;
                                        cmd.Parameters.Add(dbParameter);
                                    }
                                    cmd.ExecuteScalar();
                                }
                            }

                            trans.Commit();
                        }
                    }
                    catch (Exception)
                    {
                        trans.Rollback();
                        throw;
                    }
                }
            }
            catch (Exception)
            {
                
                throw;
            }
            return IsValid;
        }
        public bool DeleteData(long Id, long ModifyBy, DateTime ModifyDate)
        {
            bool IsValid = true;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@UserGroupId", Id);
                Parameters.Add("@ModifyBy", ModifyBy);
                Parameters.Add("@ModifyDate", ModifyDate);
                ExecuteSP("sp_DeleteUserGroup", Parameters);
            }
            catch (Exception)
            {
                throw;
            }
            return IsValid;
        }
        public UserGroupModel GetDetails(long Id)
        {
            UserGroupModel objvalue = new UserGroupModel();
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                if (Id > 0)
                {
                    Parameters.Add("@UserGroupId", Id);
                    dt = GetDataTableUsingSP("sp_GetUserGroupDetails", Parameters);

                    if (CommonFunctions.IsValidDataTable(dt))
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {

                            objvalue.UserGroupId = CommonFunctions.ValidIntValue(dt.Rows[i]["UserGroupId"]?.ToString() ?? "0");
                            objvalue.UserGroupName = dt.Rows[i]["UserGroupName"]?.ToString() ?? "";
                            objvalue.IsDepartmentAdmin = CommonFunctions.ValidBoolValue(dt.Rows[i]["IsDepartmentAdmin"]?.ToString() ?? "0");
                        }
                    }
                }

                objvalue.RightsList = GetRigths(Id);

            }
            catch (Exception)
            {
                throw;
            }
            return objvalue;
        }

        public List<Rights> GetRigths(long Id)
        {
            List<Rights> objList = new List<Rights>();
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters = new Dictionary<string, object>();
                Parameters.Add("@UserGroupId", Id);
                dt = GetDataTableUsingSP("sp_GetRights", Parameters);

                if (CommonFunctions.IsValidDataTable(dt))
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        Rights objRights = new Rights();
                        objRights.MenuId = CommonFunctions.ValidIntValue(dt.Rows[i]["MenuId"]?.ToString() ?? "0");
                        objRights.ParentId = CommonFunctions.ValidIntValue(dt.Rows[i]["ParentId"]?.ToString() ?? "0");
                        objRights.Sequence = CommonFunctions.ValidIntValue(dt.Rows[i]["Sequence"]?.ToString() ?? "0");
                        objRights.RightId = CommonFunctions.ValidlongValue(dt.Rows[i]["RightId"]?.ToString() ?? "0");
                        objRights.Add = CommonFunctions.ValidBoolValue(dt.Rows[i]["Add"]?.ToString() ?? "0");
                        objRights.Edit = CommonFunctions.ValidBoolValue(dt.Rows[i]["Edit"]?.ToString() ?? "0");
                        objRights.Delete = CommonFunctions.ValidBoolValue(dt.Rows[i]["Delete"]?.ToString() ?? "0");
                        objRights.Print = CommonFunctions.ValidBoolValue(dt.Rows[i]["Print"]?.ToString() ?? "0");
                        objRights.IsShowAll = CommonFunctions.ValidBoolValue(dt.Rows[i]["IsShowAll"]?.ToString() ?? "0");
                        objRights.IsShowInMenu = CommonFunctions.ValidBoolValue(dt.Rows[i]["IsShowInMenu"]?.ToString() ?? "0");
                        objRights.View = CommonFunctions.ValidBoolValue(dt.Rows[i]["View"]?.ToString() ?? "0");
                        objRights.Action = dt.Rows[i]["Action"]?.ToString() ?? "";
                        objRights.MenuName = dt.Rows[i]["MenuName"]?.ToString() ?? "";
                        objList.Add(objRights);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return objList;
        }

        public DataTable GetRights(long Id)
        {
            List<Rights> objList = new List<Rights>();
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters = new Dictionary<string, object>();
                Parameters.Add("@UserGroupId", Id);
                dt = GetDataTableUsingSP("sp_GetRights", Parameters);
            }
            catch (Exception)
            {

                throw;
            }
            return dt;
        }
        public List<SelectListItem> GetUserGroup()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                dt = GetDataTableUsingSP("sp_GetGroupList", Parameters);
                if (CommonFunctions.IsValidDataTable(dt))
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SelectListItem objValue = new SelectListItem();
                        objValue.Value = dt.Rows[i]["UserGroupId"]?.ToString() ?? "0";
                        objValue.Text = dt.Rows[i]["UserGroupName"]?.ToString() ?? "";
                        list.Add(objValue);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return list;
        }
    }
}
