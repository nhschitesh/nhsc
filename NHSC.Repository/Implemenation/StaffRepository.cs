﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHSC.Models.Entities;
using System.Data.Entity;
using NHSC.Models.Common;
using System.Data;
using System.Data.Sql;
using MHSC.Repository;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Data.Common;

namespace NHSC.Repository
{
    public class StaffRepository : Repository<StaffModel>, IStaffRepository
    {
        public StaffRepository(DbContext _db) : base(_db)
        {

        }
        public List<StaffModel> GetList(CommonModel objPaging)
        {
            List<StaffModel> objList = new List<StaffModel>();
            DataSet ds;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@SearchValue", CommonFunctions.ValidateString(objPaging.searchValue));
                Parameters.Add("@OrderBy", CommonFunctions.ValidateString(objPaging.ShortColumnName));
                Parameters.Add("@Start", objPaging.Skip);
                Parameters.Add("@End", objPaging.PageSize);
                Parameters.Add("@IsAllow", CommonFunctions.ShowAll);
                Parameters.Add("@UserId", objPaging.Id);
                ds = GetDataSetUsingSP("sp_GetStaff", Parameters);

                DataTable dt = ds.Tables[0];

                if (CommonFunctions.IsValidDataTable(dt))
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        StaffModel objvalue = new StaffModel();
                        objvalue.StaffId = CommonFunctions.ValidIntValue(dt.Rows[i]["StaffId"]?.ToString() ?? "0");
                        objvalue.RowNumber = CommonFunctions.ValidlongValue(dt.Rows[i]["RowNumber"]?.ToString() ?? "0");
                        objvalue.SkillSetName = dt.Rows[i]["SkillSetName"]?.ToString() ?? "";
                        objvalue.DepartmentName = dt.Rows[i]["DepartmentName"]?.ToString() ?? "";
                        objvalue.TitleName = dt.Rows[i]["TitleName"]?.ToString() ?? "";
                        objvalue.Surname = dt.Rows[i]["Surname"]?.ToString() ?? "";
                        objvalue.Forename = dt.Rows[i]["Forename"]?.ToString() ?? "";
                        objvalue.Date = dt.Rows[i]["CreatedDate"]?.ToString() ?? "";
                        objvalue.IsAllowEdit = CommonFunctions.ValidStringIntBoolValue(dt.Rows[i]["IsAllowEdit"]?.ToString() ?? "0");
                        objList.Add(objvalue);
                    }
                }
                dt = ds.Tables[1];

                if (CommonFunctions.IsValidDataTable(dt))
                {
                    objPaging.TotalCount = CommonFunctions.ValidlongValue(dt.Rows[0]["TotalCount"].ToString());
                }
            }
            catch (Exception)
            {
                throw;
            }
            return objList;
        }
        public bool ExistName(StaffModel objValue)
        {
            bool IsExists = false;
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                //Parameters.Add("@SiteName", CommonFunctions.ValidateString(objValue.SiteName));
                Parameters.Add("@StaffId", objValue.StaffId);
                dt = GetDataTableUsingSP("sp_CheckSiteExist", Parameters);
                if (CommonFunctions.IsValidDataTable(dt))
                {
                    IsExists = true;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return IsExists;
        }
        public bool InsertData(StaffModel objValue, ref string strErrorMsg)
        {
            bool IsValid = true;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@StaffId", objValue.StaffId);
                Parameters.Add("@Title", objValue.TitleId);
                Parameters.Add("@SkillSetIds", CommonFunctions.ValidateString(objValue.SkillSetIds));
                Parameters.Add("@DepartmentIds", CommonFunctions.ValidateString(objValue.DepartmentIds));
                Parameters.Add("@Forename", CommonFunctions.ValidateString(objValue.Forename));
                Parameters.Add("@Surname", CommonFunctions.ValidateString(objValue.Surname));
                Parameters.Add("@Extension", CommonFunctions.ValidateString(objValue.Extension));
                Parameters.Add("@Bleep", CommonFunctions.ValidateString(objValue.Bleep));
                Parameters.Add("@Pager", CommonFunctions.ValidateString(objValue.Pager));
                Parameters.Add("@Home", CommonFunctions.ValidateString(objValue.Home));
                Parameters.Add("@Mobile", CommonFunctions.ValidateString(objValue.Mobile));
                Parameters.Add("@Email", CommonFunctions.ValidateString(objValue.Email));
                Parameters.Add("@AltExtn", CommonFunctions.ValidateString(objValue.AltExtn));
                Parameters.Add("@AltBleep", CommonFunctions.ValidateString(objValue.AltBleep));
                Parameters.Add("@AltPager", CommonFunctions.ValidateString(objValue.AltPager));
                Parameters.Add("@AltMobile", CommonFunctions.ValidateString(objValue.AltMobile));
                Parameters.Add("@OtherPrivate", CommonFunctions.ValidateString(objValue.OtherPrivate));
                Parameters.Add("@AltEmail", CommonFunctions.ValidateString(objValue.AltEmail));
                Parameters.Add("@ContactOrder", CommonFunctions.ValidateString(objValue.ContactOrder));
                Parameters.Add("@ModifyBy", objValue.ModifyBy);
                Parameters.Add("@ModifyDate", objValue.ModifyDate);
                int Value = ExecuteSP("sp_InsertStaff", Parameters);
            }
            catch (Exception)
            {
                throw;
            }
            return IsValid;
        }
        public bool AssignBukDepSkillData(StaffModel objValue, ref string strErrorMsg)
        {
            bool IsValid = true;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@SkillSetIds", CommonFunctions.ValidateString(objValue.SkillSetIds));
                Parameters.Add("@DepartmentIds", CommonFunctions.ValidateString(objValue.DepartmentIds));
                Parameters.Add("@StaffIds", CommonFunctions.ValidateString(objValue.StaffIds));
                Parameters.Add("@ModifyBy", objValue.ModifyBy);
                Parameters.Add("@ModifyDate", objValue.ModifyDate);
                int Value = ExecuteSP("sp_AssignBulkDepSkill", Parameters);
            }
            catch (Exception)
            {
                throw;
            }
            return IsValid;
        }
        public bool DeleteData(long Id, long ModifyBy, DateTime ModifyDate)
        {
            bool IsValid = true;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@StaffId", Id);
                Parameters.Add("@ModifyBy", ModifyBy);
                Parameters.Add("@ModifyDate", ModifyDate);
                ExecuteSP("sp_DeleteStaff", Parameters);
            }
            catch (Exception)
            {
                throw;
            }
            return IsValid;
        }
        public StaffModel GetDetails(long Id)
        {
            StaffModel objvalue = new StaffModel();
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                if (Id > 0)
                {
                    Parameters.Add("@StaffId", Id);
                    dt = GetDataTableUsingSP("sp_GetStaffDetails", Parameters);

                    if (CommonFunctions.IsValidDataTable(dt))
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            objvalue.StaffId = CommonFunctions.ValidlongValue(dt.Rows[i]["StaffId"]?.ToString() ?? "0");
                            objvalue.Title = CommonFunctions.ValidIntValue(dt.Rows[i]["Title"]?.ToString() ?? "0");
                            objvalue.TitleId = objvalue.Title;
                            objvalue.SkillSetIds = dt.Rows[i]["SkillSetIds"]?.ToString() ?? "";
                            objvalue.DepartmentIds = dt.Rows[i]["DepartmentIds"]?.ToString() ?? "";
                            objvalue.Surname = dt.Rows[i]["Surname"]?.ToString() ?? "";
                            objvalue.Forename = dt.Rows[i]["Forename"]?.ToString() ?? "";
                            objvalue.Extension = dt.Rows[i]["Extension"]?.ToString() ?? "";
                            objvalue.Bleep = dt.Rows[i]["Bleep"]?.ToString() ?? "";
                            objvalue.Pager = dt.Rows[i]["Pager"]?.ToString() ?? "";
                            objvalue.Home = dt.Rows[i]["Home"]?.ToString() ?? "";
                            objvalue.Mobile = dt.Rows[i]["Mobile"]?.ToString() ?? "";
                            objvalue.Email = dt.Rows[i]["Email"]?.ToString() ?? "";
                            objvalue.AltExtn = dt.Rows[i]["AltExtn"]?.ToString() ?? "";
                            objvalue.AltBleep = dt.Rows[i]["AltBleep"]?.ToString() ?? "";
                            objvalue.AltPager = dt.Rows[i]["AltPager"]?.ToString() ?? "";
                            objvalue.AltMobile = dt.Rows[i]["AltMobile"]?.ToString() ?? "";
                            objvalue.OtherPrivate = dt.Rows[i]["OtherPrivate"]?.ToString() ?? "";
                            objvalue.AltEmail = dt.Rows[i]["AltEmail"]?.ToString() ?? "";
                            objvalue.ContactOrder = dt.Rows[i]["ContactOrder"]?.ToString() ?? "";
                            objvalue.SkillSetList = objvalue.SkillSetIds.Split(',');
                            objvalue.DepartmentList = objvalue.DepartmentIds.Split(',');
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return objvalue;
        }
        public StaffModel GetStaffOrder(long Id)
        {
            StaffModel objStaff = new StaffModel();
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                if (Id > 0)
                {
                    Parameters.Add("@StaffId", Id);
                    dt = GetDataTableUsingSP("sp_GetStaffOrder", Parameters);

                    if (CommonFunctions.IsValidDataTable(dt))
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            StaffOrderModel objvalue = new StaffOrderModel();
                            objvalue.StaffId = CommonFunctions.ValidlongValue(dt.Rows[i]["StaffId"]?.ToString() ?? "0");
                            objvalue.StaffOrderId = CommonFunctions.ValidlongValue(dt.Rows[i]["StaffOrderId"]?.ToString() ?? "0");
                            objvalue.OrderSeq = CommonFunctions.ValidIntValue(dt.Rows[i]["OrderSeq"]?.ToString() ?? "0");
                            objvalue.IsShowColumn = CommonFunctions.ValidBoolValue(dt.Rows[i]["IsShowColumn"]?.ToString() ?? "0");
                            objvalue.ColumnName = dt.Rows[i]["ColumnName"]?.ToString() ?? "";
                            objvalue.ColumnKey = dt.Rows[i]["ColumnKey"]?.ToString() ?? "";
                            objStaff.StaffOrderList.Add(objvalue);
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return objStaff;
        }
        public List<SelectListItem> GetStaff(long UserId)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@IsAllow", CommonFunctions.ShowAll);
                Parameters.Add("@UserId", UserId);
                dt = GetDataTableUsingSP("sp_GetStaffList", Parameters);
                if (CommonFunctions.IsValidDataTable(dt))
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SelectListItem objValue = new SelectListItem();
                        objValue.Value = dt.Rows[i]["StaffId"]?.ToString() ?? "0";
                        objValue.Text = dt.Rows[i]["StaffName"]?.ToString() ?? "";
                        list.Add(objValue);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return list;
        }

        public DataTable GetStaffDDL()
        {
            System.Web.UI.WebControls.DropDownList ddl = new System.Web.UI.WebControls.DropDownList();
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
            return    dt = GetDataTableUsingSP("sp_GetStaffNameList", Parameters);
                //if (CommonFunctions.IsValidDataTable(dt))
                //{
                //    for (int i = 0; i < dt.Rows.Count; i++)
                //    {
                //        SelectListItem objValue = new SelectListItem();
                //        objValue.Value = dt.Rows[i]["StaffId"]?.ToString() ?? "0";
                //        objValue.Text = dt.Rows[i]["StaffName"]?.ToString() ?? "";
                //        ddl.Items.Add(objValue.Text);
                //    }
                //}
            }
            catch (Exception)
            {
                throw;
            }
            //return ddl;
        }
        public bool InsertStaffOrderData(StaffModel objStaff)
        {
            bool IsValid = true;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            SqlTransaction trans = null;
            try
            {
                using (var con = db.Database.Connection)
                {
                    try
                    {
                        con.Open();
                        if (trans == null)
                        {
                            trans = ((SqlConnection)db.Database.Connection).BeginTransaction();
                        }
                        using (var cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trans;
                            if (objStaff.StaffOrderList != null && objStaff.StaffOrderList.Count > 0)
                            {
                                foreach (var item in objStaff.StaffOrderList)
                                {
                                    cmd.Parameters.Clear();
                                    Parameters = new Dictionary<string, object>();
                                    Parameters.Add("@StaffId", objStaff.StaffId);
                                    Parameters.Add("@StaffOrderId", item.StaffOrderId);
                                    Parameters.Add("@ColumnName", item.ColumnName);
                                    Parameters.Add("@ColumnKey", item.ColumnKey);
                                    Parameters.Add("@OrderSeq", item.OrderSeq);
                                    Parameters.Add("@IsShowColumn", item.IsShowColumn);
                                    Parameters.Add("@ModifyBy", objStaff.ModifyBy);
                                    Parameters.Add("@ModifyDate", objStaff.ModifyDate);
                                    cmd.CommandText = "sp_InsertStaffOrder";
                                    cmd.CommandType = CommandType.StoredProcedure;

                                    foreach (KeyValuePair<string, object> param in Parameters)
                                    {
                                        DbParameter dbParameter = cmd.CreateParameter();
                                        dbParameter.ParameterName = param.Key;
                                        dbParameter.Value = param.Value;
                                        cmd.Parameters.Add(dbParameter);
                                    }
                                    cmd.ExecuteScalar();
                                }
                            }

                            trans.Commit();
                        }
                    }
                    catch (Exception)
                    {
                        trans.Rollback();
                        throw;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return IsValid;
        }
    }
}
