﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHSC.Models.Entities;
using System.Data.Entity;
using NHSC.Models.Common;
using System.Data;
using System.Data.Sql;
using MHSC.Repository;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Data.Common;

namespace NHSC.Repository
{
    public class SitesRepository : Repository<SitesModel>, ISitesRepository
    {
        public SitesRepository(DbContext _db) : base(_db)
        {

        }
        public List<SitesModel> GetList(CommonModel objPaging)
        {
            List<SitesModel> objList = new List<SitesModel>();
            DataSet ds;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@SearchValue", CommonFunctions.ValidateString(objPaging.searchValue));
                Parameters.Add("@OrderBy", CommonFunctions.ValidateString(objPaging.ShortColumnName));
                Parameters.Add("@Start", objPaging.Skip);
                Parameters.Add("@End", objPaging.PageSize);
                ds = GetDataSetUsingSP("sp_GetSites", Parameters);

                DataTable dt = ds.Tables[0];

                if (CommonFunctions.IsValidDataTable(dt))
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SitesModel objvalue = new SitesModel();
                        objvalue.SiteId = CommonFunctions.ValidIntValue(dt.Rows[i]["SiteId"]?.ToString() ?? "0");
                        objvalue.RowNumber = CommonFunctions.ValidlongValue(dt.Rows[i]["RowNumber"]?.ToString() ?? "0");
                        objvalue.SiteName = dt.Rows[i]["SiteName"]?.ToString() ?? "";
                        objvalue.Date = dt.Rows[i]["CreatedDate"]?.ToString() ?? "";
                        objList.Add(objvalue);
                    }
                }
                dt = ds.Tables[1];

                if (CommonFunctions.IsValidDataTable(dt))
                {
                    objPaging.TotalCount = CommonFunctions.ValidlongValue(dt.Rows[0]["TotalCount"].ToString());
                }
            }
            catch (Exception)
            {
                throw;
            }
            return objList;
        }
        public bool ExistName(SitesModel objValue)
        {
            bool IsExists = false;
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@SiteName", CommonFunctions.ValidateString(objValue.SiteName));
                Parameters.Add("@SiteId", objValue.SiteId);
                dt = GetDataTableUsingSP("sp_CheckSiteExist", Parameters);
                if (CommonFunctions.IsValidDataTable(dt))
                {
                    IsExists = true;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return IsExists;
        }
        public bool InsertData(SitesModel objValue, ref string strErrorMsg)
        {
            bool IsValid = true;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@SiteId", objValue.SiteId);
                Parameters.Add("@SiteName", CommonFunctions.ValidateString(objValue.SiteName));
                Parameters.Add("@ModifyBy", objValue.ModifyBy);
                Parameters.Add("@ModifyDate", objValue.ModifyDate);
                int Value = ExecuteSP("sp_InsertSites", Parameters);
            }
            catch (Exception)
            {
                throw;
            }
            return IsValid;
        }
        public bool DeleteData(long Id, long ModifyBy, DateTime ModifyDate)
        {
            bool IsValid = true;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@SiteId", Id);
                Parameters.Add("@ModifyBy", ModifyBy);
                Parameters.Add("@ModifyDate", ModifyDate);
                ExecuteSP("sp_DeleteSites", Parameters);
            }
            catch (Exception)
            {
                throw;
            }
            return IsValid;
        }
        public SitesModel GetDetails(long Id)
        {
            SitesModel objvalue = new SitesModel();
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                if (Id > 0)
                {
                    Parameters.Add("@SiteId", Id);
                    dt = GetDataTableUsingSP("sp_GetSitesDetails", Parameters);

                    if (CommonFunctions.IsValidDataTable(dt))
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            objvalue.SiteId = CommonFunctions.ValidIntValue(dt.Rows[i]["SiteId"]?.ToString() ?? "0");
                            objvalue.SiteName = dt.Rows[i]["SiteName"]?.ToString() ?? "";
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return objvalue;
        }

        public List<SelectListItem> GetSites()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                dt = GetDataTableUsingSP("sp_GetSitesList", Parameters);
                if (CommonFunctions.IsValidDataTable(dt))
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SelectListItem objValue = new SelectListItem();
                        objValue.Value = dt.Rows[i]["SiteId"]?.ToString() ?? "0";
                        objValue.Text = dt.Rows[i]["SiteName"]?.ToString() ?? "";
                        list.Add(objValue);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return list;
        }
    }
}
