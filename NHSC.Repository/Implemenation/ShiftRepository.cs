﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHSC.Models.Entities;
using System.Data.Entity;
using NHSC.Models.Common;
using System.Data;
using System.Data.Sql;
using MHSC.Repository;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Data.Common;

namespace NHSC.Repository
{
    public class ShiftRepository : Repository<ShiftModel>, IShiftRepository
    {
        public ShiftRepository(DbContext _db) : base(_db)
        {

        }
        public List<ShiftModel> GetList(CommonModel objPaging)
        {
            List<ShiftModel> objList = new List<ShiftModel>();
            DataSet ds;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@SearchValue", CommonFunctions.ValidateString(objPaging.searchValue));
                Parameters.Add("@OrderBy", CommonFunctions.ValidateString(objPaging.ShortColumnName));
                Parameters.Add("@Start", objPaging.Skip);
                Parameters.Add("@End", objPaging.PageSize);
                Parameters.Add("@IsAllow", CommonFunctions.ShowAll);
                Parameters.Add("@UserId", objPaging.Id);
                ds = GetDataSetUsingSP("sp_GetShift", Parameters);

                DataTable dt = ds.Tables[0];

                if (CommonFunctions.IsValidDataTable(dt))
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        ShiftModel objvalue = new ShiftModel();
                        objvalue.ShiftId = CommonFunctions.ValidIntValue(dt.Rows[i]["ShiftId"]?.ToString() ?? "0");
                        objvalue.RowNumber = CommonFunctions.ValidlongValue(dt.Rows[i]["RowNumber"]?.ToString() ?? "0");
                        objvalue.Date = dt.Rows[i]["CreatedDate"]?.ToString() ?? "";
                        objvalue.ShiftName = dt.Rows[i]["ShiftName"]?.ToString() ?? "";
                        objvalue.StartTimes = dt.Rows[i]["StartTime"]?.ToString() ?? "";
                        objvalue.EndTimes = dt.Rows[i]["EndTime"]?.ToString() ?? "";
                        objvalue.Active = CommonFunctions.ValidBoolValue(dt.Rows[i]["Active"]?.ToString() ?? "0");
                        objvalue.MustBeFilled = CommonFunctions.ValidBoolValue(dt.Rows[i]["MustBeFilled"]?.ToString() ?? "0");
                        objvalue.Surname = dt.Rows[i]["Surname"]?.ToString() ?? "";
                        if (string.IsNullOrEmpty(objvalue.Surname.Trim()))
                        {
                            objvalue.Surname = "N/A";
                        }
                        objvalue.ColorName = dt.Rows[i]["ColorName"]?.ToString() ?? "";
                        objvalue.SkillSetName = dt.Rows[i]["SkillSetName"]?.ToString() ?? "";
                        objvalue.Set24Hour = CommonFunctions.ValidBoolValue(dt.Rows[i]["Set24Hour"]?.ToString() ?? "0");
                        objvalue.IsAllowEdit = CommonFunctions.ValidStringIntBoolValue(dt.Rows[i]["IsAllowEdit"]?.ToString() ?? "0");
                        objList.Add(objvalue);
                    }
                }
                dt = ds.Tables[1];

                if (CommonFunctions.IsValidDataTable(dt))
                {
                    objPaging.TotalCount = CommonFunctions.ValidlongValue(dt.Rows[0]["TotalCount"].ToString());
                }
            }
            catch (Exception)
            {
                throw;
            }
            return objList;
        }
        public bool ExistStaffInSameShif(ShiftModel objValue)
        {
            bool IsExists = false;
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@StartTime", objValue.StartTime.ToString(AppConstants.TimeFormat));
                Parameters.Add("@EndTime", objValue.EndTime.ToString(AppConstants.TimeFormat));
                Parameters.Add("@ShiftId", objValue.ShiftId);
                Parameters.Add("@StaffId", objValue.StaffId);
                Parameters.Add("@Set24Hour", objValue.Set24Hour);
                dt = GetDataTableUsingSP("sp_CheckStaffExistsSameShift", Parameters);
                if (CommonFunctions.IsValidDataTable(dt))
                {
                    IsExists = true;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return IsExists;
        }
        public bool InsertData(ShiftModel objValue, ref string strErrorMsg)
        {
            bool IsValid = true;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@ShiftId", objValue.ShiftId);
                Parameters.Add("@ShiftName", CommonFunctions.ValidateString(objValue.ShiftName));
                Parameters.Add("@SkillSetId", objValue.SkillSetId);
                if (objValue.Set24Hour)
                {
                    Parameters.Add("@EndTime", "23:59:59.9000000");
                }
                else
                {

                    Parameters.Add("@EndTime", objValue.EndTime);
                }
                Parameters.Add("@StartTime", objValue.StartTime);
                Parameters.Add("@Active", objValue.ActiveStatus);
                Parameters.Add("@MustBeFilled", objValue.MustBeFilledStatus);
                Parameters.Add("@ColorId", objValue.ColorId);
                Parameters.Add("@StaffId", objValue.StaffId);
                Parameters.Add("@ModifyBy", objValue.ModifyBy);
                Parameters.Add("@ModifyDate", objValue.ModifyDate);
                Parameters.Add("@Set24Hour", objValue.Set24Hour);
                Parameters.Add("@DepartmentIds", CommonFunctions.ValidateString(objValue.DepartmentIds));

                int Value = ExecuteSP("sp_InsertShift", Parameters);
            }
            catch (Exception)
            {
                throw;
            }
            return IsValid;
        }

        public bool InsertDataShiftOrder(RotaShiftOrderModel objStaff, ref string strErrorMsg)
        {
            bool IsValid = true;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            SqlTransaction trans = null;
            try
            {
                using (var con = db.Database.Connection)
                {
                    try
                    {
                        con.Open();
                        if (trans == null)
                        {
                            trans = ((SqlConnection)db.Database.Connection).BeginTransaction();
                        }
                        using (var cmd = con.CreateCommand())
                        {
                            cmd.Transaction = trans;
                            if (objStaff.ShiftOrderList != null && objStaff.ShiftOrderList.Count > 0)
                            {
                                foreach (var item in objStaff.ShiftOrderList)
                                {
                                    cmd.Parameters.Clear();
                                    Parameters = new Dictionary<string, object>();
                                    Parameters.Add("@ShiftId", item.ShiftId);
                                    Parameters.Add("@ShiftOrderId", item.ShiftOrderId);
                                    Parameters.Add("@RotaTemplateId", objStaff.RotaTemplateId);
                                    Parameters.Add("@OrderSeq", item.OrderSeq);
                                    Parameters.Add("@ModifyBy", objStaff.ModifyBy);
                                    Parameters.Add("@ModifyDate", objStaff.ModifyDate);
                                    cmd.CommandText = "sp_InsertShiftOrder";
                                    cmd.CommandType = CommandType.StoredProcedure;

                                    foreach (KeyValuePair<string, object> param in Parameters)
                                    {
                                        DbParameter dbParameter = cmd.CreateParameter();
                                        dbParameter.ParameterName = param.Key;
                                        dbParameter.Value = param.Value;
                                        cmd.Parameters.Add(dbParameter);
                                    }
                                    cmd.ExecuteScalar();
                                }
                            }

                            trans.Commit();
                        }
                    }
                    catch (Exception)
                    {
                        trans.Rollback();
                        throw;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return IsValid;
        }
        public bool DeleteData(long Id, long ModifyBy, DateTime ModifyDate)
        {
            bool IsValid = true;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@ShiftId", Id);
                Parameters.Add("@ModifyBy", ModifyBy);
                Parameters.Add("@ModifyDate", ModifyDate);
                ExecuteSP("sp_DeleteShift", Parameters);
            }
            catch (Exception)
            {
                throw;
            }
            return IsValid;
        }
        public ShiftModel GetDetails(long Id)
        {
            ShiftModel objvalue = new ShiftModel();
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                if (Id > 0)
                {
                    Parameters.Add("@ShiftId", Id);
                    dt = GetDataTableUsingSP("sp_GetShiftDetails", Parameters);

                    if (CommonFunctions.IsValidDataTable(dt))
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            objvalue.ShiftId = CommonFunctions.ValidlongValue(dt.Rows[i]["ShiftId"]?.ToString() ?? "0");
                            objvalue.SkillSetId = CommonFunctions.ValidIntValue(dt.Rows[i]["SkillSetId"]?.ToString() ?? "0");
                            objvalue.ColorId = CommonFunctions.ValidIntValue(dt.Rows[i]["ColorId"]?.ToString() ?? "0");
                            objvalue.StaffId = CommonFunctions.ValidlongValue(dt.Rows[i]["StaffId"]?.ToString() ?? "0");
                            objvalue.ShiftName = dt.Rows[i]["ShiftName"]?.ToString() ?? "";
                            objvalue.StartTime = Convert.ToDateTime(dt.Rows[i]["StartTime"]?.ToString() ?? "");
                            objvalue.EndTime = Convert.ToDateTime(dt.Rows[i]["EndTime"]?.ToString() ?? "");
                            objvalue.StartTimes = Convert.ToDateTime(dt.Rows[i]["StartTime"]?.ToString() ?? "").ToString(AppConstants.TimeFormat);
                            objvalue.EndTimes = Convert.ToDateTime(dt.Rows[i]["EndTime"]?.ToString() ?? "").ToString(AppConstants.TimeFormat);
                            objvalue.Active = CommonFunctions.ValidBoolValue(dt.Rows[i]["Active"]?.ToString() ?? "0");
                            objvalue.MustBeFilled = CommonFunctions.ValidBoolValue(dt.Rows[i]["MustBeFilled"]?.ToString() ?? "0");
                            objvalue.ActiveStatus = CommonFunctions.ValidIntBoolValue(dt.Rows[i]["Active"]?.ToString() ?? "0");
                            objvalue.MustBeFilledStatus = CommonFunctions.ValidIntBoolValue(dt.Rows[i]["MustBeFilled"]?.ToString() ?? "0");
                            objvalue.Set24Hour = CommonFunctions.ValidBoolValue(dt.Rows[i]["Set24Hour"]?.ToString() ?? "0");
                            objvalue.ColorName = dt.Rows[i]["ColorName"]?.ToString() ?? "";
                            objvalue.Date = dt.Rows[i]["CreatedDate"]?.ToString() ?? "";
                            objvalue.DepartmentIds = dt.Rows[i]["DepartmentIds"]?.ToString() ?? "";
                            objvalue.DepartmentList = objvalue.DepartmentIds.Split(',');
                        }
                    }
                }
                else
                {
                    objvalue.StartTimes = DateTime.Now.ToString(AppConstants.TimeFormat);
                    objvalue.EndTimes = DateTime.Now.ToString(AppConstants.TimeFormat);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return objvalue;
        }
        public List<SelectListItem> GetShift(long Id)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@RotaTemplateId", Id);
                dt = GetDataTableUsingSP("sp_GetShiftList", Parameters);
                if (CommonFunctions.IsValidDataTable(dt))
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SelectListItem objValue = new SelectListItem();
                        objValue.Value = dt.Rows[i]["ShiftId"]?.ToString() ?? "0";
                        objValue.Text = dt.Rows[i]["ShiftName"]?.ToString() ?? "";
                        list.Add(objValue);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return list;
        }

        public List<RotaShiftOrderModel> GetShiftByRota(long RoataTemplateId)
        {
            List<RotaShiftOrderModel> list = new List<RotaShiftOrderModel>();
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@RotaTemplateId", RoataTemplateId);
                dt = GetDataTableUsingSP("sp_GetShiftByRota", Parameters);
                if (CommonFunctions.IsValidDataTable(dt))
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        RotaShiftOrderModel objValue = new RotaShiftOrderModel();
                        objValue.ShiftName = dt.Rows[i]["ShiftName"]?.ToString() ?? "0";
                        objValue.ShiftId = Convert.ToInt32(dt.Rows[i]["ShiftId"]?.ToString() ?? "0");
                        objValue.ShiftOrderId = Convert.ToInt32(dt.Rows[i]["ShiftOrderId"]?.ToString() ?? "0");
                        objValue.OrderSeq = Convert.ToInt32(dt.Rows[i]["OrderSeq"]?.ToString() ?? "0");
                        list.Add(objValue);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return list;
        }
    }
}
