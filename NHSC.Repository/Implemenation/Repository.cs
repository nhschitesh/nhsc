﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using NHSC.Models.Common;
using NHSC.Repository;

namespace MHSC.Repository
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly DbContext db;

        public Repository(DbContext _db)
        {
            db = _db;
        }
        public TEntity Add(TEntity entity, bool getEntity)
        {
            try
            {
                return db.Set<TEntity>().Add(entity);
            }
            catch (Exception) { throw; }
        }

        public IEnumerable<TEntity> AddRange(IEnumerable<TEntity> entities, bool getEntity)
        {
            try
            {
                return db.Set<TEntity>().AddRange(entities);
            }
            catch (Exception) { throw; }
        }

        public void Add(TEntity entity)
        {
            try
            {
                db.Set<TEntity>().Add(entity);
            }
            catch (Exception) { throw; }
        }

        public void AddRange(IEnumerable<TEntity> entities)
        {
            try
            {
                db.Set<TEntity>().AddRange(entities);
            }
            catch (Exception) { throw; }
        }

        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            try
            {
                return db.Set<TEntity>().Where(predicate);
            }
            catch (Exception) { throw; }
        }

        public TEntity Get(object Id)
        {
            try
            {
                return db.Set<TEntity>().Find(Id);
            }
            catch (Exception) { throw; }
        }

        public IEnumerable<TEntity> GetAll()
        {
            try
            {
                return db.Set<TEntity>().ToList();
            }
            catch (Exception) { throw; }
        }

        public void Remove(object Id)
        {
            try
            {
                TEntity entity = db.Set<TEntity>().Find(Id);
                db.Set<TEntity>().Remove(entity);
            }
            catch (Exception) { throw; }
        }

        public void Remove(TEntity entity)
        {
            try
            {
                db.Set<TEntity>().Remove(entity);
            }
            catch (Exception) { throw; }
        }

        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            try
            {
                db.Set<TEntity>().RemoveRange(entities);
            }
            catch (Exception) { throw; }

        }

        public void Update(TEntity entity)
        {
            try
            {
                db.Entry<TEntity>(entity).State = EntityState.Modified;
            }
            catch (Exception) { throw; }
        }

        public DataTable GetDataTableUsingSP(string StoredProcedureName, Dictionary<string, object> Parameters)
        {
            try
            {
                DataTable dtData = new DataTable();
                using (var cmd = db.Database.Connection.CreateCommand())
                {
                    cmd.CommandText = StoredProcedureName;
                    cmd.CommandType = CommandType.StoredProcedure;

                    foreach (KeyValuePair<string, object> param in Parameters)
                    {
                        DbParameter dbParameter = cmd.CreateParameter();
                        dbParameter.ParameterName = param.Key;
                        dbParameter.Value = param.Value;
                        cmd.Parameters.Add(dbParameter);
                    }

                    using (DbDataAdapter adapter = new SqlDataAdapter())
                    {
                        adapter.SelectCommand = cmd;
                        adapter.Fill(dtData);
                    }
                }
                return dtData;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataSet GetDataSetUsingSP(string StoredProcedureName, Dictionary<string, object> Parameters)
        {
            try
            {
                DataSet dsData = new DataSet();
                using (var cmd = db.Database.Connection.CreateCommand())
                {
                    cmd.CommandText = StoredProcedureName;
                    cmd.CommandType = CommandType.StoredProcedure;

                    foreach (KeyValuePair<string, object> param in Parameters)
                    {
                        DbParameter dbParameter = cmd.CreateParameter();
                        dbParameter.ParameterName = param.Key;
                        dbParameter.Value = param.Value;
                        cmd.Parameters.Add(dbParameter);
                    }

                    using (DbDataAdapter adapter = new SqlDataAdapter())
                    {
                        adapter.SelectCommand = cmd;
                        adapter.Fill(dsData);
                    }
                }
                return dsData;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetDataTable(string pstrQuery)
        {
            try
            {
                DataTable dtDataTable = new DataTable();
                using (var cmd = db.Database.Connection.CreateCommand())
                {
                    cmd.CommandText = pstrQuery;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandTimeout = 0;
                    using (DbDataAdapter adapter = new SqlDataAdapter())
                    {
                        adapter.SelectCommand = cmd;
                        adapter.Fill(dtDataTable);
                    }
                }
                return dtDataTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public int ExecuteSP(string StoredProcedureName, Dictionary<string, object> Parameters)
        //{
        //    try
        //    {
               
        //        DataTable dtData = new DataTable();
        //        using (var con = db.Database.Connection)
        //        {
        //            if (con.State == ConnectionState.Open) con.Close();
        //            con.Open();
        //            using (var cmd = con.CreateCommand())
        //            {
        //                cmd.CommandText = StoredProcedureName;
        //                cmd.CommandType = CommandType.StoredProcedure;

        //                foreach (KeyValuePair<string, object> param in Parameters)
        //                {
        //                    DbParameter dbParameter = cmd.CreateParameter();
        //                    dbParameter.ParameterName = param.Key;
        //                    dbParameter.Value = param.Value;
        //                    cmd.Parameters.Add(dbParameter);
        //                }
        //                return cmd.ExecuteNonQuery();
        //            }
                    
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}

        public int ExecuteSP(string StoredProcedureName, Dictionary<string, object> Parameters)
        {
            SqlConnection conn = null;
            SqlCommand cmd = null;
            SqlTransaction trans = null;
            int id = 0;
            try
            {
                conn = new SqlConnection(CommonFunctions.ConnectionString);
                conn.Open();
                trans = ((SqlConnection)conn).BeginTransaction();

                cmd = new SqlCommand(StoredProcedureName, conn);
                cmd.CommandType= CommandType.StoredProcedure;
                cmd.Connection = ((SqlConnection)conn);
                cmd.Transaction = trans;

                foreach (KeyValuePair<string, object> param in Parameters)
                {
                    DbParameter dbParameter = cmd.CreateParameter();
                    dbParameter.ParameterName = param.Key;
                    dbParameter.Value = param.Value;
                    cmd.Parameters.Add(dbParameter);
                }
                 id = cmd.ExecuteNonQuery();
                trans.Commit();
            }
            catch (Exception)
            {
                trans.Rollback();
                throw;
            }
            finally
            {
                if (conn.State == ConnectionState.Open) conn.Close();
            }
            return id;
        }
        public long ExecuteSPScalar(string StoredProcedureName, Dictionary<string, object> Parameters)
        {
            try
            {
                DataTable dtData = new DataTable();

                using (var con = db.Database.Connection)
                {
                    con.Open();
                  
                    using (var cmd = con.CreateCommand())
                    {
                        cmd.CommandText = StoredProcedureName;
                        cmd.CommandType = CommandType.StoredProcedure;
                        foreach (KeyValuePair<string, object> param in Parameters)
                        {
                            DbParameter dbParameter = cmd.CreateParameter();
                            dbParameter.ParameterName = param.Key;
                            dbParameter.Value = param.Value;
                            cmd.Parameters.Add(dbParameter);
                        }
                        return CommonFunctions.ValidlongValue(cmd.ExecuteScalar().ToString());
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}