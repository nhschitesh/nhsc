﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHSC.Models.Entities;
using System.Data.Entity;
using NHSC.Models.Common;
using System.Data;
using System.Data.Sql;
using MHSC.Repository;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Data.Common;
using System.Globalization;

namespace NHSC.Repository
{
    public class BankHolidayRepository : Repository<BankHolidayModel>, IBankHolidayRepository
    {
        public BankHolidayRepository(DbContext _db) : base(_db)
        {

        }
        public List<BankHolidayModel> GetList(CommonModel objPaging)
        {
            List<BankHolidayModel> objList = new List<BankHolidayModel>();
            DataSet ds;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@SearchValue", CommonFunctions.ValidateString(objPaging.searchValue));
                Parameters.Add("@OrderBy", CommonFunctions.ValidateString(objPaging.ShortColumnName));
                Parameters.Add("@Start", objPaging.Skip);
                Parameters.Add("@End", objPaging.PageSize);
                ds = GetDataSetUsingSP("sp_GetBankHoliDay", Parameters);

                DataTable dt = ds.Tables[0];

                if (CommonFunctions.IsValidDataTable(dt))
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        BankHolidayModel objvalue = new BankHolidayModel();
                        objvalue.BankHolidaysId = CommonFunctions.ValidIntValue(dt.Rows[i]["BankHolidaysId"]?.ToString() ?? "0");
                        objvalue.RowNumber = CommonFunctions.ValidlongValue(dt.Rows[i]["RowNumber"]?.ToString() ?? "0");
                        objvalue.HolidayName = dt.Rows[i]["HolidayName"]?.ToString() ?? "";
                        objvalue.Date = dt.Rows[i]["CreatedDate"]?.ToString() ?? "";
                        objvalue.HolidayDates = dt.Rows[i]["HolidayDate"]?.ToString() ?? "";
                        objList.Add(objvalue);
                    }
                }
                dt = ds.Tables[1];

                if (CommonFunctions.IsValidDataTable(dt))
                {
                    objPaging.TotalCount = CommonFunctions.ValidlongValue(dt.Rows[0]["TotalCount"].ToString());
                }
            }
            catch (Exception)
            {
                throw;
            }
            return objList;
        }
        public bool ExistName(BankHolidayModel objValue)
        {
            bool IsExists = false;
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@HolidayName", CommonFunctions.ValidateString(objValue.HolidayName));
                Parameters.Add("@BankHolidaysId", objValue.BankHolidaysId);
                Parameters.Add("@HolidayDate", CommonFunctions.ConvertToValidDateFormate(objValue.HolidayDates));
                dt = GetDataTableUsingSP("sp_CheckBankHolidayExist", Parameters);
                if (CommonFunctions.IsValidDataTable(dt))
                {
                    IsExists = true;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return IsExists;
        }
        public bool InsertData(BankHolidayModel objValue, ref string strErrorMsg)
        {
            bool IsValid = true;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {

                Parameters.Add("@BankHolidaysId", objValue.BankHolidaysId);
                Parameters.Add("@HolidayName", CommonFunctions.ValidateString(objValue.HolidayName));
                Parameters.Add("@HolidayDate", CommonFunctions.ConvertToValidDateFormate(objValue.HolidayDates));
                Parameters.Add("@ModifyBy", objValue.ModifyBy);
                Parameters.Add("@ModifyDate", objValue.ModifyDate);
                int Value = ExecuteSP("sp_InsertBankHoliday", Parameters);
            }
            catch (Exception)
            {
                throw;
            }
            return IsValid;
        }
        public bool DeleteData(long Id, long ModifyBy, DateTime ModifyDate)
        {
            bool IsValid = true;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@BankHolidaysId", Id);
                Parameters.Add("@ModifyBy", ModifyBy);
                Parameters.Add("@ModifyDate", ModifyDate);
                ExecuteSP("sp_DeleteBankHoliDay", Parameters);
            }
            catch (Exception)
            {
                throw;
            }
            return IsValid;
        }
        public BankHolidayModel GetDetails(long Id)
        {
            BankHolidayModel objvalue = new BankHolidayModel();
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                if (Id > 0)
                {
                    Parameters.Add("@BankHolidaysId", Id);
                    dt = GetDataTableUsingSP("sp_GetBankHolidayDetails", Parameters);

                    if (CommonFunctions.IsValidDataTable(dt))
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            objvalue.BankHolidaysId = CommonFunctions.ValidIntValue(dt.Rows[i]["BankHolidaysId"]?.ToString() ?? "0");
                            objvalue.HolidayName = dt.Rows[i]["HolidayName"]?.ToString() ?? "";
                            objvalue.HolidayDate = Convert.ToDateTime(dt.Rows[i]["HolidayDate"]?.ToString() ?? "");
                            objvalue.HolidayDates = Convert.ToDateTime(dt.Rows[i]["HolidayDate"]?.ToString() ?? "").ToString(AppConstants.DateShowFormate);
                        }
                    }
                }
                else
                {
                    objvalue.HolidayDate = CommonFunctions.Datetimesetting();
                    objvalue.HolidayDates = CommonFunctions.Datetimesetting().ToString(AppConstants.DateShowFormate);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return objvalue;
        }
    }
}
