﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHSC.Models.Entities;
using System.Data.Entity;
using NHSC.Models.Common;
using System.Data;
using System.Data.Sql;
using MHSC.Repository;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Data.Common;
using NHSC.Common;

namespace NHSC.Repository
{
    public class CommonRepository : Repository<CommonModel>, ICommonRepository
    {
        public CommonRepository(DbContext _db) : base(_db)
        {

        }
        public bool InsertErrorData(ErrorLogModel objError)
        {
            bool IsValid = true;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@ErrorMsg", CommonFunctions.ValidateString(objError.ErrorMsg));
                Parameters.Add("@InnerException", CommonFunctions.ValidateString(objError.InnerException));
                Parameters.Add("@IpAddress", CommonFunctions.ValidateString(objError.IpAddress));
                Parameters.Add("@Controller", CommonFunctions.ValidateString(objError.Controller));
                Parameters.Add("@Action", CommonFunctions.ValidateString(objError.Action));
                Parameters.Add("@UserId", objError.UserId);
                Parameters.Add("@ModifyDate", objError.ModifyDate);
                Parameters.Add("@LineNumber", objError.LineNumber);
                int Value = ExecuteSP("sp_InsertError", Parameters);
            }
            catch (Exception)
            {
                throw;
            }
            return IsValid;
        }
        public bool InsertActivityLog(ActivityLogModel objActivity)
        {
            bool IsValid = true;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@ActivityLog", CommonFunctions.ValidateString(objActivity.ActivityLog));
                Parameters.Add("@MenuId", objActivity.MenuId);
                Parameters.Add("@UserId", objActivity.UserId);
                Parameters.Add("@ModifyDate", objActivity.ModifyDate);
                Parameters.Add("@ModifyBy", objActivity.ModifyBy);
                Parameters.Add("@RecordId", objActivity.RecordId);
                int Value = ExecuteSP("sp_InsertActivityLog", Parameters);
            }
            catch (Exception)
            {
                throw;
            }
            return IsValid;
        }

        public List<ErrorLogModel> GetErrorLogList(CommonModel objPaging)
        {
            List<ErrorLogModel> objList = new List<ErrorLogModel>();
            DataSet ds;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@SearchValue", CommonFunctions.ValidateString(objPaging.searchValue));
                Parameters.Add("@OrderBy", CommonFunctions.ValidateString(objPaging.ShortColumnName));
                Parameters.Add("@Start", objPaging.Skip);
                Parameters.Add("@End", objPaging.PageSize);
                ds = GetDataSetUsingSP("sp_GetErrorLog", Parameters);

                DataTable dt = ds.Tables[0];

                if (CommonFunctions.IsValidDataTable(dt))
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        ErrorLogModel objvalue = new ErrorLogModel();
                        objvalue.ErrorId = CommonFunctions.ValidIntValue(dt.Rows[i]["ErrorId"]?.ToString() ?? "0");
                        objvalue.LineNumber = CommonFunctions.ValidIntValue(dt.Rows[i]["LineNumber"]?.ToString() ?? "0");
                        objvalue.ErrorMsg = dt.Rows[i]["ErrorMsg"]?.ToString() ?? "";
                        objvalue.InnerException = dt.Rows[i]["InnerException"]?.ToString() ?? "";
                        objvalue.IpAddress = dt.Rows[i]["IpAddress"]?.ToString() ?? "";
                        objvalue.Controller = dt.Rows[i]["Controller"]?.ToString() ?? "";
                        objvalue.Action = dt.Rows[i]["Action"]?.ToString() ?? "";
                        objvalue.Email = dt.Rows[i]["Email"]?.ToString() ?? "";
                        objvalue.FullName = dt.Rows[i]["FName"]?.ToString() ?? "" + " " + dt.Rows[i]["LName"]?.ToString() ?? "";
                        objvalue.Date = dt.Rows[i]["CreatedDate"]?.ToString() ?? "";
                        objList.Add(objvalue);
                    }
                }
                dt = ds.Tables[1];

                if (CommonFunctions.IsValidDataTable(dt))
                {
                    objPaging.TotalCount = CommonFunctions.ValidlongValue(dt.Rows[0]["TotalCount"].ToString());
                }
            }
            catch (Exception)
            {
                throw;
            }
            return objList;
        }
        public ErrorLogModel GetErrorLogDetails(long Id)
        {
            ErrorLogModel objvalue = new ErrorLogModel();
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                if (Id > 0)
                {
                    Parameters.Add("@ErrorId", Id);
                    dt = GetDataTableUsingSP("sp_GetErrorDetails", Parameters);

                    if (CommonFunctions.IsValidDataTable(dt))
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            objvalue.ErrorId = CommonFunctions.ValidIntValue(dt.Rows[i]["ErrorId"]?.ToString() ?? "0");
                            objvalue.LineNumber = CommonFunctions.ValidIntValue(dt.Rows[i]["LineNumber"]?.ToString() ?? "0");
                            objvalue.ErrorMsg = dt.Rows[i]["ErrorMsg"]?.ToString() ?? "";
                            objvalue.InnerException = dt.Rows[i]["InnerException"]?.ToString() ?? "";
                            objvalue.IpAddress = dt.Rows[i]["IpAddress"]?.ToString() ?? "";
                            objvalue.Controller = dt.Rows[i]["Controller"]?.ToString() ?? "";
                            objvalue.Action = dt.Rows[i]["Action"]?.ToString() ?? "";
                            objvalue.Email = dt.Rows[i]["Email"]?.ToString() ?? "";
                            objvalue.FullName = dt.Rows[i]["FName"]?.ToString() ?? "" + " " + dt.Rows[i]["LName"]?.ToString() ?? "";
                            objvalue.Date = dt.Rows[i]["CreatedDate"]?.ToString() ?? "";
                            objvalue.UserGroupName = dt.Rows[i]["UserGroupName"]?.ToString() ?? "";
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return objvalue;
        }
        public List<ActivityLogModel> GetActivityLogList(CommonModel objPaging)
        {
            List<ActivityLogModel> objList = new List<ActivityLogModel>();
            DataSet ds;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@SearchValue", CommonFunctions.ValidateString(objPaging.searchValue));
                Parameters.Add("@OrderBy", CommonFunctions.ValidateString(objPaging.ShortColumnName));
                Parameters.Add("@Start", objPaging.Skip);
                Parameters.Add("@End", objPaging.PageSize);
                ds = GetDataSetUsingSP("sp_GetActivityLog", Parameters);

                DataTable dt = ds.Tables[0];

                if (CommonFunctions.IsValidDataTable(dt))
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        ActivityLogModel objvalue = new ActivityLogModel();
                        objvalue.ActivityId = CommonFunctions.ValidIntValue(dt.Rows[i]["ActivityId"]?.ToString() ?? "0");
                        objvalue.RecordId = CommonFunctions.ValidIntValue(dt.Rows[i]["RecordId"]?.ToString() ?? "0");
                        objvalue.MenuId = CommonFunctions.ValidIntValue(dt.Rows[i]["MenuId"]?.ToString() ?? "0");
                        if (objvalue.MenuId > 0)
                        {
                            objvalue.MenuName = DisplayText.Get(typeof(Menu), objvalue.MenuId).ToString();
                        }
                        objvalue.ActivityLog = dt.Rows[i]["ActivityLog"]?.ToString() ?? "";
                        objvalue.Email = dt.Rows[i]["Email"]?.ToString() ?? "";
                        objvalue.FullName = dt.Rows[i]["FName"]?.ToString() ?? "" + " " + dt.Rows[i]["LName"]?.ToString() ?? "";
                        objvalue.Date = dt.Rows[i]["CreatedDate"]?.ToString() ?? "";
                        objList.Add(objvalue);
                    }
                }
                dt = ds.Tables[1];

                if (CommonFunctions.IsValidDataTable(dt))
                {
                    objPaging.TotalCount = CommonFunctions.ValidlongValue(dt.Rows[0]["TotalCount"].ToString());
                }
            }
            catch (Exception)
            {
                throw;
            }
            return objList;
        }
        public ActivityLogModel GetActivityLogDetails(long Id)
        {
            ActivityLogModel objvalue = new ActivityLogModel();
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                if (Id > 0)
                {
                    Parameters.Add("@ActivityId", Id);
                    dt = GetDataTableUsingSP("sp_GetActivityDetails", Parameters);

                    if (CommonFunctions.IsValidDataTable(dt))
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            objvalue.ActivityId = CommonFunctions.ValidIntValue(dt.Rows[i]["ActivityId"]?.ToString() ?? "0");
                            objvalue.RecordId = CommonFunctions.ValidIntValue(dt.Rows[i]["RecordId"]?.ToString() ?? "0");
                            objvalue.MenuId = CommonFunctions.ValidIntValue(dt.Rows[i]["MenuId"]?.ToString() ?? "0");
                            if (objvalue.MenuId > 0)
                            {
                                objvalue.MenuName = DisplayText.Get(typeof(Menu), objvalue.MenuId).ToString();
                            }
                            objvalue.ActivityLog = dt.Rows[i]["ActivityLog"]?.ToString() ?? "";
                            objvalue.Email = dt.Rows[i]["Email"]?.ToString() ?? "";
                            objvalue.FullName = dt.Rows[i]["FName"]?.ToString() ?? "" + " " + dt.Rows[i]["LName"]?.ToString() ?? "";
                            objvalue.Date = dt.Rows[i]["CreatedDate"]?.ToString() ?? "";
                            objvalue.UserGroupName = dt.Rows[i]["UserGroupName"]?.ToString() ?? "";
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return objvalue;
        }

        public bool IsAssignToAnyModule(string ModuleType, long Id, ref string strErrorMsg)
        {
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            bool IsValid = false;
            try
            {
                if (Id > 0)
                {
                    Parameters.Add("@Id", Id);
                    Parameters.Add("@ModuleType", ModuleType);
                    dt = GetDataTableUsingSP("sp_CheckAssginToAnyModule", Parameters);

                    if (CommonFunctions.IsValidDataTable(dt))
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            strErrorMsg = dt.Rows[i][0]?.ToString() ?? "";
                            IsValid = CommonFunctions.ValidStringIntBoolValue(dt.Rows[i][1]?.ToString() ?? "0");
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return IsValid;
        }
    }

}
