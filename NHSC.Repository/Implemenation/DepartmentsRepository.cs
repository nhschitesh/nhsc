﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHSC.Models.Entities;
using System.Data.Entity;
using NHSC.Models.Common;
using System.Data;
using System.Data.Sql;
using MHSC.Repository;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Data.Common;

namespace NHSC.Repository
{
    public class DepartmentsRepository : Repository<DepartmentsModel>, IDepartmentsRepository
    {
        public DepartmentsRepository(DbContext _db) : base(_db)
        {

        }
        public List<DepartmentsModel> GetList(CommonModel objPaging)
        {
            List<DepartmentsModel> objList = new List<DepartmentsModel>();
            DataSet ds;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@SearchValue", CommonFunctions.ValidateString(objPaging.searchValue));
                Parameters.Add("@OrderBy", CommonFunctions.ValidateString(objPaging.ShortColumnName));
                Parameters.Add("@Start", objPaging.Skip);
                Parameters.Add("@End", objPaging.PageSize);
                ds = GetDataSetUsingSP("sp_GetDepartments", Parameters);

                DataTable dt = ds.Tables[0];

                if (CommonFunctions.IsValidDataTable(dt))
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DepartmentsModel objvalue = new DepartmentsModel();
                        objvalue.DepartmentId = CommonFunctions.ValidIntValue(dt.Rows[i]["DepartmentId"]?.ToString() ?? "0");
                        objvalue.RowNumber = CommonFunctions.ValidlongValue(dt.Rows[i]["RowNumber"]?.ToString() ?? "0");
                        objvalue.DepartmentName = dt.Rows[i]["DepartmentName"]?.ToString() ?? "";
                        objvalue.SiteName = dt.Rows[i]["SiteName"]?.ToString() ?? "";
                        objvalue.Date = dt.Rows[i]["CreatedDate"]?.ToString() ?? "";
                        objList.Add(objvalue);
                    }
                }
                dt = ds.Tables[1];

                if (CommonFunctions.IsValidDataTable(dt))
                {
                    objPaging.TotalCount = CommonFunctions.ValidlongValue(dt.Rows[0]["TotalCount"].ToString());
                }
            }
            catch (Exception)
            {
                throw;
            }
            return objList;
        }
        public bool ExistName(DepartmentsModel objValue)
        {
            bool IsExists = false;
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@DepartmentName", CommonFunctions.ValidateString(objValue.DepartmentName));
                Parameters.Add("@DepartmentId", objValue.DepartmentId);
                Parameters.Add("@SiteId", objValue.SiteId);
                dt = GetDataTableUsingSP("sp_CheckDepartmentsExist", Parameters);
                if (CommonFunctions.IsValidDataTable(dt))
                {
                    IsExists = true;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return IsExists;
        }
        public bool InsertData(DepartmentsModel objValue, ref string strErrorMsg)
        {
            bool IsValid = true;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@DepartmentId", objValue.DepartmentId);
                Parameters.Add("@SiteId", objValue.SiteId);
                Parameters.Add("@DepartmentName", CommonFunctions.ValidateString(objValue.DepartmentName));
                Parameters.Add("@ModifyBy", objValue.ModifyBy);
                Parameters.Add("@ModifyDate", objValue.ModifyDate);
                int Value = ExecuteSP("sp_InsertDepartments", Parameters);
            }
            catch (Exception)
            {
                throw;
            }
            return IsValid;
        }
        public bool DeleteData(long Id, long ModifyBy, DateTime ModifyDate)
        {
            bool IsValid = true;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@DepartmentId", Id);
                Parameters.Add("@ModifyBy", ModifyBy);
                Parameters.Add("@ModifyDate", ModifyDate);
                ExecuteSP("sp_DeleteDepartment", Parameters);
            }
            catch (Exception)
            {
                throw;
            }
            return IsValid;
        }
        public DepartmentsModel GetDetails(long Id)
        {
            DepartmentsModel objvalue = new DepartmentsModel();
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                if (Id > 0)
                {
                    Parameters.Add("@DepartmentId", Id);
                    dt = GetDataTableUsingSP("sp_GetDepartmentDetails", Parameters);

                    if (CommonFunctions.IsValidDataTable(dt))
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            objvalue.DepartmentId = CommonFunctions.ValidIntValue(dt.Rows[i]["DepartmentId"]?.ToString() ?? "0");
                            objvalue.SiteId = CommonFunctions.ValidIntValue(dt.Rows[i]["SiteId"]?.ToString() ?? "0");
                            objvalue.DepartmentName = dt.Rows[i]["DepartmentName"]?.ToString() ?? "";
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return objvalue;
        }
        public List<SelectListItem> GetDepartments()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                dt = GetDataTableUsingSP("sp_GetDepartmentsList", Parameters);
                if (CommonFunctions.IsValidDataTable(dt))
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SelectListItem objValue = new SelectListItem();
                        objValue.Value = dt.Rows[i]["DepartmentId"]?.ToString() ?? "0";
                        objValue.Text = dt.Rows[i]["DepartmentName"]?.ToString() ?? "";
                        list.Add(objValue);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return list;
        }

        public List<AssignDepartmentToUserModel> GetDepartmentList(CommonModel objPaging)
        {
            List<AssignDepartmentToUserModel> objList = new List<AssignDepartmentToUserModel>();
            DataSet ds;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@SearchValue", CommonFunctions.ValidateString(objPaging.searchValue));
                Parameters.Add("@OrderBy", CommonFunctions.ValidateString(objPaging.ShortColumnName));
                Parameters.Add("@Start", objPaging.Skip);
                Parameters.Add("@End", objPaging.PageSize);
                Parameters.Add("@UserId", objPaging.Id);
                ds = GetDataSetUsingSP("sp_GetDepartmentByUser", Parameters);

                DataTable dt = ds.Tables[0];

                if (CommonFunctions.IsValidDataTable(dt))
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        AssignDepartmentToUserModel objvalue = new AssignDepartmentToUserModel();
                        objvalue.DepartmentId = CommonFunctions.ValidIntValue(dt.Rows[i]["DepartmentId"]?.ToString() ?? "0");
                        objvalue.isAssign = CommonFunctions.ValidStringIntBoolValue(dt.Rows[i]["isAssign"]?.ToString() ?? "0");
                        objvalue.DepartmentName = dt.Rows[i]["DepartmentName"]?.ToString() ?? "";
                        objvalue.SiteName = dt.Rows[i]["SiteName"]?.ToString() ?? "";
                        objvalue.Date = dt.Rows[i]["CreatedDate"]?.ToString() ?? "";
                        objList.Add(objvalue);
                    }
                }
                dt = ds.Tables[1];

                if (CommonFunctions.IsValidDataTable(dt))
                {
                    objPaging.TotalCount = CommonFunctions.ValidlongValue(dt.Rows[0]["TotalCount"].ToString());
                }

            }
            catch (Exception)
            {
                throw;
            }
            return objList;
        }

        public bool AssignDepartmentToUser(UserModel Objuser, string DepartmentIds)
        {
            bool IsValid = true;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@UserId", Objuser.UserId);
                Parameters.Add("@DepartmentIds", CommonFunctions.ValidateString(DepartmentIds));
                Parameters.Add("@ModifyBy", Objuser.ModifyBy);
                Parameters.Add("@ModifyDate", Objuser.ModifyDate);
                int Value = ExecuteSP("sp_AssignDepartmentToUser", Parameters);
            }
            catch (Exception)
            {
                throw;
            }
            return IsValid;
        }

        public string GetDepartmentIds(long Id)
        {
            string DepartmentIDs = "";
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                if (Id > 0)
                {
                    Parameters.Add("@UserId", Id);
                    dt = GetDataTableUsingSP("sp_GetDepartmentIds", Parameters);

                    if (CommonFunctions.IsValidDataTable(dt))
                    {
                        DepartmentIDs = dt.Rows[0][0]?.ToString() ?? "";
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return DepartmentIDs;
        }
    }
}
