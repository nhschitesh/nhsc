﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHSC.Models.Entities;
using System.Data.Entity;
using NHSC.Models.Common;
using System.Data;
using System.Data.Sql;
using MHSC.Repository;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Data.Common;

namespace NHSC.Repository
{
    public class SkillSetRepository : Repository<SkillSetModel>, ISkillSetRepository
    {
        public SkillSetRepository(DbContext _db) : base(_db)
        {

        }
        public List<SkillSetModel> GetList(CommonModel objPaging)
        {
            List<SkillSetModel> objList = new List<SkillSetModel>();
            DataSet ds;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@SearchValue", CommonFunctions.ValidateString(objPaging.searchValue));
                Parameters.Add("@OrderBy", CommonFunctions.ValidateString(objPaging.ShortColumnName));
                Parameters.Add("@Start", objPaging.Skip);
                Parameters.Add("@End", objPaging.PageSize);
                ds = GetDataSetUsingSP("sp_GetSkillSets", Parameters);

                DataTable dt = ds.Tables[0];

                if (CommonFunctions.IsValidDataTable(dt))
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SkillSetModel objvalue = new SkillSetModel();
                        objvalue.SkillSetId = CommonFunctions.ValidIntValue(dt.Rows[i]["SkillSetId"]?.ToString() ?? "0");
                        objvalue.RowNumber = CommonFunctions.ValidlongValue(dt.Rows[i]["RowNumber"]?.ToString() ?? "0");
                        objvalue.SkillSetName = dt.Rows[i]["SkillSetName"]?.ToString() ?? "";
                        objvalue.Date = dt.Rows[i]["CreatedDate"]?.ToString() ?? "";
                        objList.Add(objvalue);
                    }
                }
                dt = ds.Tables[1];

                if (CommonFunctions.IsValidDataTable(dt))
                {
                    objPaging.TotalCount = CommonFunctions.ValidlongValue(dt.Rows[0]["TotalCount"].ToString());
                }
            }
            catch (Exception)
            {
                throw;
            }
            return objList;
        }
        public bool ExistName(SkillSetModel objValue)
        {
            bool IsExists = false;
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@SkillSetName", CommonFunctions.ValidateString(objValue.SkillSetName));
                Parameters.Add("@SkillSetId", objValue.SkillSetId);
                dt = GetDataTableUsingSP("sp_CheckSkillSetExist", Parameters);
                if (CommonFunctions.IsValidDataTable(dt))
                {
                    IsExists = true;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return IsExists;
        }
        public bool InsertData(SkillSetModel objValue, ref string strErrorMsg)
        {
            bool IsValid = true;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@SkillSetId", objValue.SkillSetId);
                Parameters.Add("@SkillSetName", CommonFunctions.ValidateString(objValue.SkillSetName));
                Parameters.Add("@ModifyBy", objValue.ModifyBy);
                Parameters.Add("@ModifyDate", objValue.ModifyDate);
                int Value = ExecuteSP("sp_InsertSkillSet", Parameters);
            }
            catch (Exception)
            {
                throw;
            }
            return IsValid;
        }
        public bool DeleteData(long Id, long ModifyBy, DateTime ModifyDate)
        {
            bool IsValid = true;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@SkillSetId", Id);
                Parameters.Add("@ModifyBy", ModifyBy);
                Parameters.Add("@ModifyDate", ModifyDate);
                ExecuteSP("sp_DeleteSkillSet", Parameters);
            }
            catch (Exception)
            {
                throw;
            }
            return IsValid;
        }
        public SkillSetModel GetDetails(long Id)
        {
            SkillSetModel objvalue = new SkillSetModel();
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                if (Id > 0)
                {
                    Parameters.Add("@SkillSetId", Id);
                    dt = GetDataTableUsingSP("sp_GetSkillSetDetails", Parameters);

                    if (CommonFunctions.IsValidDataTable(dt))
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            objvalue.SkillSetId = CommonFunctions.ValidIntValue(dt.Rows[i]["SkillSetId"]?.ToString() ?? "0");
                            objvalue.SkillSetName = dt.Rows[i]["SkillSetName"]?.ToString() ?? "";
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return objvalue;
        }

        public List<SelectListItem> GetSkillSet()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                dt = GetDataTableUsingSP("sp_GetSkillSetList", Parameters);
                if (CommonFunctions.IsValidDataTable(dt))
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SelectListItem objValue = new SelectListItem();
                        objValue.Value = dt.Rows[i]["SkillSetId"]?.ToString() ?? "0";
                        objValue.Text = dt.Rows[i]["SkillSetName"]?.ToString() ?? "";
                        list.Add(objValue);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return list;
        }
    }
}
