﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHSC.Models.Entities;
using System.Data.Entity;
using NHSC.Models.Common;
using System.Data;
using System.Data.Sql;
using MHSC.Repository;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Data.Common;
using NHSC.Common;

namespace NHSC.Repository
{
    public class RotaTemplateRepository : Repository<RotaTemplateModel>, IRotaTemplateRepository
    {
        public RotaTemplateRepository(DbContext _db) : base(_db)
        {

        }
        public List<RotaTemplateModel> GetList(CommonModel objPaging)
        {
            List<RotaTemplateModel> objList = new List<RotaTemplateModel>();
            DataSet ds;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@SearchValue", CommonFunctions.ValidateString(objPaging.searchValue));
                Parameters.Add("@OrderBy", CommonFunctions.ValidateString(objPaging.ShortColumnName));
                Parameters.Add("@Start", objPaging.Skip);
                Parameters.Add("@End", objPaging.PageSize);
                Parameters.Add("@IsAllow", CommonFunctions.ShowAll);
                Parameters.Add("@UserId", objPaging.Id);
                ds = GetDataSetUsingSP("sp_GetRotaTemplate", Parameters);

                DataTable dt = ds.Tables[0];

                if (CommonFunctions.IsValidDataTable(dt))
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        RotaTemplateModel objvalue = new RotaTemplateModel();
                        objvalue.RotaTemplateId = CommonFunctions.ValidlongValue(dt.Rows[i]["RotaTemplateId"]?.ToString() ?? "0");
                        //objvalue.SiteId = CommonFunctions.ValidIntValue(dt.Rows[i]["SiteId"]?.ToString() ?? "0");
                        //objvalue.DepartmentId = CommonFunctions.ValidIntValue(dt.Rows[i]["DepartmentId"]?.ToString() ?? "0");
                        objvalue.StartDayOfWeek = CommonFunctions.ValidIntValue(dt.Rows[i]["StartDayOfWeek"]?.ToString() ?? "0");
                        objvalue.StartDayOfWeekName = DisplayText.Get(typeof(WeekName), objvalue.StartDayOfWeek).ToString();
                        objvalue.DurationInDays = CommonFunctions.ValidIntValue(dt.Rows[i]["DurationInDays"]?.ToString() ?? "0");
                        objvalue.GeneratedUpToDates = dt.Rows[i]["GeneratedUpToDate"]?.ToString() ?? "";
                        objvalue.RowNumber = CommonFunctions.ValidlongValue(dt.Rows[i]["RowNumber"]?.ToString() ?? "0");
                        objvalue.RotaTemplateName = dt.Rows[i]["RotaTemplateName"]?.ToString() ?? "";
                        //objvalue.ShiftIds = dt.Rows[i]["ShiftIds"]?.ToString() ?? "";
                        //objvalue.UserIds = dt.Rows[i]["UserIds"]?.ToString() ?? "";
                        objvalue.ShiftName = dt.Rows[i]["ShiftName"]?.ToString() ?? "";
                        objvalue.UserName = dt.Rows[i]["UserName"]?.ToString() ?? "";
                        objvalue.DepartmentName = dt.Rows[i]["DepartmentName"]?.ToString() ?? "";
                        objvalue.SiteName = dt.Rows[i]["SiteName"]?.ToString() ?? "";
                        objvalue.Date = dt.Rows[i]["CreatedDate"]?.ToString() ?? "";
                        objvalue.IsAllowEdit = CommonFunctions.ValidStringIntBoolValue(dt.Rows[i]["IsAllowEdit"]?.ToString() ?? "0");


                        objList.Add(objvalue);
                    }
                }
                dt = ds.Tables[1];

                if (CommonFunctions.IsValidDataTable(dt))
                {
                    objPaging.TotalCount = CommonFunctions.ValidlongValue(dt.Rows[0]["TotalCount"].ToString());
                }
            }
            catch (Exception)
            {
                throw;
            }
            return objList;
        }
        public bool ExistName(RotaTemplateModel objValue)
        {
            bool IsExists = false;
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@RotaTemplateName", CommonFunctions.ValidateString(objValue.RotaTemplateName));
                Parameters.Add("@RotaTemplateId", objValue.RotaTemplateId);
                dt = GetDataTableUsingSP("sp_CheckRotaTemplateExist", Parameters);
                if (CommonFunctions.IsValidDataTable(dt))
                {
                    IsExists = true;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return IsExists;
        }
        public bool InsertData(RotaTemplateModel objValue, ref string strErrorMsg)
        {
            bool IsValid = true;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            SqlTransaction trans = null;
            int Value = 0;
            try
            {
                using (var con = db.Database.Connection)
                {
                    try
                    {
                        con.Open();
                        if (trans == null)
                        {
                            trans = ((SqlConnection)db.Database.Connection).BeginTransaction();
                        }
                        using (var cmd = con.CreateCommand())
                        {

                            Parameters.Add("@RotaTemplateId", objValue.RotaTemplateId);
                            Parameters.Add("@SiteId", objValue.SiteId);
                            Parameters.Add("@DepartmentId", objValue.DepartmentId);
                            Parameters.Add("@StartDayOfWeek", objValue.StartDayOfWeek);
                            Parameters.Add("@DurationInDays", objValue.DurationInDays);
                            //Parameters.Add("@GeneratedUpToDate", objValue.GeneratedUpToDate);
                            Parameters.Add("@RotaTemplateName", CommonFunctions.ValidateString(objValue.RotaTemplateName));
                            Parameters.Add("@ShiftIds", CommonFunctions.ValidateString(objValue.ShiftIds));
                            Parameters.Add("@UserIds", CommonFunctions.ValidateString(objValue.UserIds));
                            Parameters.Add("@ModifyBy", objValue.ModifyBy);
                            Parameters.Add("@ModifyDate", objValue.ModifyDate);

                            cmd.CommandText = "sp_InsertRotaTemplate";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Transaction = trans;
                            foreach (KeyValuePair<string, object> param in Parameters)
                            {
                                DbParameter dbParameter = cmd.CreateParameter();
                                dbParameter.ParameterName = param.Key;
                                dbParameter.Value = param.Value;
                                cmd.Parameters.Add(dbParameter);
                            }
                            Value = CommonFunctions.ValidIntValue(cmd.ExecuteScalar().ToString());


                            string[] arryShift = objValue.ShiftIds.Split(',');

                            if (arryShift != null && arryShift.Count() > 0)
                            {
                                int i = 0;
                                foreach (var item in arryShift)
                                {
                                    i++;
                                    cmd.Parameters.Clear();
                                    Parameters = new Dictionary<string, object>();
                                    Parameters.Add("@ShiftId", item);
                                    Parameters.Add("@RotaTemplateId", Value);
                                    Parameters.Add("@DurationInDays", objValue.DurationInDays);
                                    Parameters.Add("@IsChange", objValue.IsChange);
                                    Parameters.Add("@ShiftIds", CommonFunctions.ValidateString(objValue.ShiftIds));
                                    Parameters.Add("@ModifyBy", objValue.ModifyBy);

                                    Parameters.Add("@ModifyDate", objValue.ModifyDate);
                                    if (i == arryShift.Count())
                                    {
                                        Parameters.Add("@IsLast", true);
                                    }
                                    else
                                    {
                                        Parameters.Add("@IsLast", false);
                                    }
                                    cmd.CommandText = "sp_AssignRota";
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    foreach (KeyValuePair<string, object> param in Parameters)
                                    {
                                        DbParameter dbParameter = cmd.CreateParameter();
                                        dbParameter.ParameterName = param.Key;
                                        dbParameter.Value = param.Value;
                                        cmd.Parameters.Add(dbParameter);
                                    }
                                    cmd.ExecuteScalar();
                                }
                            }
                            trans.Commit();
                        }
                    }
                    catch (Exception)
                    {
                        trans.Rollback();
                        throw;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return IsValid;
        }
        public bool DeleteData(long Id, long ModifyBy, DateTime ModifyDate)
        {
            bool IsValid = true;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@RotaTemplateId", Id);
                Parameters.Add("@ModifyBy", ModifyBy);
                Parameters.Add("@ModifyDate", ModifyDate);
                ExecuteSP("sp_DeleteRotaTemplate", Parameters);
            }
            catch (Exception)
            {
                throw;
            }
            return IsValid;
        }
        public RotaTemplateModel GetDetails(long Id)
        {
            RotaTemplateModel objvalue = new RotaTemplateModel();
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                if (Id > 0)
                {
                    Parameters.Add("@RotaTemplateId", Id);
                    dt = GetDataTableUsingSP("sp_GetRotaTemplateDetails", Parameters);

                    if (CommonFunctions.IsValidDataTable(dt))
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            objvalue.RotaTemplateId = CommonFunctions.ValidlongValue(dt.Rows[i]["RotaTemplateId"]?.ToString() ?? "0");
                            objvalue.SiteId = CommonFunctions.ValidIntValue(dt.Rows[i]["SiteId"]?.ToString() ?? "0");
                            objvalue.DepartmentId = CommonFunctions.ValidIntValue(dt.Rows[i]["DepartmentId"]?.ToString() ?? "0");
                            objvalue.StartDayOfWeek = CommonFunctions.ValidIntValue(dt.Rows[i]["StartDayOfWeek"]?.ToString() ?? "0");
                            objvalue.DurationInDays = CommonFunctions.ValidIntValue(dt.Rows[i]["DurationInDays"]?.ToString() ?? "0");
                            objvalue.OldDurationInDays = objvalue.DurationInDays;
                            objvalue.GeneratedUpToDates = dt.Rows[i]["GeneratedUpToDate"]?.ToString() ?? "";
                            objvalue.RotaTemplateName = dt.Rows[i]["RotaTemplateName"]?.ToString() ?? "";
                            objvalue.ShiftIds = dt.Rows[i]["ShiftIds"]?.ToString() ?? "";
                            objvalue.OldShiftIds = objvalue.ShiftIds;
                            objvalue.UserIds = dt.Rows[i]["UserIds"]?.ToString() ?? "";
                            //objvalue.ShiftName = dt.Rows[i]["ShiftName"]?.ToString() ?? "";
                            //objvalue.UserName = dt.Rows[i]["UserName"]?.ToString() ?? "";
                            //objvalue.DepartmentName = dt.Rows[i]["DepartmentName"]?.ToString() ?? "";
                            //objvalue.SiteName = dt.Rows[i]["SiteName"]?.ToString() ?? "";
                            objvalue.Date = dt.Rows[i]["CreatedDate"]?.ToString() ?? "";
                            objvalue.ShiftList = objvalue.ShiftIds.Split(',');
                            objvalue.UserList = objvalue.UserIds.Split(',');
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return objvalue;
        }
        public List<RotaTemplateModel> GetRotaTemplate(string SearchValue)
        {
            List<RotaTemplateModel> list = new List<RotaTemplateModel>();
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {

                Parameters.Add("@SearchValue", CommonFunctions.IsValidString(SearchValue));
                dt = GetDataTableUsingSP("sp_GetRotaTemplateList", Parameters);
                if (CommonFunctions.IsValidDataTable(dt))
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        RotaTemplateModel objValue = new RotaTemplateModel();
                        objValue.RotaTemplateId = CommonFunctions.ValidlongValue(dt.Rows[i]["RotaTemplateId"]?.ToString() ?? "0");
                        objValue.RotaTemplateName = dt.Rows[i]["RotaTemplateName"]?.ToString() ?? "";
                        //objValue.ShiftName = dt.Rows[i]["ShiftName"]?.ToString() ?? "";
                        //objValue.UserName = dt.Rows[i]["UserName"]?.ToString() ?? "";
                        //objValue.DepartmentName = dt.Rows[i]["DepartmentName"]?.ToString() ?? "";
                        //objValue.SiteName = dt.Rows[i]["SiteName"]?.ToString() ?? "";
                        //objValue.Date = dt.Rows[i]["CreatedDate"]?.ToString() ?? "";
                        list.Add(objValue);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return list;
        }
        public bool IsAssignNewStaff(long Id)
        {
            bool IsExists = false;
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@RotaTemplateId", Id);
                dt = GetDataTableUsingSP("sp_CheckAssignNewStaff", Parameters);
                if (CommonFunctions.IsValidDataTable(dt))
                {
                    IsExists = true;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return IsExists;
        }
        public AssignRotaModel GetAssignRotaList(long MultidisciplinaryId,long UserId)
        {
            AssignRotaModel objAssignRotaModel = new AssignRotaModel();
            List<RotaList> objRotaList = new List<RotaList>();
            DataSet ds;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@MultidisciplinaryId", MultidisciplinaryId);
                Parameters.Add("@IsAllow", CommonFunctions.ShowAll);
                Parameters.Add("@UserId", UserId);
                ds = GetDataSetUsingSP("sp_GetRotaTemplateByMulti", Parameters);

                DataTable dt = ds.Tables[0];

                // get template distinct
                DataView rotatemplate = new DataView(dt);
                DataTable rotatemplatedistinct = rotatemplate.ToTable(true, "RotaTemplateId");



                if (CommonFunctions.IsValidDataTable(rotatemplatedistinct))
                {
                    for (int i = 0; i < rotatemplatedistinct.Rows.Count; i++)
                    {
                        RotaList objvalue = new RotaList();
                        // Get template id
                        long RotaTemplateId = CommonFunctions.ValidlongValue(rotatemplatedistinct.Rows[i]["RotaTemplateId"]?.ToString() ?? "0");

                        // get distint shift by template
                        DataRow[] distshift = rotatemplate.ToTable(true, "RotaTemplateId", "ShiftId").Select("RotaTemplateId = " + RotaTemplateId);

                        // get all data by template id
                        DataRow[] drdatabytempid = dt.Select("RotaTemplateId = " + RotaTemplateId);

                        // get max day
                        int maxdays = CommonFunctions.ValidIntValue(drdatabytempid[drdatabytempid.Length - 1][2]?.ToString() ?? "0");

                        objvalue.RotaTemplateId = RotaTemplateId;
                        objvalue.RotaTemplateName = drdatabytempid[0]["RotaTemplateName"]?.ToString() ?? "";
                        objvalue.IsAllowEdit = CommonFunctions.ValidStringIntBoolValue(drdatabytempid[0]["IsAllowEdit"]?.ToString() ?? "0");

                        for (int a = 1; a <= maxdays; a++)
                        {
                            DaysList objDaysList = new DaysList();
                            objDaysList.Day = a;
                            objDaysList.RotaTemplateId = RotaTemplateId;

                            for (int b = 0; b < distshift.Length; b++)
                            {
                                RotaDayWiseShiftList objDayWiseShift = new RotaDayWiseShiftList();
                                objDayWiseShift.RotaTemplateId = RotaTemplateId;
                                objDayWiseShift.ShiftId = CommonFunctions.ValidlongValue(distshift[b]["ShiftId"].ToString());
                                objDayWiseShift.Day = a;
                                // get data by template id and shift id and day wise
                                DataRow[] shiftdata = dt.Select("RotaTemplateId = " + RotaTemplateId + " and ShiftId = " + objDayWiseShift.ShiftId + " and Day = " + a);

                                objDayWiseShift.RotaAssignId = CommonFunctions.ValidlongValue(shiftdata[0]["RotaAssignId"].ToString());
                                objDayWiseShift.StaffId = CommonFunctions.ValidlongValue(shiftdata[0]["StaffId"].ToString());
                                objDayWiseShift.IsChange = CommonFunctions.ValidBoolValue(shiftdata[0]["IsChange"].ToString());
                                objDayWiseShift.ShiftName = shiftdata[0]["ShiftName"].ToString();
                                objDayWiseShift.StaffName = shiftdata[0]["StaffName"].ToString();
                                if (string.IsNullOrEmpty(objDayWiseShift.StaffName.Trim()))
                                {
                                    objDayWiseShift.StaffName = "N/A";
                                }
                                objDayWiseShift.StartTime = shiftdata[0]["StartTime"].ToString();
                                objDayWiseShift.EndTime = shiftdata[0]["EndTime"].ToString();
                                objDayWiseShift.ColorName = shiftdata[0]["ColorName"].ToString();
                                objDaysList.DayWiseShiftList.Add(objDayWiseShift);
                                if (a == 1)
                                {
                                    ShiftList objShiftList = new ShiftList();
                                    objShiftList.ShiftId = objDayWiseShift.ShiftId;
                                    objShiftList.ShiftName = objDayWiseShift.ShiftName;
                                    objShiftList.StartTime = objDayWiseShift.StartTime;
                                    objShiftList.EndTime = objDayWiseShift.EndTime;

                                    objvalue.ShiftList.Add(objShiftList);
                                }
                            }

                            objvalue.DaysList.Add(objDaysList);
                        }
                        objRotaList.Add(objvalue);
                    }
                    objAssignRotaModel.RotaList = objRotaList;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return objAssignRotaModel;
        }

        public DataTable GetAssignRotaListExport(long MultidisciplinaryId)
        {
            AssignRotaModel objAssignRotaModel = new AssignRotaModel();
            List<RotaList> objRotaList = new List<RotaList>();
            DataSet ds;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            DataTable dtExport = new DataTable();
            try
            {
               
                Parameters.Add("@MultidisciplinaryId", MultidisciplinaryId);
                ds = GetDataSetUsingSP("sp_GetRotaTemplateByMultiExport", Parameters);

                dtExport = ds.Tables[0];
            }
            catch (Exception)
            {
                throw;
            }
            return dtExport;
        }
        public bool AssignNewStaff(long AssignId, long StaffId, long ModifyBy, DateTime ModifyDate)
        {
            bool IsValid = true;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@AssignId", AssignId);
                Parameters.Add("@StaffId", StaffId);
                Parameters.Add("@ModifyBy", ModifyBy);
                Parameters.Add("@ModifyDate", ModifyDate);
                ExecuteSP("sp_AssignNewStaffToShift", Parameters);
            }
            catch (Exception)
            {
                throw;
            }
            return IsValid;
        }
        public AssignRotaModel GetAssignRotaTemplateList(long RotaTemplateId,long UserId)
        {
            AssignRotaModel objAssignRotaModel = new AssignRotaModel();
            List<RotaList> objRotaList = new List<RotaList>();
            DataSet ds;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@RotaTemplateId", RotaTemplateId);
                Parameters.Add("@IsAllow", CommonFunctions.ShowAll);
                Parameters.Add("@UserId", UserId);
                ds = GetDataSetUsingSP("sp_GetTemplateRota", Parameters);

                DataTable dt = ds.Tables[0];

                // get template distinct
                DataView rotatemplate = new DataView(dt);
                DataTable rotatemplatedistinct = rotatemplate.ToTable(true, "RotaTemplateId");



                if (CommonFunctions.IsValidDataTable(rotatemplatedistinct))
                {
                    for (int i = 0; i < rotatemplatedistinct.Rows.Count; i++)
                    {
                        RotaList objvalue = new RotaList();
                        // Get template id

                        // get distint shift by template
                        DataRow[] distshift = rotatemplate.ToTable(true, "RotaTemplateId", "ShiftId").Select("RotaTemplateId = " + RotaTemplateId);

                        // get all data by template id
                        DataRow[] drdatabytempid = dt.Select("RotaTemplateId = " + RotaTemplateId);

                        // get max day
                        int maxdays = CommonFunctions.ValidIntValue(drdatabytempid[drdatabytempid.Length - 1][2]?.ToString() ?? "0");

                        objvalue.RotaTemplateId = RotaTemplateId;
                        objvalue.RotaTemplateName = drdatabytempid[0]["RotaTemplateName"]?.ToString() ?? "";
                        objvalue.IsAllowEdit = CommonFunctions.ValidStringIntBoolValue(drdatabytempid[0]["IsAllowEdit"]?.ToString() ?? "0");

                        for (int a = 1; a <= maxdays; a++)
                        {
                            DaysList objDaysList = new DaysList();
                            objDaysList.Day = a;
                            objDaysList.RotaTemplateId = RotaTemplateId;

                            for (int b = 0; b < distshift.Length; b++)
                            {
                                RotaDayWiseShiftList objDayWiseShift = new RotaDayWiseShiftList();
                                objDayWiseShift.RotaTemplateId = RotaTemplateId;
                                objDayWiseShift.ShiftId = CommonFunctions.ValidlongValue(distshift[b]["ShiftId"].ToString());
                                objDayWiseShift.Day = a;
                                // get data by template id and shift id and day wise
                                DataRow[] shiftdata = dt.Select("RotaTemplateId = " + RotaTemplateId + " and ShiftId = " + objDayWiseShift.ShiftId + " and Day = " + a);

                                objDayWiseShift.RotaAssignId = CommonFunctions.ValidlongValue(shiftdata[0]["RotaAssignId"].ToString());
                                objDayWiseShift.StaffId = CommonFunctions.ValidlongValue(shiftdata[0]["StaffId"].ToString());
                                objDayWiseShift.OrderSeq = CommonFunctions.ValidIntValue(shiftdata[0]["OrderSeq"].ToString());
                                objDayWiseShift.IsChange = CommonFunctions.ValidBoolValue(shiftdata[0]["IsChange"].ToString());
                                objDayWiseShift.ShiftName = shiftdata[0]["ShiftName"].ToString();
                                objDayWiseShift.StaffName = shiftdata[0]["StaffName"].ToString();
                                if (string.IsNullOrEmpty(objDayWiseShift.StaffName.Trim()))
                                {
                                    objDayWiseShift.StaffName = "N/A";
                                }
                                objDayWiseShift.StartTime = shiftdata[0]["StartTime"].ToString();
                                objDayWiseShift.EndTime = shiftdata[0]["EndTime"].ToString();
                                objDayWiseShift.ColorName = shiftdata[0]["ColorName"].ToString();
                                objDaysList.DayWiseShiftList.Add(objDayWiseShift);
                                if (a == 1)
                                {
                                    ShiftList objShiftList = new ShiftList();
                                    objShiftList.ShiftId = objDayWiseShift.ShiftId;
                                    objShiftList.ShiftName = objDayWiseShift.ShiftName;
                                    objShiftList.StartTime = objDayWiseShift.StartTime;
                                    objShiftList.EndTime = objDayWiseShift.EndTime;
                                    objShiftList.OrderSeq = objDayWiseShift.OrderSeq;

                                    objvalue.ShiftList.Add(objShiftList);
                                }
                            }

                            objvalue.DaysList.Add(objDaysList);
                        }
                        objRotaList.Add(objvalue);
                    }
                    objAssignRotaModel.RotaList = objRotaList;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return objAssignRotaModel;
        }

        public bool UpdateStaff(RotaDayWiseShiftList objRota, long ModifyBy, DateTime ModifyDate)
        {
            bool IsValid = true;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@AssignId", objRota.RotaAssignId);
                Parameters.Add("@StaffName", objRota.StaffName);
                Parameters.Add("@ModifyBy", ModifyBy);
                Parameters.Add("@ModifyDate", ModifyDate);
                ExecuteSP("sp_UpdateStaffToRota", Parameters);
            }
            catch (Exception)
            {
                throw;
            }
            return IsValid;
        }
    }
}
