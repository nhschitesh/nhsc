﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHSC.Models.Entities;
using System.Data.Entity;
using NHSC.Models.Common;
using System.Data;
using System.Data.Sql;
using MHSC.Repository;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Data.Common;
using NHSC.Common;

namespace NHSC.Repository
{
    public class ActualRotaRepository : Repository<ActualRotaModel>, IActualRotaRepository
    {
        public ActualRotaRepository(DbContext _db) : base(_db)
        {

        }
        public ActualRotaModel GetAssignActualRotaList(long MultidisciplinaryId,long UserId)
        {
            ActualRotaModel objActualRotaModel = new ActualRotaModel();
            List<RotaList> objRotaList = new List<RotaList>();
            DataSet ds;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@MultidisciplinaryId", MultidisciplinaryId);
                Parameters.Add("@IsAllow", CommonFunctions.ShowAll);
                Parameters.Add("@UserId", UserId);
                ds = GetDataSetUsingSP("sp_GetActualRotaTemplateByMulti", Parameters);

                DataTable dt = ds.Tables[0];

                // get template distinct
                DataView ActualRota = new DataView(dt);
                DataTable ActualRotadistinct = ActualRota.ToTable(true, "RotaTemplateId");



                if (CommonFunctions.IsValidDataTable(ActualRotadistinct))
                {
                    for (int i = 0; i < ActualRotadistinct.Rows.Count; i++)
                    {
                        RotaList objvalue = new RotaList();
                        // Get template id
                        long RotaTemplateId = CommonFunctions.ValidlongValue(ActualRotadistinct.Rows[i]["RotaTemplateId"]?.ToString() ?? "0");

                        // get distint shift by template
                        DataRow[] distshift = ActualRota.ToTable(true, "RotaTemplateId", "ShiftId").Select("RotaTemplateId = " + RotaTemplateId);

                        // get all data by template id
                        DataRow[] drdatabytempid = dt.Select("RotaTemplateId = " + RotaTemplateId);


                        DateTime Startdate; DateTime EndDate;

                        // Get start date
                        Startdate = Convert.ToDateTime(drdatabytempid[0][2]?.ToString() ?? "0");

                        // Get end date
                        EndDate = Convert.ToDateTime(drdatabytempid[drdatabytempid.Length - 1][2]?.ToString() ?? "0");



                        objvalue.RotaTemplateId = RotaTemplateId;
                        objvalue.RotaTemplateName = drdatabytempid[0]["RotaTemplateName"]?.ToString() ?? "";
                        objvalue.IsAllowEdit = CommonFunctions.ValidStringIntBoolValue(drdatabytempid[0]["IsAllowEdit"]?.ToString() ?? "0");


                        for (DateTime a = Startdate.Date; a <= EndDate.Date;)
                        {
                            DaysList objDaysList = new DaysList();
                            objDaysList.RotaDate = a.ToString(AppConstants.DateShowFormate);
                            objDaysList.RotaTemplateId = RotaTemplateId;

                            for (int b = 0; b < distshift.Length; b++)
                            {
                                RotaDayWiseShiftList objDayWiseShift = new RotaDayWiseShiftList();
                                objDayWiseShift.RotaTemplateId = RotaTemplateId;
                                objDayWiseShift.ShiftId = CommonFunctions.ValidlongValue(distshift[b]["ShiftId"].ToString());
                                objDayWiseShift.RotaDate = a.ToString(AppConstants.DateShowFormate);
                                // get data by template id and shift id and day wise
                                DataRow[] shiftdata = dt.Select("RotaTemplateId = " + RotaTemplateId + " and ShiftId = " + objDayWiseShift.ShiftId + " and RotaDate = '" + a.Date.ToString() + "'");

                                objDayWiseShift.ActualRotaId = CommonFunctions.ValidlongValue(shiftdata[0]["ActualRotaId"].ToString());
                                objDayWiseShift.IsChange = CommonFunctions.ValidBoolValue(shiftdata[0]["IsChange"].ToString());
                                objDayWiseShift.ShiftName = shiftdata[0]["ShiftName"].ToString();

                                objDayWiseShift.StaffId = CommonFunctions.ValidlongValue(shiftdata[0]["StaffId"].ToString());
                                objDayWiseShift.StaffName = shiftdata[0]["StaffName"].ToString();
                                objDayWiseShift.StartTime = shiftdata[0]["StartTime"].ToString();
                                objDayWiseShift.EndTime = shiftdata[0]["EndTime"].ToString();


                                objDayWiseShift.SplitStaffId = CommonFunctions.ValidlongValue(shiftdata[0]["SplitStaffId"].ToString());
                                objDayWiseShift.SplitStaffName = shiftdata[0]["SplitStaffName"].ToString();
                                objDayWiseShift.SplitStartTime = shiftdata[0]["SplitStartTime"].ToString();
                                objDayWiseShift.SplitEndTime = shiftdata[0]["SplitEndTime"].ToString();
                                objDayWiseShift.HolidayName = shiftdata[0]["HolidayName"].ToString();
                                objDayWiseShift.IsSplit = CommonFunctions.ValidBoolValue(shiftdata[0]["IsSplit"].ToString());
                                objDayWiseShift.ColorName = shiftdata[0]["ColorName"].ToString();

                                objDaysList.DayWiseShiftList.Add(objDayWiseShift);
                                if (a.Date == Startdate.Date)
                                {
                                    ShiftList objShiftList = new ShiftList();
                                    objShiftList.ShiftId = objDayWiseShift.ShiftId;
                                    objShiftList.ShiftName = objDayWiseShift.ShiftName;
                                    objShiftList.StartTime = shiftdata[0]["ActualStartTime"].ToString();
                                    objShiftList.EndTime = shiftdata[0]["ActualEndTime"].ToString();

                                    objvalue.ShiftList.Add(objShiftList);
                                }
                            }

                            objvalue.DaysList.Add(objDaysList);
                            a = a.AddDays(1);
                        }
                        objRotaList.Add(objvalue);
                    }
                    objActualRotaModel.RotaList = objRotaList;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return objActualRotaModel;
        }
        public bool AssignNewStaff(StaffPopUp objStaffPopUp, long ModifyBy, DateTime ModifyDate)
        {
            bool IsValid = true;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@ActualRotaId", objStaffPopUp.ActualRotaId);
                Parameters.Add("@StaffId", objStaffPopUp.StaffId);
                Parameters.Add("@ActualEndTime", CommonFunctions.ValidateString(objStaffPopUp.ActualEndTime));
                Parameters.Add("@ActualStartTime", CommonFunctions.ValidateString(objStaffPopUp.ActualStartTime));

                if (objStaffPopUp.IsSplit)
                {
                    Parameters.Add("@SplitStaffId", objStaffPopUp.SplitStaffId);
                    Parameters.Add("@StartTime", CommonFunctions.ValidateString(objStaffPopUp.StartTime));
                    Parameters.Add("@EndTime", CommonFunctions.ValidateString(objStaffPopUp.EndTime));
                    Parameters.Add("@SplitStartTime", CommonFunctions.ValidateString(objStaffPopUp.SplitStartTime));
                    Parameters.Add("@SplitEndTime", CommonFunctions.ValidateString(objStaffPopUp.SplitEndTime));
                }
                else
                {
                    Parameters.Add("@SplitStaffId", 0);
                    Parameters.Add("@StartTime", CommonFunctions.ValidateString(objStaffPopUp.StartTime));
                    Parameters.Add("@EndTime", CommonFunctions.ValidateString(objStaffPopUp.EndTime));
                    Parameters.Add("@SplitStartTime", "");
                    Parameters.Add("@SplitEndTime", "");
                }
                Parameters.Add("@ModifyBy", ModifyBy);
                Parameters.Add("@ModifyDate", ModifyDate);
                ExecuteSP("sp_AssignActualStaffToShift", Parameters);
            }
            catch (Exception)
            {
                throw;
            }
            return IsValid;
        }
        public bool UnAssignStaff(StaffPopUp objStaffPopUp, long ModifyBy, DateTime ModifyDate)
        {
            bool IsValid = true;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@ActualRotaId", objStaffPopUp.ActualRotaId);
                Parameters.Add("@ModifyBy", ModifyBy);
                ExecuteSP("sp_UnAssignStaff", Parameters);
            }
            catch (Exception)
            {
                throw;
            }
            return IsValid;
        }
        public bool InsertActualRota(RotaTemplateModel objRota)
        {
            bool IsValid = true;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@RotaTemplateId", objRota.RotaTemplateId);
                Parameters.Add("@GeneratedUpToDate", CommonFunctions.ConvertToValidDateFormate(objRota.GeneratedUpToDates));
                Parameters.Add("@ModifyBy", objRota.ModifyBy);
                //Parameters.Add("@ModifyDate", objRota.ModifyDate);
                ExecuteSP("sp_AssignActualRota", Parameters);
            }
            catch (Exception)
            {
                throw;
            }
            return IsValid;
        }
        public StaffPopUp GetActualRota(long ActualRotaId)
        {
            StaffPopUp objvalue = new StaffPopUp();
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@ActualRotaId", ActualRotaId);
                dt = GetDataTableUsingSP("sp_GetActualData", Parameters);


                if (CommonFunctions.IsValidDataTable(dt))
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        objvalue.ActualRotaId = CommonFunctions.ValidlongValue(dt.Rows[i]["ActualRotaId"]?.ToString() ?? "0");
                        objvalue.RotaTemplateId = CommonFunctions.ValidlongValue(dt.Rows[i]["RotaTemplateId"]?.ToString() ?? "0");
                        objvalue.SplitStaffId = CommonFunctions.ValidlongValue(dt.Rows[i]["SplitStaffId"]?.ToString() ?? "0");
                        //objvalue.ShiftId = CommonFunctions.ValidlongValue(dt.Rows[i]["ShiftId"]?.ToString() ?? "0");
                        objvalue.StaffId = CommonFunctions.ValidlongValue(dt.Rows[i]["StaffId"]?.ToString() ?? "0");
                        objvalue.IsSplit = CommonFunctions.ValidBoolValue(dt.Rows[i]["IsSplit"]?.ToString() ?? "0");
                        objvalue.StartTime = dt.Rows[i]["StartTime"]?.ToString() ?? "";
                        objvalue.EndTime = dt.Rows[i]["EndTime"]?.ToString() ?? "";
                        objvalue.ActualStartTime = dt.Rows[i]["ActualStartTime"]?.ToString() ?? "";
                        objvalue.ActualEndTime = dt.Rows[i]["ActualEndTime"]?.ToString() ?? "";
                        objvalue.RotaDate = dt.Rows[i]["RotaDate"]?.ToString() ?? "";

                        objvalue.SplitStartTime = objvalue.ActualStartTime;
                        objvalue.SplitEndTime = objvalue.ActualEndTime;
                        if (objvalue.IsSplit)
                        {
                            objvalue.SplitStartTime = dt.Rows[i]["SplitStartTime"]?.ToString() ?? "";
                            objvalue.SplitEndTime = dt.Rows[i]["SplitEndTime"]?.ToString() ?? "";
                        }
                        objvalue.StaffName = dt.Rows[i]["StaffName"]?.ToString() ?? "";
                        objvalue.SplitStaffName = dt.Rows[i]["SplitStaffName"]?.ToString() ?? "";


                        DataTable dt1;
                        Dictionary<string, object> Parameters1 = new Dictionary<string, object>();
                        objvalue.StaffList = new List<StaffList>();
                        objvalue.SplitStaffList = new List<StaffList>();
                        if (objvalue.StaffId > 0)
                        {
                            Parameters1 = new Dictionary<string, object>();
                            Parameters1.Add("@StaffId", objvalue.StaffId);
                            dt1 = GetDataTableUsingSP("sp_GetStaffDetailsByOrder", Parameters1);

                            if (CommonFunctions.IsValidDataTable(dt))
                            {
                                foreach (var row in dt1.Columns)
                                {
                                    StaffList objstaff = new StaffList();
                                    objstaff.ColumnName = row.ToString();
                                    objstaff.Value = dt1.Rows[0][objstaff.ColumnName].ToString();
                                    if (!string.IsNullOrEmpty(objstaff.Value))
                                    {
                                        objvalue.StaffList.Add(objstaff);
                                    }
                                }
                            }
                        }

                        if (objvalue.SplitStaffId > 0)
                        {
                            Parameters1 = new Dictionary<string, object>();
                            Parameters1.Add("@StaffId", objvalue.SplitStaffId);
                            dt1 = GetDataTableUsingSP("sp_GetStaffDetailsByOrder", Parameters1);

                            if (CommonFunctions.IsValidDataTable(dt))
                            {
                                foreach (var row in dt1.Columns)
                                {
                                    StaffList objstaff = new StaffList();
                                    objstaff.ColumnName = row.ToString();
                                    objstaff.Value = dt1.Rows[0][objstaff.ColumnName].ToString();
                                    if (!string.IsNullOrEmpty(objstaff.Value))
                                    {
                                        objvalue.SplitStaffList.Add(objstaff);
                                    }
                                }
                            }
                        }

                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return objvalue;
        }
        public bool CheckActualSatffExits(StaffPopUp objStaffPopUp)
        {
            bool IsExists = false;
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@ActualId", objStaffPopUp.ActualRotaId);
                Parameters.Add("@StaffId", objStaffPopUp.SplitStaffId);
                Parameters.Add("@StartTime", CommonFunctions.ValidateString(objStaffPopUp.SplitStartTime));
                Parameters.Add("@EndTime", CommonFunctions.ValidateString(objStaffPopUp.SplitEndTime));
                Parameters.Add("@Set24Hour", 0);
                Parameters.Add("@Date", CommonFunctions.ValidateString(objStaffPopUp.RotaDate));
                dt = GetDataTableUsingSP("sp_CheckActualStaffExistsSameShift", Parameters);
                if (CommonFunctions.IsValidDataTable(dt))
                {
                    IsExists = true;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return IsExists;
        }
        public bool CheckActualMainSatffExits(StaffPopUp objStaffPopUp)
        {
            bool IsExists = false;
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@ActualId", objStaffPopUp.ActualRotaId);
                Parameters.Add("@StaffId", objStaffPopUp.StaffId);
                Parameters.Add("@StartTime", CommonFunctions.ValidateString(objStaffPopUp.StartTime));
                Parameters.Add("@EndTime", CommonFunctions.ValidateString(objStaffPopUp.EndTime));
                Parameters.Add("@Set24Hour", 0);
                Parameters.Add("@Date", CommonFunctions.ValidateString(objStaffPopUp.RotaDate));
                dt = GetDataTableUsingSP("sp_CheckActualMainStaffExistsSameShift", Parameters);
                if (CommonFunctions.IsValidDataTable(dt))
                {
                    IsExists = true;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return IsExists;
        }
        public ActualRotaModel GetActualRotaList(long RotaTemplateId, long UserId)
        {
            ActualRotaModel objActualRotaModel = new ActualRotaModel();
            List<RotaList> objRotaList = new List<RotaList>();
            DataSet ds;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@RotaTemplateId", RotaTemplateId);
                Parameters.Add("@IsAllow", CommonFunctions.ShowAll);
                Parameters.Add("@UserId", UserId);
                ds = GetDataSetUsingSP("sp_GetActualRotaList", Parameters);

                DataTable dt = ds.Tables[0];

                // get template distinct
                DataView ActualRota = new DataView(dt);
                DataTable ActualRotadistinct = ActualRota.ToTable(true, "RotaTemplateId");



                if (CommonFunctions.IsValidDataTable(ActualRotadistinct))
                {
                    for (int i = 0; i < ActualRotadistinct.Rows.Count; i++)
                    {
                        RotaList objvalue = new RotaList();

                        // get distint shift by template
                        DataRow[] distshift = ActualRota.ToTable(true, "RotaTemplateId", "ShiftId").Select("RotaTemplateId = " + RotaTemplateId);

                        // get all data by template id
                        DataRow[] drdatabytempid = dt.Select("RotaTemplateId = " + RotaTemplateId);


                        DateTime Startdate; DateTime EndDate;

                        // Get start date
                        Startdate = Convert.ToDateTime(drdatabytempid[0][2]?.ToString() ?? "0");

                        // Get end date
                        EndDate = Convert.ToDateTime(drdatabytempid[drdatabytempid.Length - 1][2]?.ToString() ?? "0");



                        objvalue.RotaTemplateId = RotaTemplateId;
                        objvalue.RotaTemplateName = drdatabytempid[0]["RotaTemplateName"]?.ToString() ?? "";
                        objvalue.IsAllowEdit = CommonFunctions.ValidStringIntBoolValue(drdatabytempid[0]["IsAllowEdit"]?.ToString() ?? "0");


                        for (DateTime a = Startdate.Date; a <= EndDate.Date;)
                        {
                            DaysList objDaysList = new DaysList();
                            objDaysList.RotaDate = a.ToString(AppConstants.DateShowFormate) +" " + Convert.ToDateTime(a).ToString("ddd"); ;
                            objDaysList.RotaTemplateId = RotaTemplateId;

                            for (int b = 0; b < distshift.Length; b++)
                            {
                                RotaDayWiseShiftList objDayWiseShift = new RotaDayWiseShiftList();
                                objDayWiseShift.RotaTemplateId = RotaTemplateId;
                                objDayWiseShift.ShiftId = CommonFunctions.ValidlongValue(distshift[b]["ShiftId"].ToString());
                                objDayWiseShift.RotaDate = a.ToString(AppConstants.DateShowFormate);
                                // get data by template id and shift id and day wise
                                DataRow[] shiftdata = dt.Select("RotaTemplateId = " + RotaTemplateId + " and ShiftId = " + objDayWiseShift.ShiftId + " and RotaDate = '" + a.Date.ToString() + "'");

                                objDayWiseShift.ActualRotaId = CommonFunctions.ValidlongValue(shiftdata[0]["ActualRotaId"].ToString());
                                objDayWiseShift.IsChange = CommonFunctions.ValidBoolValue(shiftdata[0]["IsChange"].ToString());
                                objDayWiseShift.OrderSeq = CommonFunctions.ValidIntValue(shiftdata[0]["OrderSeq"].ToString());
                                objDayWiseShift.ShiftName = shiftdata[0]["ShiftName"].ToString();

                                objDayWiseShift.StaffId = CommonFunctions.ValidlongValue(shiftdata[0]["StaffId"].ToString());
                                objDayWiseShift.StaffName = shiftdata[0]["StaffName"].ToString();
                                objDayWiseShift.StartTime = shiftdata[0]["StartTime"].ToString();
                                objDayWiseShift.EndTime = shiftdata[0]["EndTime"].ToString();


                                objDayWiseShift.SplitStaffId = CommonFunctions.ValidlongValue(shiftdata[0]["SplitStaffId"].ToString());
                                objDayWiseShift.SplitStaffName = shiftdata[0]["SplitStaffName"].ToString();
                                objDayWiseShift.SplitStartTime = shiftdata[0]["SplitStartTime"].ToString();
                                objDayWiseShift.SplitEndTime = shiftdata[0]["SplitEndTime"].ToString();
                                objDayWiseShift.HolidayName = shiftdata[0]["HolidayName"].ToString();
                                objDayWiseShift.ColorName = shiftdata[0]["ColorName"].ToString();
                                objDayWiseShift.IsSplit = CommonFunctions.ValidBoolValue(shiftdata[0]["IsSplit"].ToString());

                                objDaysList.DayWiseShiftList.Add(objDayWiseShift);
                                if (a.Date == Startdate.Date)
                                {
                                    ShiftList objShiftList = new ShiftList();
                                    objShiftList.ShiftId = objDayWiseShift.ShiftId;
                                    objShiftList.ShiftName = objDayWiseShift.ShiftName;
                                    objShiftList.OrderSeq = objDayWiseShift.OrderSeq;
                                    objShiftList.StartTime = shiftdata[0]["ActualStartTime"].ToString();
                                    objShiftList.EndTime = shiftdata[0]["ActualEndTime"].ToString();

                                    objvalue.ShiftList.Add(objShiftList);
                                }
                            }

                            objvalue.DaysList.Add(objDaysList);
                            a = a.AddDays(1);
                        }
                        objRotaList.Add(objvalue);
                    }
                    objActualRotaModel.RotaList = objRotaList;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return objActualRotaModel;
        }
    }
}
