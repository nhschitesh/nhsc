﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHSC.Models.Entities;
using System.Data.Entity;
using NHSC.Models.Common;
using System.Data;
using System.Data.Sql;
using MHSC.Repository;
using System.Web.Mvc;

namespace NHSC.Repository
{
    public class UserRepository : Repository<UserModel>, IUserRepository
    {
        public UserRepository(DbContext _db) : base(_db)
        {

        }
        public List<UserModel> GetList(CommonModel objPaging)
        {
            List<UserModel> objList = new List<UserModel>();
            DataSet ds;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@SearchValue", CommonFunctions.ValidateString(objPaging.searchValue));
                Parameters.Add("@OrderBy", CommonFunctions.ValidateString(objPaging.ShortColumnName));
                Parameters.Add("@Start", objPaging.Skip);
                Parameters.Add("@End", objPaging.PageSize);
                ds = GetDataSetUsingSP("sp_getUsers", Parameters);

                DataTable dt = ds.Tables[0];

                if (CommonFunctions.IsValidDataTable(dt))
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        UserModel objvalue = new UserModel();
                        objvalue.UserId = CommonFunctions.ValidlongValue(dt.Rows[i]["UserId"]?.ToString() ?? "0");
                        objvalue.RowNumber = CommonFunctions.ValidlongValue(dt.Rows[i]["RowNumber"]?.ToString() ?? "0");
                        objvalue.UserName = dt.Rows[i]["UserName"]?.ToString() ?? "";
                        objvalue.LName = dt.Rows[i]["LName"]?.ToString() ?? "";

                        objvalue.FName = dt.Rows[i]["FName"]?.ToString() ?? "" + " " + objvalue.LName;

                        objvalue.Email = dt.Rows[i]["Email"]?.ToString() ?? "";
                        objvalue.Password = dt.Rows[i]["Password"]?.ToString() ?? "";
                        objvalue.IsAlertsReceiver = CommonFunctions.ValidBoolValue(dt.Rows[i]["IsAlertsReceiver"]?.ToString() ?? "0");
                        objvalue.UserStatus = CommonFunctions.ValidBoolValue(dt.Rows[i]["UserStatus"]?.ToString() ?? "0");
                        objvalue.IsLogIn = CommonFunctions.ValidBoolValue(dt.Rows[i]["IsLogIn"]?.ToString() ?? "0");
                        objvalue.LastLogInDate = dt.Rows[i]["LastLogInDate"]?.ToString() ?? "";
                        objvalue.UserGroupId = CommonFunctions.ValidIntValue(dt.Rows[i]["UserGroupId"].ToString());
                        objvalue.UserGroupName = dt.Rows[i]["UserGroupName"].ToString();
                        objvalue.Date = dt.Rows[i]["CreatedDate"].ToString();
                        objvalue.IsAdmin = CommonFunctions.ValidBoolValue(dt.Rows[i]["IsAdmin"]?.ToString() ?? "0");
                        objList.Add(objvalue);
                    }
                }
                dt = ds.Tables[1];

                if (CommonFunctions.IsValidDataTable(dt))
                {
                    objPaging.TotalCount = CommonFunctions.ValidlongValue(dt.Rows[0]["TotalCount"].ToString());
                }
            }
            catch (Exception)
            {
                throw;
            }
            return objList;
        }

        public bool ExistEmail(UserModel objUser)
        {
            bool IsExists = false;
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@Email", CommonFunctions.ValidateString(objUser.Email));
                Parameters.Add("@UserName", CommonFunctions.ValidateString(objUser.UserName));
                Parameters.Add("@UserId", objUser.UserId);
                dt = GetDataTableUsingSP("sp_CheckUserExist", Parameters);
                if (CommonFunctions.IsValidDataTable(dt))
                {
                    IsExists = true;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return IsExists;
        }
        public bool InsertData(UserModel objUser, ref string strErrorMsg)
        {
            bool IsValid = true;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@UserId", objUser.UserId);
                Parameters.Add("@UserGroupId", objUser.UserGroupId);
                Parameters.Add("@Email", CommonFunctions.ValidateString(objUser.Email));
                Parameters.Add("@UserName", CommonFunctions.ValidateString(objUser.UserName));
                Parameters.Add("@FName", CommonFunctions.ValidateString(objUser.FName));
                Parameters.Add("@LName", CommonFunctions.ValidateString(objUser.LName));
                Parameters.Add("@Password", CommonFunctions.Encrypt(CommonFunctions.ValidateString(objUser.Password)));
                Parameters.Add("@IsAlertsReceiver", objUser.AlertsReceiver);
                Parameters.Add("@UserStatus", objUser.Status);
                Parameters.Add("@ModifyBy", objUser.ModifyBy);
                Parameters.Add("@ModifyDate", objUser.ModifyDate);
                int Value = ExecuteSP("sp_InsertUser", Parameters);
            }
            catch (Exception)
            {
                throw;
            }
            return IsValid;
        }
        public bool UpdateUserProfile(UserModel objUser)
        {
            bool IsValid = true;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@UserId", objUser.UserId);
                Parameters.Add("@UserName", CommonFunctions.ValidateString(objUser.UserName));
                Parameters.Add("@FName", CommonFunctions.ValidateString(objUser.FName));
                Parameters.Add("@LName", CommonFunctions.ValidateString(objUser.LName));
                Parameters.Add("@ModifyDate", objUser.ModifyDate);
                int Value = ExecuteSP("sp_UpdateUser", Parameters);
            }
            catch (Exception)
            {
                throw;
            }
            return IsValid;
        }
        public bool UpdateLastLogIn(UserModel objUser)
        {
            bool IsValid = true;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@UserId", objUser.UserId);
                Parameters.Add("@ModifyDate", objUser.ModifyDate);
                int Value = ExecuteSP("sp_UpdateLastLogInDate", Parameters);
            }
            catch (Exception)
            {
                throw;
            }
            return IsValid;
        }
        public bool DeleteData(long Id, long ModifyBy, DateTime ModifyDate)
        {
            bool IsValid = true;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@UserId", Id);
                Parameters.Add("@ModifyBy", ModifyBy);
                Parameters.Add("@ModifyDate", ModifyDate);
                ExecuteSP("sp_DeleteUser", Parameters);
            }
            catch (Exception)
            {
                throw;
            }
            return IsValid;
        }

        public UserModel GetDetails(long Id)
        {
            UserModel objvalue = new UserModel();
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                if (Id > 0)
                {
                    Parameters.Add("@UserId", Id);
                    dt = GetDataTableUsingSP("sp_GetUserDetails", Parameters);

                    if (CommonFunctions.IsValidDataTable(dt))
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {

                            objvalue.UserId = CommonFunctions.ValidlongValue(dt.Rows[i]["UserId"]?.ToString() ?? "0");
                            objvalue.UserName = dt.Rows[i]["UserName"]?.ToString() ?? "";
                            objvalue.LName = dt.Rows[i]["LName"]?.ToString() ?? "";
                            objvalue.FName = dt.Rows[i]["FName"]?.ToString() ?? "" + " " + objvalue.LName;
                            objvalue.Email = dt.Rows[i]["Email"]?.ToString() ?? "";
                            objvalue.Password = CommonFunctions.Decrypt(dt.Rows[i]["Password"]?.ToString() ?? "");
                            objvalue.AlertsReceiver = CommonFunctions.ValidIntBoolValue(dt.Rows[i]["IsAlertsReceiver"]?.ToString() ?? "0");
                            objvalue.Status = CommonFunctions.ValidIntBoolValue(dt.Rows[i]["UserStatus"]?.ToString() ?? "0");
                            objvalue.UserGroupId = CommonFunctions.ValidIntValue(dt.Rows[i]["UserGroupId"].ToString());
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return objvalue;
        }

        public UserModel GetDetailsByEmail(string Email)
        {
            UserModel objvalue = new UserModel();
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {

                Parameters.Add("@Email", Email);
                dt = GetDataTableUsingSP("sp_GetUserByEmail", Parameters);

                if (CommonFunctions.IsValidDataTable(dt))
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        objvalue.UserId = CommonFunctions.ValidlongValue(dt.Rows[i]["UserId"]?.ToString() ?? "0");
                        objvalue.UserName = dt.Rows[i]["UserName"]?.ToString() ?? "";
                        objvalue.LName = dt.Rows[i]["LName"]?.ToString() ?? "";
                        objvalue.FName = dt.Rows[i]["FName"]?.ToString() ?? "" + " " + objvalue.LName;
                        objvalue.Email = dt.Rows[i]["Email"]?.ToString() ?? "";
                        objvalue.Password = CommonFunctions.Decrypt(dt.Rows[i]["Password"]?.ToString() ?? "");
                        objvalue.Status = CommonFunctions.ValidIntBoolValue(dt.Rows[i]["UserStatus"]?.ToString() ?? "0");
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return objvalue;
        }

        public UserModel ValidateUser(LogInModel objLogIn)
        {
            UserModel objvalue = new UserModel();
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {

                Parameters.Add("@Email", objLogIn.UserName);
                Parameters.Add("@Password", CommonFunctions.Encrypt(objLogIn.Password));
                dt = GetDataTableUsingSP("sp_ValidateUser", Parameters);

                if (CommonFunctions.IsValidDataTable(dt))
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        objvalue.UserId = CommonFunctions.ValidlongValue(dt.Rows[i]["UserId"]?.ToString() ?? "0");
                        objvalue.UserName = dt.Rows[i]["UserName"]?.ToString() ?? "";
                        objvalue.LName = dt.Rows[i]["LName"]?.ToString() ?? "";
                        objvalue.FName = dt.Rows[i]["FName"]?.ToString() ?? "" + " " + objvalue.LName;
                        objvalue.Email = dt.Rows[i]["Email"]?.ToString() ?? "";
                        objvalue.Password = CommonFunctions.Decrypt(dt.Rows[i]["Password"]?.ToString() ?? "");
                        objvalue.AlertsReceiver = CommonFunctions.ValidIntBoolValue(dt.Rows[i]["IsAlertsReceiver"]?.ToString() ?? "0");
                        objvalue.Status = CommonFunctions.ValidIntBoolValue(dt.Rows[i]["UserStatus"]?.ToString() ?? "0");
                        objvalue.UserGroupId = CommonFunctions.ValidIntValue(dt.Rows[i]["UserGroupId"].ToString());
                        objvalue.IsAdmin = CommonFunctions.ValidBoolValue(dt.Rows[i]["IsAdmin"]?.ToString() ?? "0");
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return objvalue;
        }

        public long InsertForgotData(ForgotPassword objForgot)
        {
            long Id = 0;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@UserId", objForgot.UserId);
                Parameters.Add("@ForgotId", objForgot.ForgotId);
                Parameters.Add("@Email", CommonFunctions.ValidateString(objForgot.Email));
                Parameters.Add("@ModifyDate", objForgot.ModifyDate);
                Id = ExecuteSPScalar("sp_InsertForgotPassword", Parameters);
            }
            catch (Exception)
            {
                throw;
            }
            return Id;
        }

        public ForgotPassword GetForgotDetails(long Id)
        {
            ForgotPassword objvalue = new ForgotPassword();
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                if (Id > 0)
                {
                    Parameters.Add("@ForgotId", Id);
                    dt = GetDataTableUsingSP("sp_GetForgotDetails", Parameters);

                    if (CommonFunctions.IsValidDataTable(dt))
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            objvalue.UserId = CommonFunctions.ValidlongValue(dt.Rows[i]["UserId"]?.ToString() ?? "0");
                            objvalue.ForgotId = CommonFunctions.ValidlongValue(dt.Rows[i]["ForgotId"]?.ToString() ?? "0");
                            objvalue.Email = dt.Rows[i]["Email"]?.ToString() ?? "";
                            objvalue.Status = CommonFunctions.ValidBoolValue(dt.Rows[i]["IsStatus"]?.ToString() ?? "0");
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return objvalue;
        }

        public bool UpdatePassword(ChangePassword objChangePwd)
        {
            bool IsValid = true;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@UserId", objChangePwd.UserId);
                Parameters.Add("@ForgotId", objChangePwd.ForgotId);
                Parameters.Add("@Password", CommonFunctions.Encrypt(CommonFunctions.ValidateString(objChangePwd.Password)));
                Parameters.Add("@ModifyDate", objChangePwd.ModifyDate);
                ExecuteSP("sp_UpdatePassword", Parameters);
            }
            catch (Exception)
            {
                throw;
            }
            return IsValid;
        }

        public List<SelectListItem> UserDepartmentList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                dt = GetDataTableUsingSP("sp_GetDepartmentUser", Parameters);
                if (CommonFunctions.IsValidDataTable(dt))
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SelectListItem objValue = new SelectListItem();
                        objValue.Value = dt.Rows[i]["UserId"]?.ToString() ?? "0";
                        objValue.Text = dt.Rows[i]["Name"]?.ToString() ?? "";
                        list.Add(objValue);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return list;
        }

        public List<SelectListItem> UserList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                dt = GetDataTableUsingSP("sp_GetUsersList", Parameters);
                if (CommonFunctions.IsValidDataTable(dt))
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SelectListItem objValue = new SelectListItem();
                        objValue.Value = dt.Rows[i]["UserId"]?.ToString() ?? "0";
                        objValue.Text = dt.Rows[i]["Name"]?.ToString() ?? "";
                        list.Add(objValue);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return list;
        }
    }
}
