﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHSC.Models.Entities;
using System.Data.Entity;
using NHSC.Models.Common;
using System.Data;
using System.Data.Sql;
using MHSC.Repository;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Data.Common;
using NHSC.Common;

namespace NHSC.Repository
{
    public class MultidisciplinaryRotaRepository : Repository<MultidisciplinaryRotaModel>, IMultidisciplinaryRotaRepository
    {
        public MultidisciplinaryRotaRepository(DbContext _db) : base(_db)
        {

        }
        public List<MultidisciplinaryRotaModel> GetList(CommonModel objPaging)
        {
            List<MultidisciplinaryRotaModel> objList = new List<MultidisciplinaryRotaModel>();
            DataSet ds;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@SearchValue", CommonFunctions.ValidateString(objPaging.searchValue));
                Parameters.Add("@OrderBy", CommonFunctions.ValidateString(objPaging.ShortColumnName));
                Parameters.Add("@Start", objPaging.Skip);
                Parameters.Add("@End", objPaging.PageSize);
                Parameters.Add("@IsAllow", CommonFunctions.ShowAll);
                Parameters.Add("@UserId", objPaging.Id);
                ds = GetDataSetUsingSP("sp_GetMultidisciplinary", Parameters);

                DataTable dt = ds.Tables[0];

                if (CommonFunctions.IsValidDataTable(dt))
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        MultidisciplinaryRotaModel objvalue = new MultidisciplinaryRotaModel();
                        objvalue.MultidisciplinaryId = CommonFunctions.ValidlongValue(dt.Rows[i]["MultidisciplinaryId"]?.ToString() ?? "0");
                        objvalue.RowNumber = CommonFunctions.ValidlongValue(dt.Rows[i]["RowNumber"]?.ToString() ?? "0");
                        objvalue.MultidisciplinaryName = dt.Rows[i]["MultidisciplinaryName"]?.ToString() ?? "";
                        objvalue.RotaTemplateName = dt.Rows[i]["RotaTemplateName"]?.ToString() ?? "";
                        objvalue.UserName = dt.Rows[i]["UserName"]?.ToString() ?? "";
                        objvalue.Date = dt.Rows[i]["CreatedDate"]?.ToString() ?? "";
                        objvalue.IsAllowEdit = CommonFunctions.ValidStringIntBoolValue(dt.Rows[i]["IsAllowEdit"]?.ToString() ?? "0");
                        objList.Add(objvalue);
                    }
                }
                dt = ds.Tables[1];

                if (CommonFunctions.IsValidDataTable(dt))
                {
                    objPaging.TotalCount = CommonFunctions.ValidlongValue(dt.Rows[0]["TotalCount"].ToString());
                }
            }
            catch (Exception)
            {
                throw;
            }
            return objList;
        }
        public bool ExistName(MultidisciplinaryRotaModel objValue)
        {
            bool IsExists = false;
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@MultidisciplinaryName", CommonFunctions.ValidateString(objValue.MultidisciplinaryName));
                Parameters.Add("@MultidisciplinaryId", objValue.MultidisciplinaryId);
                dt = GetDataTableUsingSP("sp_CheckMultidisciplinaryExist", Parameters);
                if (CommonFunctions.IsValidDataTable(dt))
                {
                    IsExists = true;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return IsExists;
        }
        public bool InsertData(MultidisciplinaryRotaModel objValue, ref string strErrorMsg)
        {
            bool IsValid = true;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@MultidisciplinaryId", objValue.MultidisciplinaryId);
                Parameters.Add("@MultidisciplinaryName", CommonFunctions.ValidateString(objValue.MultidisciplinaryName));
                Parameters.Add("@RotaTemplateIds", CommonFunctions.ValidateString(objValue.RotaTemplateIds));
                Parameters.Add("@UserIds", CommonFunctions.ValidateString(objValue.UserIds));
                Parameters.Add("@ModifyBy", objValue.ModifyBy);
                Parameters.Add("@ModifyDate", objValue.ModifyDate);
                int Value = ExecuteSP("sp_InsertMultidisciplinary", Parameters);
            }
            catch (Exception)
            {
                throw;
            }
            return IsValid;
        }
        public bool DeleteData(long Id, long ModifyBy, DateTime ModifyDate)
        {
            bool IsValid = true;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                Parameters.Add("@MultidisciplinaryId", Id);
                Parameters.Add("@ModifyBy", ModifyBy);
                Parameters.Add("@ModifyDate", ModifyDate);
                ExecuteSP("sp_DeleteMultidisciplinary", Parameters);
            }
            catch (Exception)
            {
                throw;
            }
            return IsValid;
        }
        public MultidisciplinaryRotaModel GetDetails(long Id)
        {
            MultidisciplinaryRotaModel objvalue = new MultidisciplinaryRotaModel();
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                if (Id > 0)
                {
                    Parameters.Add("@MultidisciplinaryId", Id);
                    dt = GetDataTableUsingSP("sp_GetMultidisciplinaryDetails", Parameters);

                    if (CommonFunctions.IsValidDataTable(dt))
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            objvalue.MultidisciplinaryId = CommonFunctions.ValidlongValue(dt.Rows[i]["MultidisciplinaryId"]?.ToString() ?? "0");
                            objvalue.MultidisciplinaryName = dt.Rows[i]["MultidisciplinaryName"]?.ToString() ?? "";
                            objvalue.RotaTemplateIds = dt.Rows[i]["RotaTemplateIds"]?.ToString() ?? "";
                            objvalue.UserIds = dt.Rows[i]["UserIds"]?.ToString() ?? "";
                            objvalue.Date = dt.Rows[i]["CreatedDate"]?.ToString() ?? "";
                            objvalue.RotaTemplateList = objvalue.RotaTemplateIds.Split(',');
                            objvalue.UserList = objvalue.UserIds.Split(',');
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return objvalue;
        }

        public List<SelectListItem> GetMultidisciplinaryRota()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            DataTable dt;
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            try
            {
                dt = GetDataTableUsingSP("sp_GetMultidisciplinaryRotaList", Parameters);
                if (CommonFunctions.IsValidDataTable(dt))
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SelectListItem objValue = new SelectListItem();
                        objValue.Value = dt.Rows[i]["MultidisciplinaryId"]?.ToString() ?? "0";
                        objValue.Text = dt.Rows[i]["MultidisciplinaryName"]?.ToString() ?? "";
                        list.Add(objValue);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return list;
        }
    }
}
