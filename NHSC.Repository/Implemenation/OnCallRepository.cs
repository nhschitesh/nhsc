﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHSC.Models.Entities;
using System.Data.Entity;
using NHSC.Models.Common;
using System.Data;
using System.Data.Sql;
using MHSC.Repository;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Data.Common;
using NHSC.Common;

namespace NHSC.Repository
{
    public class OnCallRepository : Repository<OnCallModel>, IOnCallRepository
    {
        public OnCallRepository(DbContext _db) : base(_db)
        {

        }
        //public OnCallModel GetOnCallActualRotaList(string Date, string Departments, string Sites)
        //{
        //    OnCallModel objOnCall = new OnCallModel();
        //    List<RotaList> objRotaList = new List<RotaList>();
        //    Dictionary<string, object> Parameters = new Dictionary<string, object>();
        //    DataSet ds;
        //    List<StaffList> objStaffList = new List<StaffList>();
        //    try
        //    {
        //        Parameters.Add("@Sites", Sites);
        //        Parameters.Add("@Departments", Departments);
        //        Parameters.Add("@Date", CommonFunctions.ConvertToValidDateFormate(Date));
        //        ds = GetDataSetUsingSP("sp_GetOnCallActualRotaList", Parameters);


        //        DataTable dt = ds.Tables[0];
        //        DataTable dt1 = ds.Tables[1];
        //        // get template distinct
        //        DataView ActualRota = new DataView(dt);
        //        DataTable ActualRotadistinct = ActualRota.ToTable(true, "RotaTemplateId");

        //        if (CommonFunctions.IsValidDataTable(ActualRotadistinct))
        //        {
        //            for (int i = 0; i < ActualRotadistinct.Rows.Count; i++)
        //            {
        //                RotaList objvalue = new RotaList();
        //                // Get template id
        //                long RotaTemplateId = CommonFunctions.ValidlongValue(ActualRotadistinct.Rows[i]["RotaTemplateId"]?.ToString() ?? "0");

        //                // get distint shift by template
        //                DataRow[] distshift = ActualRota.ToTable(true, "RotaTemplateId", "ShiftId").Select("RotaTemplateId = " + RotaTemplateId);

        //                // get all data by template id
        //                DataRow[] drdatabytempid = dt.Select("RotaTemplateId = " + RotaTemplateId);


        //                DateTime Startdate; DateTime EndDate;

        //                // Get start date
        //                Startdate = Convert.ToDateTime(drdatabytempid[0][2]?.ToString() ?? "0");

        //                // Get end date
        //                EndDate = Convert.ToDateTime(drdatabytempid[drdatabytempid.Length - 1][2]?.ToString() ?? "0");



        //                objvalue.RotaTemplateId = RotaTemplateId;
        //                objvalue.RotaTemplateName = drdatabytempid[0]["RotaTemplateName"]?.ToString() ?? "";


        //                for (DateTime a = Startdate.Date; a <= EndDate.Date;)
        //                {
        //                    DaysList objDaysList = new DaysList();
        //                    objDaysList.RotaDate = a.ToString(AppConstants.DateShowFormate);
        //                    objDaysList.RotaTemplateId = RotaTemplateId;

        //                    for (int b = 0; b < distshift.Length; b++)
        //                    {
        //                        RotaDayWiseShiftList objDayWiseShift = new RotaDayWiseShiftList();
        //                        string StaffColumn = "";
        //                        string SplitStaffColumn = "";


        //                        objDayWiseShift.RotaTemplateId = RotaTemplateId;
        //                        objDayWiseShift.ShiftId = CommonFunctions.ValidlongValue(distshift[b]["ShiftId"].ToString());
        //                        objDayWiseShift.RotaDate = a.ToString(AppConstants.DateShowFormate);
        //                        objvalue.RotaDate = objDayWiseShift.RotaDate;
        //                        // get data by template id and shift id and day wise
        //                        DataRow[] shiftdata = dt.Select("RotaTemplateId = " + RotaTemplateId + " and ShiftId = " + objDayWiseShift.ShiftId + " and RotaDate = '" + a.Date.ToString() + "'");

        //                        objDayWiseShift.ActualRotaId = CommonFunctions.ValidlongValue(shiftdata[0]["ActualRotaId"].ToString());
        //                        objDayWiseShift.IsChange = CommonFunctions.ValidBoolValue(shiftdata[0]["IsChange"].ToString());
        //                        objDayWiseShift.ShiftName = shiftdata[0]["ShiftName"].ToString();

        //                        objDayWiseShift.StaffId = CommonFunctions.ValidlongValue(shiftdata[0]["StaffId"].ToString());
        //                        //objDayWiseShift.StaffName = shiftdata[0]["StaffName"].ToString();
        //                        objDayWiseShift.StartTime = shiftdata[0]["StartTime"].ToString();
        //                        objDayWiseShift.EndTime = shiftdata[0]["EndTime"].ToString();


        //                        objDayWiseShift.SplitStaffId = CommonFunctions.ValidlongValue(shiftdata[0]["SplitStaffId"].ToString());
        //                        //objDayWiseShift.SplitStaffName = shiftdata[0]["SplitStaffName"].ToString();
        //                        objDayWiseShift.SplitStartTime = shiftdata[0]["SplitStartTime"].ToString();
        //                        objDayWiseShift.SplitEndTime = shiftdata[0]["SplitEndTime"].ToString();
        //                        objDayWiseShift.HolidayName = shiftdata[0]["HolidayName"].ToString();
        //                        objDayWiseShift.IsSplit = CommonFunctions.ValidBoolValue(shiftdata[0]["IsSplit"].ToString());
        //                        objDayWiseShift.ColorName = shiftdata[0]["ColorName"].ToString();



        //                        StaffColumn = shiftdata[0]["StaffColumn"].ToString();
        //                        SplitStaffColumn = shiftdata[0]["SplitStaffColumn"].ToString();



        //                        GetShiftColumnList(objDayWiseShift, StaffColumn, SplitStaffColumn, dt1);

        //                        objDaysList.DayWiseShiftList.Add(objDayWiseShift);
        //                        if (a.Date == Startdate.Date)
        //                        {
        //                            ShiftList objShiftList = new ShiftList();
        //                            objShiftList.ShiftId = objDayWiseShift.ShiftId;
        //                            objShiftList.ShiftName = objDayWiseShift.ShiftName;
        //                            objShiftList.StartTime = shiftdata[0]["ActualStartTime"].ToString();
        //                            objShiftList.EndTime = shiftdata[0]["ActualEndTime"].ToString();

        //                            objvalue.ShiftList.Add(objShiftList);
        //                        }
        //                    }

        //                    objvalue.DaysList.Add(objDaysList);
        //                    a = a.AddDays(1);
        //                }
        //                objRotaList.Add(objvalue);
        //            }
        //            objOnCall.RotaList = objRotaList;
        //        }

        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //    return objOnCall;
        //}

        public OnCallModel GetOnCallActualMultiRotaList(string Date, string Departments, string Sites, int MultiId, long UserId)
        {
            OnCallModel objOnCall = new OnCallModel();
            List<RotaList> objRotaList = new List<RotaList>();
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            DataSet ds;
            List<StaffList> objStaffList = new List<StaffList>();
            try
            {
                Parameters.Add("@Sites", Sites);
                Parameters.Add("@Departments", Departments);
                Parameters.Add("@Date", CommonFunctions.ConvertToValidDateFormate(Date));
                Parameters.Add("@MultiId", MultiId);
                Parameters.Add("@IsAllow", CommonFunctions.ShowAll);
                Parameters.Add("@UserId", UserId);
                ds = GetDataSetUsingSP("sp_GetOnCallMultiActualRotaList", Parameters);


                DataTable dt = ds.Tables[0];
                DataTable dt1 = ds.Tables[1];
                // get template distinct
                DataView ActualRota = new DataView(dt);
                DataTable ActualRotadistinct = ActualRota.ToTable(true, "RotaTemplateId");

                if (CommonFunctions.IsValidDataTable(ActualRotadistinct))
                {
                    for (int i = 0; i < ActualRotadistinct.Rows.Count; i++)
                    {
                        RotaList objvalue = new RotaList();
                        // Get template id
                        long RotaTemplateId = CommonFunctions.ValidlongValue(ActualRotadistinct.Rows[i]["RotaTemplateId"]?.ToString() ?? "0");

                        // get distint shift by template
                        DataRow[] distshift = ActualRota.ToTable(true, "RotaTemplateId", "ShiftId").Select("RotaTemplateId = " + RotaTemplateId);

                        // get all data by template id
                        DataRow[] drdatabytempid = dt.Select("RotaTemplateId = " + RotaTemplateId);


                        DateTime Startdate; DateTime EndDate;

                        // Get start date
                        Startdate = Convert.ToDateTime(drdatabytempid[0][2]?.ToString() ?? "0");

                        // Get end date
                        EndDate = Convert.ToDateTime(drdatabytempid[drdatabytempid.Length - 1][2]?.ToString() ?? "0");



                        objvalue.RotaTemplateId = RotaTemplateId;
                        objvalue.RotaTemplateName = drdatabytempid[0]["RotaTemplateName"]?.ToString() ?? "";
                        objvalue.IsAllowEdit = CommonFunctions.ValidStringIntBoolValue(drdatabytempid[0]["IsAllowEdit"]?.ToString() ?? "0");


                        for (DateTime a = Startdate.Date; a <= EndDate.Date;)
                        {
                            DaysList objDaysList = new DaysList();
                            objDaysList.RotaDate = a.ToString(AppConstants.DateShowFormate);
                            objDaysList.RotaTemplateId = RotaTemplateId;

                            for (int b = 0; b < distshift.Length; b++)
                            {
                                RotaDayWiseShiftList objDayWiseShift = new RotaDayWiseShiftList();
                                string StaffColumn = "";
                                string SplitStaffColumn = "";


                                objDayWiseShift.RotaTemplateId = RotaTemplateId;
                                objDayWiseShift.ShiftId = CommonFunctions.ValidlongValue(distshift[b]["ShiftId"].ToString());
                                objDayWiseShift.RotaDate = a.ToString(AppConstants.DateShowFormate);
                                objvalue.RotaDate = objDayWiseShift.RotaDate;
                                // get data by template id and shift id and day wise
                                DataRow[] shiftdata = dt.Select("RotaTemplateId = " + RotaTemplateId + " and ShiftId = " + objDayWiseShift.ShiftId + " and RotaDate = '" + a.Date.ToString() + "'");

                                objDayWiseShift.ActualRotaId = CommonFunctions.ValidlongValue(shiftdata[0]["ActualRotaId"].ToString());
                                objDayWiseShift.IsChange = CommonFunctions.ValidBoolValue(shiftdata[0]["IsChange"].ToString());
                                objDayWiseShift.ShiftName = shiftdata[0]["ShiftName"].ToString();

                                objDayWiseShift.StaffId = CommonFunctions.ValidlongValue(shiftdata[0]["StaffId"].ToString());
                                //objDayWiseShift.StaffName = shiftdata[0]["StaffName"].ToString();
                                objDayWiseShift.StartTime = shiftdata[0]["StartTime"].ToString();
                                objDayWiseShift.EndTime = shiftdata[0]["EndTime"].ToString();


                                objDayWiseShift.SplitStaffId = CommonFunctions.ValidlongValue(shiftdata[0]["SplitStaffId"].ToString());
                                //objDayWiseShift.SplitStaffName = shiftdata[0]["SplitStaffName"].ToString();
                                objDayWiseShift.SplitStartTime = shiftdata[0]["SplitStartTime"].ToString();
                                objDayWiseShift.SplitEndTime = shiftdata[0]["SplitEndTime"].ToString();
                                objDayWiseShift.HolidayName = shiftdata[0]["HolidayName"].ToString();
                                objDayWiseShift.IsSplit = CommonFunctions.ValidBoolValue(shiftdata[0]["IsSplit"].ToString());
                                objDayWiseShift.ColorName = shiftdata[0]["ColorName"].ToString();



                                StaffColumn = shiftdata[0]["StaffColumn"].ToString();
                                SplitStaffColumn = shiftdata[0]["SplitStaffColumn"].ToString();



                                GetShiftColumnList(objDayWiseShift, StaffColumn, SplitStaffColumn, dt1);

                                objDaysList.DayWiseShiftList.Add(objDayWiseShift);
                                if (a.Date == Startdate.Date)
                                {
                                    ShiftList objShiftList = new ShiftList();
                                    objShiftList.ShiftId = objDayWiseShift.ShiftId;
                                    objShiftList.ShiftName = objDayWiseShift.ShiftName;
                                    objShiftList.StartTime = shiftdata[0]["ActualStartTime"].ToString();
                                    objShiftList.EndTime = shiftdata[0]["ActualEndTime"].ToString();

                                    objvalue.ShiftList.Add(objShiftList);
                                }
                            }

                            objvalue.DaysList.Add(objDaysList);
                            a = a.AddDays(1);
                        }
                        objRotaList.Add(objvalue);
                    }
                    objOnCall.RotaList = objRotaList;
                }

            }
            catch (Exception)
            {
                throw;
            }
            return objOnCall;
        }
        public OnCallModel GetOnCallActualRotaList(string Date, string Departments, string Sites,long UserId)
        {
            OnCallModel objOnCall = new OnCallModel();
            List<RotaList> objRotaList = new List<RotaList>();
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            DataSet ds;
            List<StaffList> objStaffList = new List<StaffList>();
            try
            {
                Parameters.Add("@Sites", Sites);
                Parameters.Add("@Departments", Departments);
                Parameters.Add("@Date", CommonFunctions.ConvertToValidDateFormate(Date));
                Parameters.Add("@IsAllow", CommonFunctions.ShowAll);
                Parameters.Add("@UserId", UserId);
                ds = GetDataSetUsingSP("sp_GetOnCallActualRotaList", Parameters);


                DataTable dt = ds.Tables[0];
                //DataTable dt1 = ds.Tables[1];
                // get template distinct
                DataView ActualRota = new DataView(dt);
                DataTable ActualRotadistinct = ActualRota.ToTable(true, "RotaTemplateId");

                if (CommonFunctions.IsValidDataTable(ActualRotadistinct))
                {
                    for (int i = 0; i < ActualRotadistinct.Rows.Count; i++)
                    {
                        RotaList objvalue = new RotaList();
                        // Get template id
                        long RotaTemplateId = CommonFunctions.ValidlongValue(ActualRotadistinct.Rows[i]["RotaTemplateId"]?.ToString() ?? "0");

                        // get distint shift by template
                        DataRow[] distshift = ActualRota.ToTable(true, "RotaTemplateId", "ShiftId").Select("RotaTemplateId = " + RotaTemplateId);

                        // get all data by template id
                        DataRow[] drdatabytempid = dt.Select("RotaTemplateId = " + RotaTemplateId);


                        DateTime Startdate; DateTime EndDate;

                        // Get start date
                        Startdate = Convert.ToDateTime(drdatabytempid[0][2]?.ToString() ?? "0");

                        // Get end date
                        EndDate = Convert.ToDateTime(drdatabytempid[drdatabytempid.Length - 1][2]?.ToString() ?? "0");



                        objvalue.RotaTemplateId = RotaTemplateId;
                        objvalue.RotaTemplateName = drdatabytempid[0]["RotaTemplateName"]?.ToString() ?? "";
                        objvalue.IsAllowEdit = CommonFunctions.ValidStringIntBoolValue(drdatabytempid[0]["IsAllowEdit"]?.ToString() ?? "0");

                        for (DateTime a = Startdate.Date; a <= EndDate.Date;)
                        {
                            DaysList objDaysList = new DaysList();
                            objDaysList.RotaDate = a.ToString(AppConstants.DateShowFormate);
                            objDaysList.RotaTemplateId = RotaTemplateId;

                            for (int b = 0; b < distshift.Length; b++)
                            {
                                RotaDayWiseShiftList objDayWiseShift = new RotaDayWiseShiftList();
                                string StaffColumn = "";
                                string SplitStaffColumn = "";


                                objDayWiseShift.RotaTemplateId = RotaTemplateId;
                                objDayWiseShift.ShiftId = CommonFunctions.ValidlongValue(distshift[b]["ShiftId"].ToString());
                                objDayWiseShift.RotaDate = a.ToString(AppConstants.DateShowFormate);
                                objvalue.RotaDate = objDayWiseShift.RotaDate;
                                // get data by template id and shift id and day wise
                                DataRow[] shiftdata = dt.Select("RotaTemplateId = " + RotaTemplateId + " and ShiftId = " + objDayWiseShift.ShiftId + " and RotaDate = '" + a.Date.ToString() + "'");

                                objDayWiseShift.ActualRotaId = CommonFunctions.ValidlongValue(shiftdata[0]["ActualRotaId"].ToString());
                                objDayWiseShift.IsChange = CommonFunctions.ValidBoolValue(shiftdata[0]["IsChange"].ToString());
                                objDayWiseShift.ShiftName = shiftdata[0]["ShiftName"].ToString();

                                objDayWiseShift.StaffId = CommonFunctions.ValidlongValue(shiftdata[0]["StaffId"].ToString());
                                //objDayWiseShift.StaffName = shiftdata[0]["StaffName"].ToString();
                                objDayWiseShift.StartTime = shiftdata[0]["StartTime"].ToString();
                                objDayWiseShift.EndTime = shiftdata[0]["EndTime"].ToString();


                                objDayWiseShift.SplitStaffId = CommonFunctions.ValidlongValue(shiftdata[0]["SplitStaffId"].ToString());
                                //objDayWiseShift.SplitStaffName = shiftdata[0]["SplitStaffName"].ToString();
                                objDayWiseShift.SplitStartTime = shiftdata[0]["SplitStartTime"].ToString();
                                objDayWiseShift.SplitEndTime = shiftdata[0]["SplitEndTime"].ToString();
                                objDayWiseShift.HolidayName = shiftdata[0]["HolidayName"].ToString();
                                objDayWiseShift.IsSplit = CommonFunctions.ValidBoolValue(shiftdata[0]["IsSplit"].ToString());
                                objDayWiseShift.ColorName = shiftdata[0]["ColorName"].ToString();



                                StaffColumn = shiftdata[0]["StaffColumn"].ToString();
                                SplitStaffColumn = shiftdata[0]["SplitStaffColumn"].ToString();

                                objDayWiseShift.StaffName = StaffColumn;
                                objDayWiseShift.SplitStaffName = SplitStaffColumn;

                                //GetShiftColumnList(objDayWiseShift, StaffColumn, SplitStaffColumn, dt1);

                                objDaysList.DayWiseShiftList.Add(objDayWiseShift);
                                if (a.Date == Startdate.Date)
                                {
                                    ShiftList objShiftList = new ShiftList();
                                    objShiftList.ShiftId = objDayWiseShift.ShiftId;
                                    objShiftList.ShiftName = objDayWiseShift.ShiftName;
                                    objShiftList.StartTime = shiftdata[0]["ActualStartTime"].ToString();
                                    objShiftList.EndTime = shiftdata[0]["ActualEndTime"].ToString();

                                    objvalue.ShiftList.Add(objShiftList);
                                }
                            }

                            objvalue.DaysList.Add(objDaysList);
                            a = a.AddDays(1);
                        }
                        objRotaList.Add(objvalue);
                    }
                    objOnCall.RotaList = objRotaList;
                }

            }
            catch (Exception)
            {
                throw;
            }
            return objOnCall;
        }

        public void GetShiftColumnList(RotaDayWiseShiftList objDayWiseShift, string StaffColumn, string SplitStaffColumn, DataTable dt)
        {
            string[] Staff;
            try
            {
                DataRow[] staffdata = dt.Select("StaffId = " + objDayWiseShift.StaffId);
                if (!string.IsNullOrEmpty(StaffColumn))
                {
                    Staff = StaffColumn.Split(',');
                    for (int i = 0; i < 3; i++)
                    {
                        StaffList objStaff = new StaffList();
                        objStaff.ColumnName = DisplayText.Get(typeof(StaffColumnName), Staff[i]);
                        objStaff.Value = staffdata[0][Staff[i]].ToString();
                        objDayWiseShift.StaffList.Add(objStaff);
                    }
                }
                if (!string.IsNullOrEmpty(SplitStaffColumn))
                {
                    staffdata = dt.Select("StaffId = " + objDayWiseShift.SplitStaffId);

                    Staff = SplitStaffColumn.Split(',');
                    for (int i = 0; i < 3; i++)
                    {
                        StaffList objStaff = new StaffList();
                        objStaff.ColumnName = DisplayText.Get(typeof(StaffColumnName), Staff[i]);
                        objStaff.Value = staffdata[0][Staff[i]].ToString();
                        objDayWiseShift.SplitStaffList.Add(objStaff);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public OnCallMultidisciplinaryModel GetOnCallMultidisciplinary(string Date, string Departments, string Sites)
        {
            OnCallMultidisciplinaryModel objOnCallMultidisciplinaryModel = new OnCallMultidisciplinaryModel();
            Dictionary<string, object> Parameters = new Dictionary<string, object>();
            DataTable dt;
            try
            {
                Parameters.Add("@Sites", Sites);
                Parameters.Add("@Departments", Departments);
                Parameters.Add("@Date", CommonFunctions.ConvertToValidDateFormate(Date));
                dt = GetDataTableUsingSP("sp_GetOnCallMultidisciplinary", Parameters);
                if (CommonFunctions.IsValidDataTable(dt))
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        OnCallMultidisciplinaryModel objMulti = new OnCallMultidisciplinaryModel();
                        objMulti.MultidisciplinaryName = dt.Rows[i]["RotaTemplateName"].ToString();
                        objMulti.ShiftName = dt.Rows[i]["ShiftName"].ToString();
                        objMulti.DepartmentName = dt.Rows[i]["DepartmentName"].ToString();
                        string Extension = dt.Rows[i]["Extension"].ToString();
                        string Bleep = dt.Rows[i]["Bleep"].ToString();
                        if (!string.IsNullOrEmpty(Extension))
                        {
                            objMulti.Person = "Extension : " + Extension;
                        }
                        else if (!string.IsNullOrEmpty(Bleep))
                        {
                            objMulti.Person = "Bleep : " + Bleep;
                        }
                        else
                        {
                            objMulti.Person = "No Data";
                        }

                        objMulti.ContactNumber = dt.Rows[i]["Mobile"].ToString();
                        objOnCallMultidisciplinaryModel.OnCallMultidisciplinaryList.Add(objMulti);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return objOnCallMultidisciplinaryModel;
        }
    }
}
