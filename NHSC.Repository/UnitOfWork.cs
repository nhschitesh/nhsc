﻿using NHSC.Repository;
using NHSC.Models;
using NHSC.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

namespace NHSC.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DbContext db;
        public UnitOfWork()
        {
            db = new NHSCContext();
        }
        public void Dispose()
        {
            try
            {
                db.Dispose();
            }
            catch (Exception)
            {
                throw;
            }
        }

        private DbContextTransaction _trans;
        public DbContextTransaction trans
        {
            get
            {
                if (_trans == null)
                {
                    _trans = db.Database.BeginTransaction();
                }
                return _trans;
            }
        }

        private IUserRepository _User;
        public IUserRepository User
        {
            get
            {
                if (_User == null)
                {
                    _User = new UserRepository(db);
                }
                return _User;
            }
        }

        private IUserGroupRepository _UserGroup;
        public IUserGroupRepository UserGroup
        {
            get
            {
                if (_UserGroup == null)
                {
                    _UserGroup = new UserGroupRepository(db);
                }
                return _UserGroup;
            }
        }

        private ICommonRepository _Common;
        public ICommonRepository Common
        {
            get
            {
                if (_Common == null)
                {
                    _Common = new CommonRepository(db);
                }
                return _Common;
            }
        }


        private IBankHolidayRepository _BankHoliday;
        public IBankHolidayRepository BankHoliday
        {
            get
            {
                if (_BankHoliday == null)
                {
                    _BankHoliday = new BankHolidayRepository(db);
                }
                return _BankHoliday;
            }
        }


        private ISkillSetRepository _SkillSet;
        public ISkillSetRepository SkillSet
        {
            get
            {
                if (_SkillSet == null)
                {
                    _SkillSet = new SkillSetRepository(db);
                }
                return _SkillSet;
            }
        }


        private ISitesRepository _Sites;
        public ISitesRepository Sites
        {
            get
            {
                if (_Sites == null)
                {
                    _Sites = new SitesRepository(db);
                }
                return _Sites;
            }
        }


        private IDepartmentsRepository _Departments;
        public IDepartmentsRepository Departments
        {
            get
            {
                if (_Departments == null)
                {
                    _Departments = new DepartmentsRepository(db);
                }
                return _Departments;
            }
        }



        private IStaffRepository _Staff;
        public IStaffRepository Staff
        {
            get
            {
                if (_Staff == null)
                {
                    _Staff = new StaffRepository(db);
                }
                return _Staff;
            }
        }


        private IShiftRepository _Shift;
        public IShiftRepository Shift
        {
            get
            {
                if (_Shift == null)
                {
                    _Shift = new ShiftRepository(db);
                }
                return _Shift;
            }
        }


        private IColorRepository _Color;
        public IColorRepository Color
        {
            get
            {
                if (_Color == null)
                {
                    _Color = new ColorRepository(db);
                }
                return _Color;
            }
        }


        private ITitleRepository _Title;
        public ITitleRepository Title
        {
            get
            {
                if (_Title == null)
                {
                    _Title = new TitleRepository(db);
                }
                return _Title;
            }
        }

        private IRotaTemplateRepository _RotaTemplate;
        public IRotaTemplateRepository RotaTemplate
        {
            get
            {
                if (_RotaTemplate == null)
                {
                    _RotaTemplate = new RotaTemplateRepository(db);
                }
                return _RotaTemplate;
            }
        }

        private IMultidisciplinaryRotaRepository _MultidisciplinaryRota;
        public IMultidisciplinaryRotaRepository MultidisciplinaryRota
        {
            get
            {
                if (_MultidisciplinaryRota == null)
                {
                    _MultidisciplinaryRota = new MultidisciplinaryRotaRepository(db);
                }
                return _MultidisciplinaryRota;
            }
        }


        private IActualRotaRepository _ActualRota;
        public IActualRotaRepository ActualRota
        {
            get
            {
                if (_ActualRota == null)
                {
                    _ActualRota = new ActualRotaRepository(db);
                }
                return _ActualRota;
            }
        }

        private IOnCallRepository _OnCall;
        public IOnCallRepository OnCall
        {
            get
            {
                if (_OnCall == null)
                {
                    _OnCall = new OnCallRepository(db);
                }
                return _OnCall;
            }
        }

    }

}