﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHSC.Models;
using NHSC.Models.Entities;
using System.Data.Entity;

namespace NHSC.Repository
{
    public interface IUnitOfWork : IDisposable
    {
        DbContextTransaction trans { get; }
        IUserRepository User { get; }
        IUserGroupRepository UserGroup { get; }
        ICommonRepository Common { get; }
        IBankHolidayRepository BankHoliday { get; }
        ISkillSetRepository SkillSet { get; }
        ISitesRepository Sites { get; }
        IDepartmentsRepository Departments { get; }
        IShiftRepository Shift { get; }
        IStaffRepository Staff { get; }
        ITitleRepository Title { get; }
        IColorRepository Color { get; }
        IRotaTemplateRepository RotaTemplate { get; }
        IMultidisciplinaryRotaRepository MultidisciplinaryRota { get; }
        IActualRotaRepository ActualRota { get; }
        IOnCallRepository OnCall { get; }

    }
}
