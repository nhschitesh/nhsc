﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using NHSC.Models.Common;
using System.Data.Sql;
namespace NHSC.Repository
{
    public class NHSCContext : DbContext
    {
        public NHSCContext() : base(CommonFunctions.ConnectionString)
        {
            Database.SetInitializer<NHSCContext>(null);
            Database.CommandTimeout = 900;
            //((IObjectContextAdapter)this).ObjectContext.CommandTimeout = 900;
        }
    }
}
