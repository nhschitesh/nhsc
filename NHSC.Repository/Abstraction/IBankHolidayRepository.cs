﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHSC.Models.Entities;
using System.Web.Mvc;
using System.Data;

namespace NHSC.Repository
{
    public interface IBankHolidayRepository : IRepository<BankHolidayModel>
    {
        List<BankHolidayModel> GetList(CommonModel objPaging);
        bool ExistName(BankHolidayModel objValue);
        bool InsertData(BankHolidayModel objValue, ref string strErrorMsg);
        bool DeleteData(long Id, long ModifyBy, DateTime ModifyDate);
        BankHolidayModel GetDetails(long Id);
    }
}
