﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHSC.Models.Entities;
using System.Web.Mvc;
using System.Data;

namespace NHSC.Repository
{
    public interface IColorRepository : IRepository<ColorModel>
    {
        List<ColorModel> GetList(CommonModel objPaging);
        bool ExistName(ColorModel objValue);
        bool InsertData(ColorModel objValue, ref string strErrorMsg);
        bool DeleteData(long Id, long ModifyBy, DateTime ModifyDate);
        ColorModel GetDetails(long Id);
        List<SelectListItem> GetColor();
    }
}
