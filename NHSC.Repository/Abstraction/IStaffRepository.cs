﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHSC.Models.Entities;
using System.Web.Mvc;
using System.Data;

namespace NHSC.Repository
{
    public interface IStaffRepository : IRepository<StaffModel>
    {
        List<StaffModel> GetList(CommonModel objPaging);
        bool ExistName(StaffModel objValue);
        bool InsertData(StaffModel objValue, ref string strErrorMsg);
        bool AssignBukDepSkillData(StaffModel objValue, ref string strErrorMsg);
        bool DeleteData(long Id, long ModifyBy, DateTime ModifyDate);
        StaffModel GetDetails(long Id);
        StaffModel GetStaffOrder(long Id);
        List<SelectListItem> GetStaff(long UserId);
        DataTable GetStaffDDL();

        bool InsertStaffOrderData(StaffModel objStaff);
    }
}
