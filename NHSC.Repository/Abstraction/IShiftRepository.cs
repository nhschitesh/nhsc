﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHSC.Models.Entities;
using System.Web.Mvc;
using System.Data;

namespace NHSC.Repository
{
    public interface IShiftRepository : IRepository<ShiftModel>
    {
        List<ShiftModel> GetList(CommonModel objPaging);
        bool ExistStaffInSameShif(ShiftModel objValue);
        bool InsertData(ShiftModel objValue, ref string strErrorMsg);
        bool InsertDataShiftOrder(RotaShiftOrderModel objValue, ref string strErrorMsg);
        bool DeleteData(long Id, long ModifyBy, DateTime ModifyDate);
        ShiftModel GetDetails(long Id);
        List<SelectListItem> GetShift(long ShiftId);
        List<RotaShiftOrderModel> GetShiftByRota(long RoataTemplateId);

        
    }
}
