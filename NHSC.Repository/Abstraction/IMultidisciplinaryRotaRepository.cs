﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHSC.Models.Entities;
using System.Web.Mvc;
using System.Data;

namespace NHSC.Repository
{
    public interface IMultidisciplinaryRotaRepository : IRepository<MultidisciplinaryRotaModel>
    {
        List<MultidisciplinaryRotaModel> GetList(CommonModel objPaging);
        bool ExistName(MultidisciplinaryRotaModel objValue);
        bool InsertData(MultidisciplinaryRotaModel objValue, ref string strErrorMsg);
        bool DeleteData(long Id, long ModifyBy, DateTime ModifyDate);
        MultidisciplinaryRotaModel GetDetails(long Id);

        List<SelectListItem> GetMultidisciplinaryRota();
    }
}
