﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHSC.Models.Entities;
using System.Web.Mvc;
using System.Data;

namespace NHSC.Repository
{
    public interface IUserGroupRepository : IRepository<UserGroupModel>
    {
        List<UserGroupModel> GetList(CommonModel objPaging);
        bool ExistName(UserGroupModel objUser);
        bool InsertData(UserGroupModel objUser, ref string strErrorMsg);
        bool DeleteData(long Id, long ModifyBy, DateTime ModifyDate);
        UserGroupModel GetDetails(long Id);
        DataTable GetRights(long Id);
        List<SelectListItem> GetUserGroup();
    }
}
