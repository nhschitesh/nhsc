﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHSC.Models.Entities;
using System.Web.Mvc;
using System.Data;

namespace NHSC.Repository
{
    public interface IRotaTemplateRepository : IRepository<RotaTemplateModel>
    {
        List<RotaTemplateModel> GetList(CommonModel objPaging);
        bool ExistName(RotaTemplateModel objValue);
        bool InsertData(RotaTemplateModel objValue, ref string strErrorMsg);
        bool DeleteData(long Id, long ModifyBy, DateTime ModifyDate);
        RotaTemplateModel GetDetails(long Id);
        List<RotaTemplateModel> GetRotaTemplate(string SearchValue);
        bool IsAssignNewStaff(long Id);
        AssignRotaModel GetAssignRotaList(long MultidisciplinaryId, long UserId);
        DataTable GetAssignRotaListExport(long MultidisciplinaryId);
        bool AssignNewStaff(long AssignId, long StaffId, long ModifyBy, DateTime ModifyDate);
        bool UpdateStaff(RotaDayWiseShiftList objRota, long ModifyBy, DateTime ModifyDate);
        AssignRotaModel GetAssignRotaTemplateList(long RotaTemplateId,long UserId);
    }
}
