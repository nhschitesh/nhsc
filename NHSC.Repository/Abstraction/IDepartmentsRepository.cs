﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHSC.Models.Entities;
using System.Web.Mvc;
using System.Data;

namespace NHSC.Repository
{
    public interface IDepartmentsRepository : IRepository<DepartmentsModel>
    {
        List<DepartmentsModel> GetList(CommonModel objPaging);
        bool ExistName(DepartmentsModel objValue);
        bool InsertData(DepartmentsModel objValue, ref string strErrorMsg);
        bool DeleteData(long Id, long ModifyBy, DateTime ModifyDate);
        DepartmentsModel GetDetails(long Id);
        List<SelectListItem> GetDepartments();
        List<AssignDepartmentToUserModel> GetDepartmentList(CommonModel objPaging);
        bool AssignDepartmentToUser(UserModel Objuser, string DepartmentIds);
        string GetDepartmentIds(long Id);
    }
}
