﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHSC.Models.Entities;
using System.Web.Mvc;
using System.Data;

namespace NHSC.Repository
{
    public interface IActualRotaRepository : IRepository<ActualRotaModel>
    {
        ActualRotaModel GetAssignActualRotaList(long MultidisciplinaryId,long UserId);
        bool AssignNewStaff(StaffPopUp objStaffPopUp, long ModifyBy, DateTime ModifyDate);
         bool UnAssignStaff(StaffPopUp objStaffPopUp, long ModifyBy, DateTime ModifyDate);
        bool InsertActualRota(RotaTemplateModel objRota);
        StaffPopUp GetActualRota(long ActualRotaId);
        bool CheckActualSatffExits(StaffPopUp objStaffPopUp);
        bool CheckActualMainSatffExits(StaffPopUp objStaffPopUp);

        ActualRotaModel GetActualRotaList(long RotaTemplateId, long UserId);
    }
}
