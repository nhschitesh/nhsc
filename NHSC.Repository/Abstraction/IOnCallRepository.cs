﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHSC.Models.Entities;
using System.Web.Mvc;
using System.Data;

namespace NHSC.Repository
{
    public interface IOnCallRepository : IRepository<OnCallModel>
    {
        OnCallModel GetOnCallActualRotaList(string Date, string Departments, string Sites, long UserId);

        OnCallMultidisciplinaryModel GetOnCallMultidisciplinary(string Date, string Departments, string Sites);

        OnCallModel GetOnCallActualMultiRotaList(string Date, string Departments, string Sites,int MultiId,long UserId);
    }
}
