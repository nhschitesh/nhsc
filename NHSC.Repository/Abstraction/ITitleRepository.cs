﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHSC.Models.Entities;
using System.Web.Mvc;
using System.Data;

namespace NHSC.Repository
{
    public interface ITitleRepository : IRepository<TitleModel>
    {
        List<TitleModel> GetList(CommonModel objPaging);
        bool ExistName(TitleModel objValue);
        bool InsertData(TitleModel objValue, ref string strErrorMsg);
        bool DeleteData(long Id, long ModifyBy, DateTime ModifyDate);
        TitleModel GetDetails(long Id);
        List<SelectListItem> GetTitle();
    }
}
