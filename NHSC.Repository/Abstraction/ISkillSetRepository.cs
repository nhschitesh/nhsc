﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHSC.Models.Entities;
using System.Web.Mvc;
using System.Data;

namespace NHSC.Repository
{
    public interface ISkillSetRepository : IRepository<SkillSetModel>
    {
        List<SkillSetModel> GetList(CommonModel objPaging);
        bool ExistName(SkillSetModel objValue);
        bool InsertData(SkillSetModel objValue, ref string strErrorMsg);
        bool DeleteData(long Id, long ModifyBy, DateTime ModifyDate);
        SkillSetModel GetDetails(long Id);
        List<SelectListItem> GetSkillSet();
    }
}
