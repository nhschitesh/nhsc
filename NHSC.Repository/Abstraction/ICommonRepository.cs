﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHSC.Models.Entities;
using System.Web.Mvc;
using System.Data;

namespace NHSC.Repository
{
    public interface ICommonRepository : IRepository<CommonModel>
    {
        bool InsertErrorData(ErrorLogModel objError);
        bool InsertActivityLog(ActivityLogModel objActivity);
        List<ErrorLogModel> GetErrorLogList(CommonModel objPaging);
        ErrorLogModel GetErrorLogDetails(long Id);
        List<ActivityLogModel> GetActivityLogList(CommonModel objPaging);
        ActivityLogModel GetActivityLogDetails(long Id);
        bool IsAssignToAnyModule(string ModuleType, long Id, ref string strErrorMsg);
    }
}
