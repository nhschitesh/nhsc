﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHSC.Models.Entities;
using System.Web.Mvc;
using System.Data;

namespace NHSC.Repository
{
    public interface ISitesRepository : IRepository<SitesModel>
    {
        List<SitesModel> GetList(CommonModel objPaging);
        bool ExistName(SitesModel objValue);
        bool InsertData(SitesModel objValue, ref string strErrorMsg);
        bool DeleteData(long Id, long ModifyBy, DateTime ModifyDate);
        SitesModel GetDetails(long Id);
        List<SelectListItem> GetSites();
    }
}
