﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Sql;
using System.Linq.Expressions;

namespace NHSC.Repository
{
    public interface IRepository<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> GetAll();
        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate);
        TEntity Get(object Id);
        TEntity Add(TEntity entity, bool getEntity);
        IEnumerable<TEntity> AddRange(IEnumerable<TEntity> entities, bool getEntity);
        void Add(TEntity entity);
        void AddRange(IEnumerable<TEntity> entities);
        void Update(TEntity entity);
        void Remove(TEntity entity);
        void Remove(object Id);
        void RemoveRange(IEnumerable<TEntity> entities);
        DataTable GetDataTableUsingSP(string StoredProcedureName, Dictionary<string, object> Parameters);
        DataSet GetDataSetUsingSP(string StoredProcedureName, Dictionary<string, object> Parameters);
        DataTable GetDataTable(string pstrQuery);
        int ExecuteSP(string StoredProcedureName, Dictionary<string, object> Parameters);
    }
}
