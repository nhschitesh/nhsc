﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using NHSC.Models.Entities;

namespace NHSC.Repository
{
    public interface IUserRepository : IRepository<UserModel>
    {
        List<UserModel> GetList(CommonModel objPaging);
        bool ExistEmail(UserModel objUser);
        bool InsertData(UserModel objUser, ref string strErrorMsg);
        bool UpdateUserProfile(UserModel objUser);
        bool UpdateLastLogIn(UserModel objUser);
        bool DeleteData(long Id, long ModifyBy, DateTime ModifyDate);
        UserModel GetDetails(long Id);
        UserModel GetDetailsByEmail(string Email);
        UserModel ValidateUser(LogInModel objLogIn);
        long InsertForgotData(ForgotPassword objForgot);
        ForgotPassword GetForgotDetails(long Id);
        bool UpdatePassword(ChangePassword objChangePwd);
        List<SelectListItem> UserDepartmentList();
        List<SelectListItem> UserList();

    }
}
