﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NHSC.Models.Entities
{
    public class ActivityLogModel : CommonModel
    {
        public long UserId { get; set; }
        public long ActivityId { get; set; }
        public string ActivityLog { get; set; }
        public long RecordId { get; set; }
        public int MenuId { get; set; }
        public string MenuName { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string UserGroupName { get; set; }
    }
}
