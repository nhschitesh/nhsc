﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NHSC.Models.Entities
{
    public class CommonModel
    {
        public long RowNumber { get; set; }
        public DateTime ModifyDate { get; set; }
        public long ModifyBy { get; set; }
        public string Date { get; set; }

        public bool IsAllowEdit { get; set; }
        public long TotalCount { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string ShortColumnName { get; set; }
        public string SortColumnDirection { get; set; }
        public int Skip { get; set; }
        public string searchValue { get; set; }

        public int IsAllow { get; set; }

        public long Id { get; set; }

    }
}
