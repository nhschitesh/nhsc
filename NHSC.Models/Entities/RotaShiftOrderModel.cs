﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NHSC.Models.Entities
{
    public class RotaShiftOrderModel : CommonModel
    {

        public RotaShiftOrderModel()
        {
            this.ShiftOrderList = new List<RotaShiftOrderModel>();
        }
        public long ShiftOrderId { get; set; }
        public long RotaTemplateId { get; set; }
        public int OrderSeq { get; set; }
        public int ShiftId { get; set; }
        public string ShiftName { get; set; }
        public List<RotaShiftOrderModel> ShiftOrderList { get; set; }
    }
}
