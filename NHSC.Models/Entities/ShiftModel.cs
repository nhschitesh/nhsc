﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NHSC.Models.Entities
{
    public class ShiftModel : CommonModel
    {
        public long ShiftId { get; set; }
        public string ShiftName { get; set; }
        public int SkillSetId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public bool Active { get; set; }

        public int ActiveStatus { get; set; }
        public int MustBeFilledStatus { get; set; }

        public bool MustBeFilled { get; set; }
        public int ColorId { get; set; }
        public long StaffId { get; set; }

        public string StartTimes { get; set; }
        public string EndTimes { get; set; }
        public string Surname { get; set; }
        public string ColorName { get; set; }
        public string SkillSetName { get; set; }
        public bool Set24Hour { get; set; }

        //public bool IsAllowEdit { get; set; }
        public string[] DepartmentList { get; set; }
        public string DepartmentIds { get; set; }
    }
}
