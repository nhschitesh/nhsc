﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NHSC.Models.Entities
{
    public class OnCallMultidisciplinaryModel : CommonModel
    {
        public OnCallMultidisciplinaryModel()
        {
            this.OnCallMultidisciplinaryList = new List<OnCallMultidisciplinaryModel>();
        }
        public long MultidisciplinaryId { get; set; }
        public string MultidisciplinaryName { get; set; }
        public string ShiftName { get; set; }
        public string DepartmentName { get; set; }
        public string Person { get; set; }
        public string ContactNumber { get; set; }
        public string OnCallDates { get; set; }
        

        public List<OnCallMultidisciplinaryModel> OnCallMultidisciplinaryList { get; set; }
    }
}
