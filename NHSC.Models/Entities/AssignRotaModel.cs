﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NHSC.Models.Entities
{
    public class AssignRotaModel : CommonModel
    {
        public AssignRotaModel()
        {
            this.RotaList = new List<RotaList>();

        }
        public long MultidisciplinaryId { get; set; }
        public long RotaTemplateId { get; set; }
        public List<RotaList> RotaList { get; set; }
    }

    public class RotaList
    {
        public RotaList()
        {
            this.ShiftList = new List<ShiftList>();
            this.DaysList = new List<DaysList>();
        }
        public long RotaTemplateId { get; set; }
        public string RotaTemplateName { get; set; }

        public string RotaDate { get; set; }
        public bool IsAllowEdit { get; set; }

        public List<ShiftList> ShiftList { get; set; }
        public List<DaysList> DaysList { get; set; }

    }

    public class DaysList
    {
        public DaysList()
        {
            this.DayWiseShiftList = new List<RotaDayWiseShiftList>();
        }
        public long RotaTemplateId { get; set; }
        public int Day { get; set; }
        public string RotaDate { get; set; }
        public List<RotaDayWiseShiftList> DayWiseShiftList { get; set; }
    }
    public class RotaDayWiseShiftList
    {
        public RotaDayWiseShiftList()
        {
            this.StaffList = new List<StaffList>();
            this.SplitStaffList = new List<StaffList>();
        }

        public long RotaAssignId { get; set; }
        public long RotaTemplateId { get; set; }
        public int Day { get; set; }

        public string RotaDate { get; set; }

        public long StaffId { get; set; }
        public long ShiftId { get; set; }
        public bool IsChange { get; set; }
        public string ShiftName { get; set; }
        public int OrderSeq { get; set; }

        
        public string StaffName { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public long ActualRotaId { get; set; }
        public bool IsSplit { get; set; }

        public long SplitStaffId { get; set; }
        public string SplitStaffName { get; set; }
        public string SplitStartTime { get; set; }
        public string SplitEndTime { get; set; }
        public string HolidayName { get; set; }
        public string ColorName { get; set; }

        public List<StaffList> StaffList { get; set; }
        public List<StaffList> SplitStaffList { get; set; }
    }

    public class StaffList
    {
        public long StaffId { get; set; }
        public string ColumnName { get; set; }
        public string Value { get; set; }
    }
    public class ShiftList
    {
        public long RotaTemplateId { get; set; }
        public long ShiftId { get; set; }
        public string ShiftName { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public bool IsSplit { get; set; }
        public int OrderSeq { get; set; }

    }

    public class StaffPopUp
    {
        public StaffPopUp()
        {
            this.StaffList = new List<StaffList>();
            this.SplitStaffList = new List<StaffList>();
        }
        public long RotaAssignId { get; set; }
        public long ActualRotaId { get; set; }

        public string ActualRotaIds { get; set; }
        public string RotaAssignIds { get; set; }
        public long RotaTemplateId { get; set; }
        public long StaffId { get; set; }
        public bool IsSplit { get; set; }
        public bool Is24Hour { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }

        public long SplitStaffId { get; set; }
        public string SplitStartTime { get; set; }
        public string SplitEndTime { get; set; }

        public string RotaDate { get; set; }

        public string ActualStartTime { get; set; }
        public string ActualEndTime { get; set; }

        public int statusCode { get; set; }
        public string msg { get; set; }
        public string SplitStaffName { get; set; }
        public string StaffName { get; set; }
        public List<StaffList> StaffList { get; set; }
        public List<StaffList> SplitStaffList { get; set; }

    }
}
