﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NHSC.Models.Entities
{
    public class TitleModel : CommonModel
    {
        public int TitleId { get; set; }
        public string TitleName { get; set; }
    }
}
