﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NHSC.Models.Entities
{
    public class BankHolidayModel : CommonModel
    {
        public int BankHolidaysId { get; set; }
        public string HolidayName { get; set; }
        public DateTime HolidayDate { get; set; }
        public string HolidayDates { get; set; }
    }
}
