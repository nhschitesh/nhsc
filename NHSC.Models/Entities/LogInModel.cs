﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NHSC.Models.Entities
{
    public class LogInModel
    {
        public string UserName { get; set; }

        public bool KeepMeLogin { get; set; }
        public string Password { get; set; }
    }


    public class Rights
    {
        public Rights()
        {
        }
        public long RightId { get; set; }
        public int MenuId { get; set; }
        public int UserGroupId { get; set; }
        public bool Add { get; set; }
        public bool Edit { get; set; }
        public bool Delete { get; set; }
        public bool View { get; set; }
        public bool IsShowAll { get; set; }
        public bool IsShowInMenu { get; set; }

        public bool Print { get; set; }
        public int ParentId { get; set; }
        public int Sequence { get; set; }
        public string MenuName { get; set; }
        public string Action { get; set; }
        public string RoleName { get; set; }

    }

    public class ForgotPassword
    {
        public long ForgotId { get; set; }
        public long UserId { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public long ModifyBy { get; set; }

        public bool Status { get; set; }
        public DateTime ModifyDate { get; set; }
    }

    public class ChangePassword
    {
        public long UserId { get; set; }
        public string Email { get; set; }
        public string OldPassword { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public DateTime ModifyDate { get; set; }
        public long ModifyBy { get; set; }
        public long ForgotId { get; set; }
    }

}
