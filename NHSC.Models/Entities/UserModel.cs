﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NHSC.Models.Entities
{
    public class UserModel : CommonModel
    {
        public long UserId { get; set; }
        public string UserName { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool IsAlertsReceiver { get; set; }
        public int AlertsReceiver { get; set; }
        public int UserGroupId { get; set; }

        public bool UserStatus { get; set; }
    
        public bool IsAdmin { get; set; }

        public string UserGroupName { get; set; }
        public int Status { get; set; }
        public bool IsLogIn { get; set; }
        public string LastLogInDate { get; set; }
    }
}
