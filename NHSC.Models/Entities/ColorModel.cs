﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NHSC.Models.Entities
{
    public class ColorModel : CommonModel
    {
        public int ColorId { get; set; }
        public string ColorName { get; set; }
    }
}
