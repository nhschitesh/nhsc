﻿using System;
using System.Linq;
using System.Web;
using System.Security.Principal;
using System.Collections.Generic;

namespace NHSC.Security
{
    public class CustomPrincipal : IPrincipal
    {
        public IIdentity Identity { get; private set; }
        public bool IsInRole(string role)
        {
            return true;
        }
        public CustomPrincipal(string UserName)
        {
            this.Identity = new GenericIdentity(UserName);
            this.UserId = 1;
        }
        public long UserId { get; set; }
        public int Status { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public int UserGroupId { get; set; }
        public string GroupName { get; set; }
        public bool Add { get; set; }
        public bool Edit { get; set; }
        public bool Delete { get; set; }
        public bool View { get; set; }
        public bool Print { get; set; }
        public bool KeepMeLogin { get; set; }
        public bool IsShowAll { get; set; }

        public bool IsAdmin { get; set; }
        public bool IsShowInMenu { get; set; }

        public List<MenuList> MenuList { get; set; }
    }

    public class CustomPrincipalSerializeModel
    {

        public long UserId { get; set; }
        public int Status { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public int UserGroupId { get; set; }
        public string GroupName { get; set; }
        public bool Add { get; set; }
        public bool Edit { get; set; }
        public bool Delete { get; set; }
        public bool View { get; set; }
        public bool Print { get; set; }
        public bool KeepMeLogin { get; set; }
        public bool IsShowAll { get; set; }
        public bool IsShowInMenu { get; set; }
        public bool IsAdmin { get; set; }
        public List<MenuList> MenuList { get; set; }
    }


    public class MenuList
    {
        
                public long RightId { get; set; }
        public int MenuId { get; set; }
        public int UserGroupId { get; set; }
        public bool Add { get; set; }
        public bool Edit { get; set; }
        public bool Delete { get; set; }
        public bool View { get; set; }
        public bool Print { get; set; }
        public int ParentId { get; set; }
        public int Sequence { get; set; }
        public string MenuName { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string Icon { get; set; }

        public bool IsShowAll { get; set; }
        public bool IsShowInMenu { get; set; }
    }
}