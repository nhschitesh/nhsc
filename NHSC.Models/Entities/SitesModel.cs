﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NHSC.Models.Entities
{
    public class SitesModel : CommonModel
    {
        public int SiteId { get; set; }
        public string SiteName { get; set; }
    }
}
