﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NHSC.Models.Entities
{
    public class ErrorLogModel : CommonModel
    {
        public long UserId { get; set; }

        public long ErrorId { get; set; }
        public string ErrorMsg { get; set; }
        public string InnerException { get; set; }
        public string IpAddress { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public long LineNumber { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string UserGroupName { get; set; }

    }
}
