﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NHSC.Models.Entities
{
    public class RotaTemplateModel : CommonModel
    {
        public long RotaTemplateId { get; set; }
        public int SiteId { get; set; }
        public int DepartmentId { get; set; }

        public int StartDayOfWeek { get; set; }
        public string StartDayOfWeekName { get; set; }

        
        public int DurationInDays { get; set; }
        public int OldDurationInDays { get; set; }
        public string RotaTemplateName { get; set; }
        public DateTime GeneratedUpToDate { get; set; }
        public string GeneratedUpToDates { get; set; }
        public string ShiftIds { get; set; }
        public string OldShiftIds { get; set; }
        public string UserIds { get; set; }

        public string[] ShiftList { get; set; }
        public string[] UserList { get; set; }

        public string ShiftName { get; set; }
        public string UserName { get; set; }

        public string SiteName { get; set; }
        public string DepartmentName { get; set; }

        public string WeekDays { get; set; }
        
        public bool IsChange { get; set; }
    }
}
