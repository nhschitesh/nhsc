﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NHSC.Models.Entities
{
    public class DepartmentsModel : CommonModel
    {
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public int SiteId { get; set; }
        public string SiteName { get; set; }
    }
}
