﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NHSC.Models.Entities
{
    public class ActualRotaModel : CommonModel
    {
        public long MultidisciplinaryId { get; set; }
        public long RotaTemplateId { get; set; }
        public List<RotaList> RotaList { get; set; }

    }
}
