﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NHSC.Models.Entities
{
    public class OnCallModel : CommonModel
    {
        public OnCallModel()
        {
            this.RotaList = new List<RotaList>();
        }
        public long MultidisciplinaryId { get; set; }
        public long RotaTemplateId { get; set; }
        public DateTime OnCallDate { get; set; }
        public string OnCallDates { get; set; }
        public string OnCallFullDates { get; set; }


        public string DepartmentIds { get; set; }
        public string SiteIds { get; set; }

        public List<RotaList> RotaList { get; set; }

    }
}
