﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NHSC.Models.Entities
{
    public class SkillSetModel : CommonModel
    {
        public int SkillSetId { get; set; }
        public string SkillSetName { get; set; }
    }
}
