﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NHSC.Models.Entities
{
    public class StaffModel : CommonModel
    {

        public StaffModel()
        {
            this.StaffOrderList = new List<StaffOrderModel>();
        }
        public long StaffId { get; set; }
        public string StaffIds { get; set; }

        public long SkillSet { get; set; }
        public long Department { get; set; }

        public string[] SkillSetList { get; set; }
        public string[] DepartmentList { get; set; }

        public string SkillSetIds { get; set; }
        public string DepartmentIds { get; set; }
        public string SkillSetName { get; set; }
        public string DepartmentName { get; set; }
        public int Title { get; set; }
        public int TitleId { get; set; }
        public string TitleName { get; set; }
        public string Surname { get; set; }
        public string Forename { get; set; }
        public string Extension { get; set; }
        public string Bleep { get; set; }
        public string Pager { get; set; }
        public string Home { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string AltExtn { get; set; }
        public string AltBleep { get; set; }
        public string AltPager { get; set; }
        public string AltMobile { get; set; }
        public string OtherPrivate { get; set; }
        public string AltEmail { get; set; }
        public string ContactOrder { get; set; }
        public bool IsAllowEdit { get; set; }
        
        public List<StaffOrderModel> StaffOrderList { get; set; }
    }
}
