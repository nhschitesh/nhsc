﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NHSC.Models.Entities
{
    public class StaffOrderModel : CommonModel
    {
        public long StaffId { get; set; }
        public long StaffOrderId { get; set; }
        public int OrderSeq { get; set; }
        public bool IsShowColumn { get; set; }
        public string ColumnKey { get; set; }
        public string ColumnName { get; set; }
    }
}
