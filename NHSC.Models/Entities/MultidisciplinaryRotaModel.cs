﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NHSC.Models.Entities
{
    public class MultidisciplinaryRotaModel : CommonModel
    {
        public long MultidisciplinaryId { get; set; }
        public string MultidisciplinaryName { get; set; }
        public string RotaTemplateIds { get; set; }
        public string UserIds { get; set; }

        public string[] RotaTemplateList { get; set; }
        public string[] UserList { get; set; }

        public string RotaTemplateName { get; set; }
        public string UserName { get; set; }

    }
}
