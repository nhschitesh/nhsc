﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NHSC.Models.Entities
{
    public class AssignDepartmentToUserModel : CommonModel
    {
        public long DepAssignId { get; set; }
        public long UserId { get; set; }
        public int DepartmentId { get; set; }
        public string DepartmentIds { get; set; }
        public string FullName { get; set; }
        public string DepartmentName { get; set; }
        public string SiteName { get; set; }

        public bool isAssign { get; set; }
    }
}
