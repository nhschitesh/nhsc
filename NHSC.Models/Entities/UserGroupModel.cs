﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NHSC.Models.Entities
{
    public class UserGroupModel : CommonModel
    {
        public UserGroupModel()
        {
            this.RightsList = new List<Rights>();
        }
        public int UserGroupId { get; set; }
        public string UserGroupName { get; set; }

        public bool IsChange { get; set; }

        public bool IsDepartmentAdmin { get; set; }

        public List<Rights> RightsList { get; set; }
    }
}
