﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Web;
using System.Data;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Configuration;
using NHSC.Models.Entities;
using System.Net;
using System.Net.Mail;
using System.Threading;
using NHSC.Security;
using System.ComponentModel;
using NHSC.Common;

namespace NHSC.Models.Common
{
    public class CommonFunctions
    {
        #region Comman Global Properties

        private static string[] InvalidStringSymbol = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "`", "~", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "-", "_", "=", "+", "[", "]", "{", "}", @"\", "|", ";", ":", "'", "<", ">", ",", ".", "/", "?" };
        public static string ApplicationDateFormat { get; private set; }
        public static string ConnectionString { get; private set; }
        public static string SessionIdCookieKeyName { get; set; }
        public static string LastLoginTimeCookieKeyName
        {
            get
            {
                return "cookLastLoginTime";
            }
        }
        public static string ChangeSessionId { get; set; }
        public static string EncryptSession { get; set; }
        public static string SendFromEmail { get; private set; }
        public static string SendFromPassword { get; private set; }
        public static int Port { get; private set; }
        public static string SMTP { get; private set; }
        public static string BCCEmail { get; private set; }

        #endregion

        #region Common Methods
        public static void ApplicationStart()
        {
            try
            {
                ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;

                SessionStateSection sessionStateSection = (SessionStateSection)ConfigurationManager.GetSection("system.web/sessionState");
                SessionIdCookieKeyName = sessionStateSection.CookieName;
                ChangeSessionId = ConfigurationManager.AppSettings["ChangeSessionId"]?.ToString()?.ToUpper() ?? "Y";
                EncryptSession = ConfigurationManager.AppSettings["EncryptSession"]?.ToString()?.ToUpper() ?? "Y";
                SendFromEmail = ConfigurationManager.AppSettings["SendFromEmail"];
                SendFromPassword = ConfigurationManager.AppSettings["SendFromPassword"];
                Port = ValidIntValue(ConfigurationManager.AppSettings["Port"]);
                SMTP = ConfigurationManager.AppSettings["SMTP"];
                BCCEmail = ConfigurationManager.AppSettings["BCCEmail"];


                #region Create folder if not exists
                var uploadFolderPath = HttpContext.Current.Server.MapPath("~/Uploads");
                if (!System.IO.Directory.Exists(uploadFolderPath))
                {
                    System.IO.Directory.CreateDirectory(uploadFolderPath);
                }
                var logsPath = HttpContext.Current.Server.MapPath("~/Logs");
                if (!System.IO.Directory.Exists(logsPath))
                {
                    System.IO.Directory.CreateDirectory(logsPath);
                }
                #endregion
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static string FormatToApplicationDateFormat(string date)
        {
            DateTime dtTemp;
            if (DateTime.TryParse(date, out dtTemp))
            {
                date = DateTime.Parse(date).ToString(CommonFunctions.ApplicationDateFormat);
            }
            return date;
        }
        public static string FormatToSQLServerDateFormat(string date, string clientDateFormat, string sqlDateFormat)
        {
            try
            {
                DateTime dtTemp;
                if (sqlDateFormat.StartsWith("mdy"))
                {
                    sqlDateFormat = "MM/dd/yyyy";
                }
                else
                {
                    sqlDateFormat = "dd/MM/yyyy";
                }

                if (DateTime.TryParseExact(date, clientDateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out dtTemp))
                {
                    return dtTemp.ToString(sqlDateFormat);
                }
                return null;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static string CreateFilterQuery(string ColumnName, string SearchValue, string DateColumn, string TimeColumn, string Date = "")
        {
            StringBuilder sqlqry = new StringBuilder();
            string[] arrysearch;
            string[] arrycolumn;
            string[] arrydatecolumn;
            string[] arrytimecolumn;
            try
            {

                if (!string.IsNullOrEmpty(SearchValue))
                {
                    arrycolumn = ColumnName.Split(',');
                    foreach (var column in arrycolumn)
                    {
                        if (!string.IsNullOrEmpty(column))
                        {
                            arrysearch = SearchValue.Split(';');
                            if (arrysearch != null && arrysearch.Count() > 0)
                            {
                                foreach (var value in arrysearch)
                                {
                                    if (!string.IsNullOrEmpty(value))
                                    {
                                        if (!string.IsNullOrEmpty(sqlqry.ToString()))
                                        {
                                            sqlqry.Append(" or  " + column + " Like &%" + value + "%&");
                                        }
                                        else
                                        {
                                            sqlqry.Append(" " + column + " Like &%" + value + "%&");
                                        }
                                    }
                                }
                            }
                        }
                    }

                    arrydatecolumn = DateColumn.Split(',');

                    foreach (var column in arrydatecolumn)
                    {
                        if (!string.IsNullOrEmpty(column))
                        {
                            arrysearch = SearchValue.Split(';');
                            if (arrysearch != null && arrysearch.Count() > 0)
                            {
                                foreach (var value in arrysearch)
                                {
                                    if (!string.IsNullOrEmpty(value))
                                    {
                                        if (!string.IsNullOrEmpty(sqlqry.ToString()))
                                        {
                                            sqlqry.Append(" or  " + "FORMAT (" + column + ", &dd-MM-yy HH:mm:ss&)" + " Like &%" + value + "%&");
                                        }
                                        else
                                        {
                                            sqlqry.Append(" " + "FORMAT (" + column + ", &dd-MM-yy HH:mm:ss&)" + " Like &%" + value + "%&");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    arrytimecolumn = TimeColumn.Split(',');

                    foreach (var column in arrytimecolumn)
                    {
                        if (!string.IsNullOrEmpty(column))
                        {
                            arrysearch = SearchValue.Split(';');
                            if (arrysearch != null && arrysearch.Count() > 0)
                            {
                                foreach (var value in arrysearch)
                                {
                                    if (!string.IsNullOrEmpty(value))
                                    {
                                        if (!string.IsNullOrEmpty(sqlqry.ToString()))
                                        {
                                            sqlqry.Append(" or  " + "FORMAT (" + column + ", &HH:mm&)" + " Like &%" + value + "%&");
                                        }
                                        else
                                        {
                                            sqlqry.Append(" " + "FORMAT (" + column + ", &HH:mm&)" + " Like &%" + value + "%&");
                                        }
                                    }
                                }
                            }
                        }
                    }

                    arrydatecolumn = Date.Split(',');

                    foreach (var column in arrydatecolumn)
                    {
                        if (!string.IsNullOrEmpty(column))
                        {
                            arrysearch = SearchValue.Split(';');
                            if (arrysearch != null && arrysearch.Count() > 0)
                            {
                                foreach (var value in arrysearch)
                                {
                                    if (!string.IsNullOrEmpty(value))
                                    {
                                        if (!string.IsNullOrEmpty(sqlqry.ToString()))
                                        {
                                            sqlqry.Append(" or  " + "FORMAT (" + column + ", &dd-MM-yy&)" + " Like &%" + value + "%&");
                                        }
                                        else
                                        {
                                            sqlqry.Append(" " + "FORMAT (" + column + ", &dd-MM-yy&)" + " Like &%" + value + "%&");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return sqlqry.ToString();
        }

        #endregion

        #region User Login
        public CustomPrincipal LoginUserDetails
        {
            get
            {
                CustomPrincipal objUserDetails = null;
                if (HttpContext.Current.Session[AppConstants.UserSession] != null)
                {
                    objUserDetails = (CustomPrincipal)HttpContext.Current.Session[AppConstants.UserSession];
                }
                return objUserDetails;
            }
        }

        public static bool Add
        {
            get
            {
                if (HttpContext.Current.Session[AppConstants.AddRecord] != null)
                {
                    return (bool)HttpContext.Current.Session[AppConstants.AddRecord];
                }
                return false;
            }
        }
        public static bool Edit
        {
            get
            {
                if (HttpContext.Current.Session[AppConstants.EditRecord] != null)
                {
                    return (bool)HttpContext.Current.Session[AppConstants.EditRecord];
                }
                return false;
            }
        }
        public static bool Delete
        {
            get
            {
                if (HttpContext.Current.Session[AppConstants.DeleteRecord] != null)
                {
                    return (bool)HttpContext.Current.Session[AppConstants.DeleteRecord];
                }
                return false;
            }
        }
        public static bool ShowAll
        {
            get
            {
                if (HttpContext.Current.Session[AppConstants.IsShowAll] != null)
                {
                    return (bool)HttpContext.Current.Session[AppConstants.IsShowAll];
                }
                return false;
            }
        }
        #endregion

        #region Date Time Setting
        public static DateTime DatetimesettingUTC(string TimeZone, string Date)
        {
            TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById(TimeZone);
            DateTime cstTime = TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(Date), cstZone);
            return cstTime;
        }
        public static DateTime Datetimesetting(string TimeZone)
        {
            TimeZoneInfo timeZoneInfo;
            DateTime dateTime;
            timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(TimeZone);
            dateTime = TimeZoneInfo.ConvertTime(DateTime.Now, timeZoneInfo);
            return dateTime;
        }
        public static DateTime Datetimesetting()
        {
            TimeZoneInfo timeZoneInfo;
            DateTime dateTime;
            timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("GMT Standard Time");
            dateTime = TimeZoneInfo.ConvertTime(DateTime.Now, timeZoneInfo);
            return dateTime;
            //return DateTime.Now;
        }
        public static DateTime DatetimeUTC()
        {
            return DateTime.UtcNow;
        }
        public static DateTime DatetimeConvertToUTC(string TimeZone, string Date)
        {
            TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById(TimeZone);
            DateTime cstTime = TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(Date), cstZone);
            return cstTime;
        }
        #endregion

        #region Email 
        public void SendForgotPasswordEmail(ForgotPassword ObjForgetDetails)
        {
            if (ObjForgetDetails.ForgotId > 0)
            {
                string CCMailID = string.Empty;
                string Subject = "Forgot Password";
                string FromMail = CommonFunctions.SendFromEmail;
                string ToMailID = ObjForgetDetails.Email;

                string _path = HttpContext.Current.Server.MapPath(@"~/html/forgotpassword.html");
                var _template = File.ReadAllText(_path);
                string MailBoday = _template.Replace("#user_name", ObjForgetDetails.UserName);
                MailBoday = MailBoday.Replace("#user_email", ObjForgetDetails.Email);
                MailBoday = MailBoday.Replace("#user_href", CommonFunctions.GetWebConfigValue("ForgotPasswordLink") + Encrypt(ObjForgetDetails.ForgotId.ToString()));
                MailBoday = MailBoday.Replace("#logo_image", CommonFunctions.GetWebConfigValue("BaseUrl") + AppConstants.ProductLogoEmail);
                Thread email = new Thread(delegate ()
                {
                    Mail_Code(Subject, MailBoday, FromMail, ToMailID);
                });

                email.IsBackground = true;
                email.Start();
            }
        }
        public void Mail_Code(string Subject, string MailBoday, string FromMail, string ToMailID)
        {
            try
            {
                System.Net.ServicePointManager.Expect100Continue = false;
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                // Command line argument must the the SMTP host.
                SmtpClient client = new SmtpClient();
                NetworkCredential NetworkCred = null;
                client.Timeout = 1 * 60 * 1000;

                client.Host = SMTP;
                client.EnableSsl = true;
                NetworkCred = new NetworkCredential(FromMail, SendFromPassword);
                client.Port = Port;
                client.UseDefaultCredentials = true;
                client.Credentials = NetworkCred;

                // Specify the e-mail sender.
                // Create a mailing address that includes a UTF8 character
                // in the display name.
                MailAddress from = new MailAddress(FromMail, AppConstants.EmailProductDisplayName);
                // Set destinations for the e-mail message.
                MailAddress to = new MailAddress(ToMailID);
                // Specify the message content.
                MailMessage message = new MailMessage(from, to);
                message.Body = MailBoday;
                // Include some non-ASCII characters in body and subject.
                message.Subject = Subject;
                message.IsBodyHtml = true;
                // Set the method that is called back when the send operation ends.
                client.SendCompleted += new
                SendCompletedEventHandler(SendCompletedCallback);
                // The userState can be any object that allows your callback 
                // method to identify this send operation.
                // For this example, the userToken is a string constant.
                string userState = "";
                Console.WriteLine("Message Initlize." + DateTime.Now);
                client.SendAsync(message, userState);
                // If the user canceled the send, and mail hasn't been sent yet,
                // then cancel the pending operation.
                //client.SendAsyncCancel();
                // Clean up.
            }
            catch (Exception)
            {
            }
        }
        private static void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
        {
            // Get the unique identifier for this asynchronous operation.
            //String token = (string)e.UserState;
            //if (!string.IsNullOrEmpty(token))
            //{
            //}
            //if (e.Error != null || e.Cancelled)
            //{
            //    Console.WriteLine("[{0}] {1}", token, e.Error.ToString());
            //}
            //else
            //{
            //    Console.WriteLine("Message sent." + token + "  " + DateTime.Now.ToString());
            //}
            //mailSent = true;
        }
        #endregion

        #region Other
        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        public static bool CheckDate(string dateString)
        {
            try
            {
                DateTime date = DateTime.Parse(dateString);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static bool IsValidMethod(string strInput)
        {
            try
            {
                if ((Convert.ToString(strInput) == ""))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static void WriteLog(string strLog)
        {
            try
            {
                string logFilePath = HttpContext.Current.Server.MapPath("~") + "\\Logs\\";
                if (!Directory.Exists(logFilePath))
                {
                    Directory.CreateDirectory(logFilePath);
                }
                logFilePath = logFilePath + "Log-" + System.DateTime.Today.ToString("MM-dd-yyyy") + "." + "txt";
                System.IO.File.AppendAllText(logFilePath, strLog + "  at Time " + DateTime.Now + Environment.NewLine);
            }
            catch (Exception)
            {
                //throw;
            }
        }
        public static bool IsValidDataTable(DataTable dtData)
        {
            try
            {
                if (dtData != null && dtData.Rows.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static string GetUserAddress()
        {
            try
            {
                System.Web.HttpContext context = System.Web.HttpContext.Current;
                string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

                if (!string.IsNullOrEmpty(ipAddress))
                {
                    string[] addresses = ipAddress.Split(',');
                    if (addresses.Length != 0)
                    {
                        return addresses[0];
                    }
                }

                return context.Request.ServerVariables["REMOTE_ADDR"].ToString();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static string ValidateString(string strInput)
        {
            try
            {
                if (string.IsNullOrEmpty(strInput))
                {
                    return "";
                }
                else
                {
                    return strInput.Trim();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static bool IsValidFlieExtension(HttpPostedFileBase[] files, ref string FileName)
        {
            try
            {
                if (files != null)
                {
                    foreach (HttpPostedFileBase file in files)
                    {
                        string fileextension = Path.GetExtension(Convert.ToString(file.FileName));
                        if (fileextension == ".xlsx" || fileextension == ".xls" || fileextension == ".doc" ||
                           fileextension == ".docx" || fileextension == ".pdf" || fileextension == ".zip" || fileextension == ".rar")
                        {
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(FileName))
                            {
                                FileName = file.FileName;
                            }
                            else
                            {
                                FileName = FileName + "," + file.FileName;
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            if (string.IsNullOrEmpty(FileName))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool IsValidFlieExtension(string fileextension)
        {
            try
            {
                if (fileextension == ".xlsx" || fileextension == ".xls" || fileextension == ".doc" ||
                       fileextension == ".docx" || fileextension == ".pdf" || fileextension == ".zip" || fileextension == ".rar")
                {
                    return true;
                }

                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static bool IsSecureConn()
        {
            string strVal = "";
            bool blnReturn = false;
            try
            {
                strVal = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_PROTO"] + "";
                if (strVal.ToUpper() == "HTTPS")
                {
                    blnReturn = true;
                }
                return blnReturn;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static string GetWebConfigValue(string strKey)
        {
            string strReturnVal;
            try
            {
                strReturnVal = WebConfigurationManager.AppSettings[strKey];
                return strReturnVal;
            }
            catch (Exception)
            {
                return "";
            }
        }
        public static DataTable ConvertIEnumerableToDataTable<TSource>(IEnumerable<TSource> source)
        {
            var dtData = new DataTable();
            try
            {
                if (source != null)
                {
                    var props = typeof(TSource).GetProperties();

                    dtData.Columns.AddRange(
                      props.Select(p => new DataColumn(p.Name, Nullable.GetUnderlyingType(p.PropertyType) ?? p.PropertyType)).ToArray()
                    );

                    source.ToList().ForEach(
                      i => dtData.Rows.Add(props.Select(p => p.GetValue(i, null)).ToArray())
                    );
                }

                return dtData;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static long? GetNullableTryParselong(string input)
        {
            long outValue;
            return Int64.TryParse(input, out outValue) ? (long?)outValue : null;
        }
        public static byte[] GetFileBytes(string strFilePath)
        {
            try
            {
                byte[] fileBytes = System.IO.File.ReadAllBytes(strFilePath);
                return fileBytes;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public static bool IsNumber(string input)
        {
            try
            {
                int test;
                return int.TryParse(input, out test);
            }
            catch (Exception)
            {
                return false;
            }

        }

        public static string SetQuote(string pstrValue)
        {
            string strReturnValue = string.Empty;
            try
            {
                strReturnValue = pstrValue.Replace("'", "''");
                return strReturnValue;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static string MapPathSetting(string path)
        {
            return HttpContext.Current.Server.MapPath("~") + path;
        }
        public static void CreateDirectory(string FilePath)
        {
            try
            {
                if (!Directory.Exists(CommonFunctions.MapPathSetting(FilePath)))
                {
                    Directory.CreateDirectory(CommonFunctions.MapPathSetting(FilePath));
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static void SaveImage(string FilePath, string FileFullPath, HttpPostedFileBase Images)
        {
            try
            {
                if (Images != null)
                {
                    CreateDirectory(FilePath);
                    Images.SaveAs(FileFullPath);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static string GetFilePath(string filename, string filepath)
        {
            string FullFilePath = string.Empty;
            try
            {
                if (filename != null)
                {
                    if (System.IO.File.Exists(MapPathSetting(filepath + "/" + filename)))
                    {
                        //objBranchBO.mst_branch_image = filepath + "/" + filename;
                        FullFilePath = BaseURL + ReplaceURLLink(filepath) + "/" + filename;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return FullFilePath;
        }
        public static string BaseURL
        {
            get
            {
                return WebConfigurationManager.AppSettings["BaseUrl"].ToString();
            }
        }
        public static string ReplaceURLLink(string link)
        {
            return link.Replace("\\", "/");
        }
        public static string ApplicationURL
        {
            get
            {
                return WebConfigurationManager.AppSettings["Application"].ToString();
            }
        }
        public static string Encrypt(string clearText)
        {
            try
            {
                string EncryptionKey = "TkhTQ0luZm9Tb2Z0TkkwMTIzNDU2Nzg5";
                byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(clearBytes, 0, clearBytes.Length);
                            cs.Close();
                        }
                        clearText = Convert.ToBase64String(ms.ToArray());
                    }
                }
            }
            catch (Exception)
            {
            }
            return clearText;
        }
        public static string Decrypt(string cipherText)
        {
            try
            {
                string EncryptionKey = "TkhTQ0luZm9Tb2Z0TkkwMTIzNDU2Nzg5";
                byte[] cipherBytes = Convert.FromBase64String(cipherText);
                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(cipherBytes, 0, cipherBytes.Length);
                            cs.Close();
                        }
                        cipherText = Encoding.Unicode.GetString(ms.ToArray());
                    }
                }
            }
            catch (Exception)
            {
                cipherText = "";
            }
            return cipherText;
        }
        public static int TableSkipRecord(int value, int? CurrentPageSize)
        {
            if (CurrentPageSize.HasValue)
            {
                value = value == 0 ? 0 : (value - 1) * (CurrentPageSize.Value);
            }
            return value;
        }
        public static string SetFixLength(string Value, string For)
        {
            string FixLengthString = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(Value))
                {
                    //if (AppConstants.ProductName == For)
                    //{
                    //    if (Value.Length > AppConstants.ProductNameLength)
                    //    {
                    //        FixLengthString = Value.Substring(0, AppConstants.ProductNameLength);
                    //    }
                    //}
                    FixLengthString = FixLengthString + "...";
                }
            }
            catch (Exception)
            {
                throw;
            }
            return FixLengthString;
        }
        public static int ValidIntValue(string Value)
        {
            int OrgnalValue = 0;
            try
            {
                if (!string.IsNullOrEmpty(Value))
                {
                    OrgnalValue = Convert.ToInt32(Value);
                }
            }
            catch (Exception)
            {
                return OrgnalValue;
            }
            return OrgnalValue;
        }
        public static long ValidlongValue(string Value)
        {
            long OrgnalValue = 0;
            try
            {
                if (!string.IsNullOrEmpty(Value))
                {
                    OrgnalValue = Convert.ToInt64(Value);
                }
            }
            catch (Exception)
            {
                return OrgnalValue;
            }
            return OrgnalValue;
        }
        public static bool ValidBoolValue(string Value)
        {
            bool OrgnalValue = false;
            try
            {
                if (!string.IsNullOrEmpty(Value))
                {
                    OrgnalValue = Convert.ToBoolean(Value);
                }
            }
            catch (Exception)
            {
                return OrgnalValue;
            }
            return OrgnalValue;
        }
        public static int ValidIntBoolValue(string Value)
        {
            int OrgnalValue = 0;
            try
            {
                if (!string.IsNullOrEmpty(Value))
                {
                    if (Value == "True")
                    {
                        OrgnalValue = 1;
                    }
                    else
                    {
                        OrgnalValue = 0;
                    }
                }
            }
            catch (Exception)
            {
                return OrgnalValue;
            }
            return OrgnalValue;
        }

        public static bool ValidStringIntBoolValue(string Value)
        {
            bool OrgnalValue = false;
            try
            {
                if (!string.IsNullOrEmpty(Value))
                {
                    if (Value == "1")
                    {
                        OrgnalValue = true;
                    }
                    else
                    {
                        OrgnalValue = false;
                    }
                }
            }
            catch (Exception)
            {
                return OrgnalValue;
            }
            return OrgnalValue;
        }
        public static decimal ValidDecimalValue(string Value)
        {
            decimal OrgnalValue = 0;
            try
            {
                if (!string.IsNullOrEmpty(Value))
                {
                    OrgnalValue = Convert.ToDecimal(Value);
                }
            }
            catch (Exception)
            {
                return OrgnalValue;
            }
            return OrgnalValue;
        }
        public static int CalculateAge(DateTime? dateOfBirth)
        {
            int age = 0;
            age = DateTime.Now.Year - dateOfBirth.Value.Year;
            if (DateTime.Now.DayOfYear < dateOfBirth.Value.DayOfYear)
                age = age - 1;

            return age;
        }
        public static bool IsValidString(string strInput)
        {
            try
            {
                strInput = strInput ?? "";
                if (!string.IsNullOrEmpty(strInput))
                {
                    if (InvalidStringSymbol.Any(strInput.Contains))
                    {
                        return false;
                    }
                }
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static bool ValidImportFileExtionsion(string strExtension)
        {
            bool IsValid = false;
            try
            {
                if (strExtension == ".xls" || strExtension == ".xlsx" || strExtension == ".csv")
                {
                    IsValid = true;
                }
            }
            catch (Exception)
            {
                return IsValid;
            }
            return IsValid;
        }
        public static void SaveFile(string FilePath, string FileFullPath, HttpPostedFileBase file)
        {
            try
            {
                if (file != null)
                {
                    //CreateDirectory(FilePath);
                    file.SaveAs(FileFullPath);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static string ConvertToValidDateFormate(string date)
        {
            string[] arry = date.Split('-');
            string monthName = DisplayText.Get(typeof(MonthName), CommonFunctions.ValidIntValue(arry[1]));
            return arry[0] + "-" + monthName + "-" + arry[2];
        }
        #endregion
    }
}
