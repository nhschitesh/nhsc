﻿using System;
using System.Collections.Generic;
using System.Reflection;
using NHSC.Resources;
using System.Web.Mvc;
using NHSC.Models.Common;

namespace NHSC.Common
{
    public class @enum
    {

    }
    public class DisplayText : Attribute
    {
        private string strText;
        public DisplayText(string strText)
        {
            this.strText = string.Format(strText, /*Resources.lblProductName*/"");
        }
        public string Text { get { return strText; } set { strText = value; } }
        public static string Get(object enm)
        {
            try
            {
                if (enm != null)
                {
                    MemberInfo[] objMI = enm.GetType().GetMember(enm.ToString());
                    if (objMI != null && objMI.Length > 0)
                    {
                        DisplayText attr = Attribute.GetCustomAttribute(objMI[0], typeof(DisplayText)) as DisplayText;
                        if (attr != null)
                        {
                            return attr.strText;
                        }
                    }
                }
                return null;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static string Get(Type enumType, string name)
        {
            if (enumType != null)
            {
                MemberInfo[] objMI = enumType.GetMember(name);
                if (objMI != null && objMI.Length > 0)
                {
                    DisplayText attr = Attribute.GetCustomAttribute(objMI[0], typeof(DisplayText)) as DisplayText;
                    if (attr != null)
                    {
                        return attr.strText;
                    }
                }
            }
            return null;
        }
        public static string Get(Type enumType, int value)
        {
            if (enumType != null)
            {
                string name = enumType.GetEnumName(value);
                if (name != null)
                {
                    MemberInfo[] objMI = enumType.GetMember(name);
                    if (objMI != null && objMI.Length > 0)
                    {
                        DisplayText attr = Attribute.GetCustomAttribute(objMI[0], typeof(DisplayText)) as DisplayText;
                        if (attr != null)
                        {
                            return attr.strText;
                        }
                    }
                }
            }

            return null;
        }
    }
    public class SelectListControl
    {

        public static IEnumerable<SelectListItem> GetWeekName()
        {
            var selectList = new List<SelectListItem>();

            // Get all values of the Industry enum
            var enumValues = Enum.GetValues(typeof(WeekName)) as WeekName[];
            if (enumValues == null)
                return null;

            foreach (WeekName enumValue in enumValues)
            {
                // Create a new SelectListItem element and set its 
                // Value and Text to the enum value and description.
                selectList.Add(new SelectListItem
                {
                    Value = ((int)enumValue).ToString(),
                    // GetWeekName just returns the Display.Name value
                    // of the enum - check out the next chapter for the code of this function.
                    Text = DisplayText.Get(typeof(WeekName), enumValue.ToString()),
                });
            }
            return selectList;
        }

        //public static List<SelectListItem> abc()
        //{
        //    List<SelectListItem> list = new List<SelectListItem>();
        //    try
        //    {
        //        list.Insert(0, new SelectListItem() { Text = "", Value = "" });
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //    return list;
        //}

    }
    public enum RecordStatus
    {
        [DisplayText("Not Active")]
        DeActive = 0,
        [DisplayText("Active")]
        Active = 1,
    }

    public enum YesNo
    {
        [DisplayText("No")]
        No = 0,
        [DisplayText("Yes")]
        Yes = 1,
    }
    public enum Menu
    {
        [DisplayText("Dashboard")]
        Dashboard = 1,
        [DisplayText("Users")]
        Users = 2,
        [DisplayText("User")]
        User = 3,
        [DisplayText("User Group")]
        UserGroup = 4,
        [DisplayText("Setting")]
        Setting = 5,
        [DisplayText("Bank Holiday")]
        BankHoliday = 6,
        [DisplayText("Departments")]
        Departments = 7,
        [DisplayText("Sites")]
        Sites = 8,
        [DisplayText("SkillSet")]
        SkillSet = 9,
        [DisplayText("Rota")]
        Rota = 10,
        [DisplayText("Template Rota")]
        TemplateRota = 11,
        [DisplayText("Multidisciplinary Actual Rota Template")]
        MultidisciplinaryActualRotaTemplate = 12,
        [DisplayText("Multidisciplinary Rota")]
        MultidisciplinaryRota = 13,
        [DisplayText("Shift")]
        Shift = 14,
        [DisplayText("Staff")]
        Staff = 15,
        [DisplayText("Log")]
        Log = 16,
        [DisplayText("Error Log")]
        ErrorLog = 17,
        [DisplayText("Activity Log")]
        ActivityLog = 18,
        [DisplayText("Title")]
        Title = 19,
        [DisplayText("Color")]
        Color = 20,
        [DisplayText("Multidisciplinary Rota Template")]
        MultidisciplinaryRotaTemplate = 21,
        [DisplayText("Actual Rota")]
        ActualRota = 22,
        [DisplayText("Template Rota")]
        SingleTemplateRota = 23,
        [DisplayText("On Call Multidisciplinar")]
        OnCallMultidisciplinar = 24,
        [DisplayText("Multidisciplinary Dashboard")]
        MultidisciplinaryDashboard = 25
    }

    public enum MonthName
    {
        [DisplayText("Jan")]
        January = 1,
        [DisplayText("Feb")]
        February = 2,
        [DisplayText("Mar")]
        March = 3,
        [DisplayText("Apr")]
        April = 4,
        [DisplayText("May")]
        May = 5,
        [DisplayText("June")]
        June = 6,
        [DisplayText("July")]
        July = 7,
        [DisplayText("Aug")]
        August = 8,
        [DisplayText("Sep")]
        September = 9,
        [DisplayText("Oct")]
        October = 10,
        [DisplayText("Nov")]
        November = 11,
        [DisplayText("Dec")]
        December = 12
    }
    public enum WeekName
    {
        [DisplayText("Mon")]
        Monday = 1,
        [DisplayText("Tue")]
        Thuesday = 2,
        [DisplayText("Wed")]
        Wednesday = 3,
        [DisplayText("Thu")]
        Thursday = 4,
        [DisplayText("Fri")]
        Friday = 5,
        [DisplayText("Sat")]
        Saturday = 6,
        [DisplayText("Sun")]
        Sunday = 7,
    }

    public enum StaffColumnName
    {
        [DisplayText("Surname")]
        Surname = 1,
        [DisplayText("Forename")]
        Forename = 2,
        [DisplayText("Title Name")]
        TitleName = 3,
        [DisplayText("SkillSet Name")]
        SkillSetName = 4,
        [DisplayText("Department Name")]
        DepartmentName = 5,
        [DisplayText("Extension")]
        Extension = 6,
        [DisplayText("Bleep")]
        Bleep = 7,
        [DisplayText("Pager")]
        Pager = 8,
        [DisplayText("Home")]
        Home = 9,
        [DisplayText("Mobile")]
        Mobile = 10,
        [DisplayText("Email")]
        Email = 11,
        [DisplayText("Alternet Extension")]
        AltExtn = 12,
        [DisplayText("Alternet Bleep")]
        AltBleep = 13,
        [DisplayText("Alternet Pager")]
        AltPager = 14,
        [DisplayText("Alternet Mobile")]
        AltMobile = 15,
        [DisplayText("Other Private")]
        OtherPrivate = 16,
        [DisplayText("Alternet Email")]
        AltEmail = 17,
        [DisplayText("Contact Order")]
        ContactOrder = 18
    }
}


