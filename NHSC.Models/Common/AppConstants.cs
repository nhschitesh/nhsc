﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHSC.Resources;
using System.Web;
using System.IO;

namespace NHSC.Models.Common
{
    public static class AppConstants
    {

        #region Images Path 
        public const string ProductLogo = @"/assets/media/img/logo.jpg";
        public const string ProductLogoEmail = @"/assets/media/img/logo.jpg";
        #endregion Images Path 

        #region Other 
        public const string DateFormate = "ddmmyyyyhhmmssttt";
        public const string SaveFileFormate = "ddmmyyyyhhmmssttt";
        public const string TimeFormat = "hh:mm tt";
        public const string DateShowFormate = "dd-MM-yy";
        public const string DateTimeShowFormate = "dd-MM-yy HH:mm:ss";

        
        public const string EmailProductDisplayName = "NHSC";
        public const int Passwordlength = 8;
        public const int MinutedifferenceTime = 30;
        public const string SearchSpliter = "';'";

        public const string XLS = "xls";
        public const string xlsx = "xlsx";

        public const string csv = "csv";

        #endregion Other

        #region Session Variable
        public const string UserSession = "_Session_Users";
        public const string KeepMeLogIn = "_KeepMeLogin";
        public const string EditRecord = "EditRecord";
        public const string DeleteRecord = "DeleteRecord";
        public const string AddRecord = "AddRecord";
        public const string IsShowAll = "IsShowAll";
        public const int KeepMeSignInExpireHour = 48;
        #endregion

        #region Acivity Log
        public const string LogIn = "{0} logIn";
        public const string LogOut = "{0} Log Out";
        #endregion

        #region Assgin  Delete  Flag
        public const string Color = "Color";
        public const string Department = "Department";
        public const string Site = "Site";
        public const string SkillSet = "SkillSet";
        public const string Title = "Title";
        public const string UserGroup = "UserGroup";
        public const string User = "User";
        public const string Staff = "Staff";
        public const string Shift = "Shift";
        public const string RotaTemplate = "RotaTemplate";
        public const string MultidisciplinaryRota = "MultidisciplinaryRota";
        
        #endregion

    }
}
