﻿/*!
*  Created by Hitesh Vijayvergiya *

Decription For Custom Validatio Class Name


$.bindspclvalidationevent('tagname','controlname');

 ----------- Attribute Section--------------
data_valmsg      -------  text Box property name for show msg 
data_length      -------  text box max length 
data-ImgPath     -------  image path select
addition_field   -------  fro Check Data Exist 
data_controller  -------  get controller name
data_action      -------  get action name
data-from        -------   range from  
data-to          -------   range to
date-number      ------   to minum value required
//password condition
//Must be at least 8 characters
//At least 1 number, 1 lowercase, 1 uppercase letter
//At least 1 special character from @#$%&

----------- Attribute Section End--------------



-----------------  Validation Class ------------------------

*     Validation For                            Class Name                 
                                                      
*      Require Field                   -         reqfield    

*      String Charecter length         -         regchar

*      NumberOnly                      -         reqnumonly        onnull   onnumeric

*      OnlyDecimalNum                  -         reqdecimalnum     onnull  onfloat

*      NumOrDecimal                    -         reqnumordec       onnull   onfloat

*      Check Box                       -         reqCheck

*      Image                           -         reqimage

*      Exist                           -         reqexist

*      Email                           -         regemail

*      Range                           -         reqrange          data-from   data-to

*      Valid Mobile number 10 digit    -        validmobilenumber

*      Valid Mobile Number             -       validpassword

*      Valid Minumum Value             -       regminnumber    date-number





-----------------  Validation Class End ------------------------

/*******************************************Global Properties*************************************************/
var wrngClass = "validationerorr";
var regexOnlyNum = /^[0-9]+$/;
var regexnumdecimal = /^[-+]?[0-9]+\.[0-9]+$/;
var onlynumdecimal = /^.[0-9]+$/;
var _valMsgBoxShow = true;
var tagevent = "input[type='text'],input[type='hidden'],input[type='email'],input[type='password'],input[type='checkbox'],input[type='radio'],input[type='file'],textarea,select";
var tagclass = ".reqfield,.reqnumonly,.reqdecimalnum,.reqnumordec,.regchar,.reqCheck,.reqimage,.reqexist";
//var regemail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
var regemail = /^(([^<>()\[\]\\.,;:\s@""]+(\.[^<>()\[\]\\.,;:\s@""]+)*)|("".+""))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
var pattern = /^.*(?=.{8,})(?=.*[A-Z])(?=.*[@#$%&]).*$/;




/*************************************************End**********************************************************/


//Know for Array Max or Min Value
Array.max = function (array) {
    return Math.max.apply(Math, array);
};

// Function to get the Min value in Array
Array.min = function (array) {
    return Math.min.apply(Math, array);
};
//end

function Valcheck(e, regexvalue) {
    return regexvalue.test($(e).val());
}

function valuecheck(e, regexvalue) {
    return regexvalue.test($(e).val());
}

//for type only number in text box
function IsNumOnlytxt(num) {
    //debugger
    $(num).keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57))
            return false;
        else
            return true;

    });

}

//for type only Decimal number in text box
function IsDecNumOnlytxt(dec) {
    //debugger
    $(dec).keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 46)
            return false;
        else
            return true;

    });

}

function Requiredcheck(e) {
    var propName = $(e).prop('tagName').toLowerCase();
    var id = $(e).prop('id').toLowerCase();
    $('.' + id).html("");
    if (propName == "select") {
        if ($(e)[0].selectedIndex == 0 || $(e)[0].selectedIndex == "" || $(e)[0].selectedIndex == null) {
            return false;
        }
        else {
            return true;
        }
    }
    else {
        if ($(e).hasClass('reqnumonly')) {
            if (parseInt($(e).val()) > 0) {
                return true;
            }
            else {
                return false;
            }
        } else if ($(e).hasClass("reqdecimalnum") || $(e).hasClass("reqnumordec")) {
            if (parseFloat($(e).val()) > 0) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            if ($(e).val() == "") {
                return false;
            }
            else {
                return true;
            }
        }
    }
}

/*******************************************Validation Functions*************************************************/

function BindSpclValidationEventClick(tag) {
    //debugger
    var log = true;
    var $errortoparray = [];


    $(tag).find(tagevent).each(function () {
        if (!customvalidationcheck(this)) {
            log = false
            $errortoparray.push($(this).position().top);
        }

    });

    if (log == false) {
        $("html, body").animate({ scrollTop: Array.min($errortoparray) - 20, scrollLeft: -20 }, 1000);
    }

    return log;
}


$('body').on('change', tagevent, function () {
    //debugger
    var log = true;
    if (!customvalidationcheck(this)) {
        log = false
    }

    return log;
});
$('body').on('blur', tagevent, function () {
    //debugger
    var log = true;
    if (!customvalidationcheck(this)) {
        log = false
    }

    return log;
});



$('body').on('focus', function () {
    if ($(this).hasClass('reqnumonly') || $(this).hasClass('reqdecimalnum')) {
        return IsNumOnlytxt(this);
    }
    if ($(this).hasClass('reqdecimalnum')) {
        return IsDecNumOnlytxt(this);
    }
});

function customvalidationcheck(e) {
    var log = true;
    var validerrormsg = "";
    var validboxid = '.' + $(e).attr('id');
    var ValidBoxName = $(e).attr('data-valmsg');
    //debugger
    if ($(e).hasClass('reqfield')) {
        // debugger
        var propName = $(e).prop('tagName').toLowerCase();
        if (!Requiredcheck(e)) {
            if (propName == "select") {
                validerrormsg = "Please Select";
            } else {
                //validerrormsg = ValidBoxName + " Field is required";
                validerrormsg = "Required";
            }
            log = false;
        }
    }
    if ($(e).hasClass('reqnumonly')) {
        if ($(e).val() != "") {
            if (!Valcheck(e, regexOnlyNum)) {
                validerrormsg = "Invalid Number";
                log = false;
            }
        }
    }
    if ($(e).hasClass('validmobilenumber')) {
        if ($(e).val() != "") {
            if ($(e).val().length != 10) {
                validerrormsg = "Please put 10  digit mobile number";
                log = false;
            }
        }
    }
    if ($(e).hasClass('validpassword')) {
        if ($(e).val() != "") {
            if (!Valcheck(e, pattern)) {
                //validerrormsg = "Password required one capital word one special characters and min length 8.";
                ShowMessageBox(0, "Password validation failed: <br /> Criteria:<br /> - Minimum length 8 characters <br /> - Minimum 1 capital letter <br />   - Minimum 1 special character");
                log = false;
            }
        }
    }
    if ($(e).hasClass('reqdecimalnum')) {
        if ($(e).val() != "") {
            if (!Valcheck(e, regexnumdecimal)) {
                validerrormsg = "Invalid";
                log = false;
            }
        }
    }
    if ($(e).hasClass('reqnumordec')) {
        if ($(e).val() != "") {
            if (!Valcheck(e, regexnumdecimal)) {
                if (!Valcheck(e, regexOnlyNum)) {
                    log = false;
                    validerrormsg = "Invalid";
                }
            }
        }
    }

    if ($(e).hasClass('regchar')) {
        if ($(e).val() != "") {
            var ValidLength = $(e).attr('data-length');
            if ($(e).val().length > ValidLength) {
                validerrormsg = ValidLength + "Character Only";
                log = false;
            }
        }
    }

    if ($(e).hasClass('reqrange')) {
        if ($(e).val() != "") {
            var from_value = parseInt($(e).attr('data-from'));
            var to_value = parseInt($(e).attr('data-to'));
            var actualvalue = parseInt($(e).val());
            actualvalue = actualvalue = '' ? 0 : actualvalue
            if (actualvalue >= from_value && actualvalue <= to_value) { }
            else {
                console.log("to_value=" + to_value);
                console.log("actual value=" + actualvalue);
                console.log("from value=" + from_value);

                //validerrormsg = from_value + ' to ' + to_value + ' value allowed';
                if (to_value == 0) {
                    validerrormsg = "There is no question available in question library";
                }
                else if (to_value < actualvalue) {
                  
                    validerrormsg = "Maximum allowed question is " + to_value +"";
                }
                

                log = false;
            }

        }
    }

    if ($(e).hasClass('regperdiscount')) {
        if ($(e).val() != "") {
            if ($(e).val() > 100) {
                $(e).val("0.00");
                //validerrormsg = "invalid(<=100)";
                //log = false;
            }
        }
    }
    if ($(e).hasClass('regfixdiscount')) {
        var FixDis = $(e).attr('data-FixDis');
        if ($(e).val() != "") {
            if ($(e).val() > parseFloat($("#" + FixDis).val())) {
                $(e).val("0.00");
                //validerrormsg = "invalid(<=Price)";
                //log = false;
            }
        }
    }

    if ($(e).hasClass('reqCheck')) {
        if (!$(e).is(':checked')) {
            validerrormsg = "Must Be Check";
            log = false;
        }
    }
    if ($(e).hasClass('reqimage')) {

        validboxid = '.' + $(e).attr('id') + '_Img';
        var ImgPath = '#' + $(e).attr('data-ImgPath');
        //if ($(ImgPath).attr('src') == _rootURL + "/images/noimage.png") {
        //    log = false;
        //    validerrormsg = "File required";

        //}
        //if ($(ImgPath).attr('src') == "") {
        //    log = false;
        //    validerrormsg = "File required";

        //}
    }

    if ($(e).hasClass('regemail')) {
        if ($(e).val() != "") {
            if (!Valcheck(e, regemail)) {
                log = false;
                validerrormsg = "Invalid Email";
            }
        }
    }
    if ($(e).hasClass('regminnumber')) {
        var number = $(e).attr('data-number');
        if ($(e).val() != "") {
            if ($(e).val() < parseFloat(number)) {
                validerrormsg = "minimum " +number + " value required";
                log = false;
            }
        }
    }


    if (_valMsgBoxShow) {
        if (!log) {
            if ($(validboxid).length < 1) {
                showValidErrormsg(e, validerrormsg);
            } else {
                var Content = validboxid;
                var msg = validerrormsg;
                $(Content).html(msg);
            }
            $('#' + $(e).attr('id')).addClass("is-border-red");
            $('#' + $(e).attr('id')).parent().find(".select2-selection").addClass("Red_border");
        } else {
            if ($(validboxid).length > 0) {
                $(validboxid).html("");
                $('#' + $(e).attr('id')).removeClass("is-border-red");
            }
            if ($("#" + $(e).attr('id')).val() != "") {
                $("." + $(e).attr('id')).html(" ");
                $('#' + $(e).attr('id')).removeClass("is-border-red");
                $('#' + $(e).attr('id')).parent().find(".select2-selection").removeClass("Red_border");
            }
        }
    }
    return log;

};

var showValidErrormsg = function (e, msg) {
    $('.' + $(e).attr('id')).html(msg);
};




$('body').on('keypress', '.onfloat', function (evt) {
    var backend_charCode = (evt.which) ? evt.which : evt.keyCode
    if (backend_charCode > 31 && (backend_charCode < 48 || backend_charCode > 57) && backend_charCode != 46)
        return false;
    else
        return true;
});

$('body').on('blur', '.onnull', function (evt) {
    if ($(evt.target).val() == "") {
        $(evt.target).val(0);
    }
});

$('body').on('keypress', '.onnumeric', function (evt) {
    var backend_charCode = (evt.which) ? evt.which : evt.keyCode
    if (backend_charCode > 31 && (backend_charCode < 48 || backend_charCode > 57))
        return false;
    else
        return true;
});


/************************************************End*******************************************************/

//valid string value
function ValidString(value) {
    if (value == "" || value.trim() == "" || value == null || value == undefined) {
        return false;
    }
    else {
        return true;
    }
}