function ShowLoader() {
    //show page loader
    $("#AjaxLoader").css("display", "block");
}
function HideLoader() {
    //hide page loader
    $("#AjaxLoader").css("display", "none");
}
//[Start]Aalert message by javascript
function ShowMessageBox(plngMessageCode, pstrMessageText, PageKey) {
    try {

        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            //"progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "3",
            "hideDuration": "1000",
            "timeOut": "10000",
            "extendedTimeOut": "1000",
            //"showEasing": "swing",
            //            "hideEasing": "linear",
            //            "showMethod": "fadeIn",
            //            "hideMethod": "fadeOut"
        };
        if (plngMessageCode === 1) {
            toastr.success(pstrMessageText, "Success");
        } else if (plngMessageCode === 2) {
            toastr.warning(pstrMessageText, "Warning");
        } else if (plngMessageCode === 3) {
            toastr.info(pstrMessageText, "Info");
        } else {
            toastr.error(pstrMessageText, "Error");
        }
    }
    catch (ex) {
        console.log(ex.message);
        return false;
    }
}
//[End]Aalert message by javascript


//----------------end Validate Image---------------------//
var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1200 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#663259", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#8950FC", "warning": "#FFA800", "danger": "#F64E60", "light": "#F3F6F9", "dark": "#212121" }, "light": { "white": "#ffffff", "primary": "#F4E1F0", "secondary": "#ECF0F3", "success": "#C9F7F5", "info": "#EEE5FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#212121", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#ECF0F3", "gray-300": "#E5EAEE", "gray-400": "#D6D6E0", "gray-500": "#B5B5C3", "gray-600": "#80808F", "gray-700": "#464E5F", "gray-800": "#1B283F", "gray-900": "#212121" } }, "font-family": "Poppins" };


//[Start]Confirmation message by javascript
function ShowConfirmYesNo(pstrMessage, pstrReturnVal, pstrTitle, pstrFrom) {
    try {

        AsyncConfirmYesNoSweetAlert(
            pstrTitle,
            pstrMessage,
            pstrReturnVal,
            pstrFrom,
            fnCallBackConfirmReturnYes,
            fnCallBackConfirmReturnNo
        );
    }
    catch (ex) {
        AsyncConfirmYesNo(
            pstrTitle,
            pstrMessage,
            pstrReturnVal,
            pstrFrom,
            fnCallBackConfirmReturnYes,
            fnCallBackConfirmReturnNo
        );
        return false;
    }
}
function AsyncConfirmYesNoSweetAlert(title, msg, returnval, from, fnCallBackConfirmReturnYes, fnCallBackConfirmReturnNo) {
    try {
        Swal.fire({
            title: title,
            text: msg,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            cancelButtonText: "Cancel"
        }).then(function (result) {
            if (result.value) {
                fnCallBackConfirmReturnYes(from, returnval);
            } else {
                fnCallBackConfirmReturnNo(from, returnval);
            }
        });

        //    then((result) => {
        //    if (result.value) {
        //        fnCallBackConfirmReturnYes(from, returnval);
        //    } else {
        //        fnCallBackConfirmReturnNo(from, returnval);
        //    }
        //});
    }
    catch (ex) {
        alert(ex.message);
        return false;
    }
}
function ShowSwalPopup(pstrMessage, pstrReturnVal, pstrTitle, pstrFrom, pstrYesButton, pstrNoButton) {
    try {
        AsyncSwalYesNoSweetAlert(
            pstrTitle,
            pstrMessage,
            pstrReturnVal,
            pstrFrom,
            pstrYesButton,
            pstrNoButton,
            fnCallBackConfirmReturnYes,
            fnCallBackConfirmReturnNo
        );
    }
    catch (ex) {
        return false;
    }
}
function AsyncSwalYesNoSweetAlert(title, msg, returnval, from, YesButtonTitle, NoButtonTitle, fnCallBackConfirmReturnYes, fnCallBackConfirmReturnNo) {
    try {
        Swal.fire({
            title: title,
            text: msg,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#ff0000',
            cancelButtonColor: '#ffffff',
            confirmButtonText: YesButtonTitle,
            cancelButtonText: NoButtonTitle
        }).then(function (result) {
            if (result.value) {
                fnCallBackConfirmReturnYes(from, returnval);
            } else {
                fnCallBackConfirmReturnNo(from, returnval);
            }
        });
        //.then((result) => {
        //if (result.value) {
        //    fnCallBackConfirmReturnYes(from, returnval);
        //} else {
        //    fnCallBackConfirmReturnNo(from, returnval);
        //}
        //});
    }
    catch (ex) {
        alert(ex.message);
        return false;
    }
}
//[End]Confirmation message by javascript




function SelectValueChosen(parm, hfdvalue, isRequire) {
    //debugger
    var nameList = "";
    var a = 0;
    //hfdvalue = document.getElementById(hfdvalue.id);
    var sel1 = document.getElementById(parm.id);
    for (var i = 0; i < sel1.options.length; i++) {

        if (sel1.options[i].selected) {
            if (a == 0) {
                nameList += sel1.options[i].value;
            } else {
                nameList += "," + sel1.options[i].value;
            }
            a++;

        }
    }
    if (isRequire) {
        if (nameList != "") {
            $('#' + hfdvalue).parent().find(".select2-selection").removeClass("error_Validation");
            $('.' + hfdvalue).html("");
        }
        else {
            $('#' + hfdvalue).parent().find(".select2-selection").addClass("error_Validation");
            $('.' + hfdvalue).html("Required");
        }
    }
    document.getElementById(hfdvalue).value = nameList;
}
//////////////////////////////
function SetActiveMenu(id) {
    $(".clsRemoveActive").removeClass("menu-active");
    $("#Menu_" + id).addClass("menu-active");
    var MenuID = $("#Menu_" + id).attr("data-id");
    if (MenuID > 0) {
        $("#Menu_" + MenuID).addClass("menu-active");
    }
}
function SetSubActiveMenu(id) {
    $(".clsRemoveSubActive").removeClass("menu-subitem-active");
    $("#SubMenu_" + id).addClass("menu-subitem-active");
    var MenuID = $("#SubMenu_" + id).attr("data-id");
    if (MenuID > 0) {
        $("#SubMenu_" + MenuID).addClass("menu-subitem-active");
    }
}


function AddPaginLength(id) {
    $('select[name="' + id + '_length"]').find('option[value="5"]').text("5 per page");
    $('select[name="' + id + '_length"]').find('option[value="10"]').text("10 per page");
    $('select[name="' + id + '_length"]').find('option[value="25"]').text("25 per page");
    $('select[name="' + id + '_length"]').find('option[value="50"]').text("50 per page");
    $('select[name="' + id + '_length"]').find('option[value="100"]').text("100 per page");
    $('select[name="' + id + '_length"]').find('option[value="500"]').text("500 per page");
}

// on tab click select first value in drop down
$(document).on('keydown', '.clsddl', function (e) {
    if (e.originalEvent && e.which == 40) {
        e.preventDefault();
        $(this).siblings('select').select2('open');
    }
});
var isSearch = false;
$("body").on("keypress keyup change click", "#tblList_filter input", function (e) {
    isSearch = true;
});
$(document).on('keydown', '#tblList_filter input', function (e) {
    isSearch = true;
});
$(document).on('keypress', '#tblList_filter input', function (e) {
    isSearch = true;
});
$(document).on('blur', '#tblList_filter input', function (e) {
    isSearch = true;
});
$(document).on('change', '#tblList_filter input', function (e) {
    isSearch = true;
});
//Append ToolTip icon After Search
function AppendToolTipAfterSearch(id) {
    $("#" + id).append('<i class="fa flaticon-info clsInfoToolTip red-tooltip" data-toggle="tooltip" data-html="true" data-placement="left" title="" data-original-title="To search multiple columns please use ; as a separator"  ></i>');

}