﻿using System.Collections.Generic;
using System.Web.Optimization;

namespace NHSC
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region Java Script
            var js_bundle_main = new ScriptBundle("~/script/jquerymain")
                  .Include("~/Scripts/jquery-3.4.1.min.js")
                   .Include("~/Scripts/jquery.unobtrusive*");
            js_bundle_main.Orderer = new AsIsBundleOrderer();
            bundles.Add(js_bundle_main);

            var js_bundle_loginjs = new ScriptBundle("~/script/loginjs")
                .Include("~/assets/plugins/global/plugins.bundle.js")
                 .Include("~/assets/plugins/custom/prismjs/prismjs.bundle.js")
                .Include("~/assets/js/scripts.bundle.js")
                //.Include("~/assets/js/pages/custom/login/login-general.js")
             .Include("~/Scripts/Validation.js")
            .Include("~/Scripts/CustomCommon.js");
            js_bundle_loginjs.Orderer = new AsIsBundleOrderer();
            bundles.Add(js_bundle_loginjs);


            var js_bundle_validataion = new ScriptBundle("~/script/Validation")
                .Include("~/Scripts/Validation.js")
                .Include("~/Scripts/CustomCommon.js");
            js_bundle_validataion.Orderer = new AsIsBundleOrderer();
            bundles.Add(js_bundle_validataion);


            var js_bundle_jquerybundle = new ScriptBundle("~/script/jquerybundle")
                .Include("~/assets/plugins/global/plugins.bundle.js")
                .Include("~/assets/plugins/custom/prismjs/prismjs.bundle.js")
                .Include("~/assets/js/scripts.bundle.js")
                .Include("~/assets/plugins/custom/datatables/datatables.bundle.js");
            js_bundle_jquerybundle.Orderer = new AsIsBundleOrderer();
            bundles.Add(js_bundle_jquerybundle);


            var js_bundle_dashboard = new ScriptBundle("~/script/dashboard")
                  .Include("~/assets/js/scripts.bundle.js")
               .Include("~/assets/js/pages/widgets.js");
            js_bundle_dashboard.Orderer = new AsIsBundleOrderer();
            bundles.Add(js_bundle_dashboard);

            var js_bundle_wizard3 = new ScriptBundle("~/script/wizard3")
             .Include("~/assets/js/pages/custom/wizard/wizard-3.js");
            js_bundle_wizard3.Orderer = new AsIsBundleOrderer();
            bundles.Add(js_bundle_wizard3);


            var js_bundle_datetimepicker = new ScriptBundle("~/script/datepicker")
       .Include("~/assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js");
            js_bundle_datetimepicker.Orderer = new AsIsBundleOrderer();

            #endregion

            #region CSS


            var css_bundle_Login = new StyleBundle("~/Content/logincss")
                     .Include("~/assets/css/pages/login/classic/login-4.css")
                      .Include("~/assets/plugins/global/plugins.bundle.css")
                         .Include("~/assets/plugins/custom/prismjs/prismjs.bundle.css")
                     .Include("~/assets/css/style.bundle.css")
                     .Include("~/Content/CustomCommon.css");
            css_bundle_Login.Orderer = new AsIsBundleOrderer();
            bundles.Add(css_bundle_Login);

            var css_bundle_maincss = new StyleBundle("~/Content/maincss")
                 .Include("~/assets/plugins/custom/datatables/datatables.bundle.css")
                     .Include("~/assets/plugins/custom/prismjs/prismjs.bundle.css")
                          .Include("~/assets/plugins/global/plugins.bundle.css")
                     .Include("~/assets/css/style.bundle.css")
                     .Include("~/Content/CustomCommon.css");
            css_bundle_maincss.Orderer = new AsIsBundleOrderer();
            bundles.Add(css_bundle_maincss);

            var css_bundle_wizard3 = new StyleBundle("~/Content/wizard3")
                  .Include("~/assets/css/pages/wizard/wizard-3.css");
            css_bundle_wizard3.Orderer = new AsIsBundleOrderer();
            bundles.Add(css_bundle_wizard3);

            #endregion
        }
    }
    class AsIsBundleOrderer : IBundleOrderer
    {
        public IEnumerable<BundleFile> OrderFiles(BundleContext context, IEnumerable<BundleFile> files)
        {
            return files;
        }
    }
}
