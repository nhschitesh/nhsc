﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Threading;
using System.Globalization;

namespace NHSC
{
    public class CultureHelper
    {
        protected HttpSessionState session;

        public CultureHelper(HttpSessionState httpSessionState)
        {
            session = httpSessionState;
        }

        public static string CurrentCulture
        {
            get
            {
                return Thread.CurrentThread.CurrentUICulture.Name;
            }

            set
            {
                if (value != null && value.ToString() != "")
                {
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(value.ToString());
                }
                else
                {
                    Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture;
                }
            }
        }
    }
}