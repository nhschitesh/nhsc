﻿using System.Net;
using System.Web.Mvc;
using NHSC.Repository;
using NHSC.Models.Entities;
using NHSC.Models.Common;
namespace NHSC
{
    public class CustomErrorHandling : HandleErrorAttribute
    {
        public override void OnException(ExceptionContext exceptionContext)
        {
            if (!exceptionContext.ExceptionHandled)
            {
                string strControllerName = (string)exceptionContext.RouteData.Values["controller"];
                string strActionName = (string)exceptionContext.RouteData.Values["action"];
                var model = new HandleErrorInfo(exceptionContext.Exception, strControllerName, strActionName);
                bool showCustomMessage = false;
                exceptionContext.ExceptionHandled = true;

                CommonFunctions.WriteLog("Application_Error=> (Controller:" + strControllerName + " Action:" + strActionName + ")  Error Custom : " + exceptionContext.Exception.Message);
                ErrorLogModel objError = new ErrorLogModel();
                CommonFunctions objCommon = new CommonFunctions();
                if (objCommon.LoginUserDetails != null)
                {
                    objError.UserId = objCommon.LoginUserDetails.UserId;
                }
                objError.ModifyDate = CommonFunctions.Datetimesetting();
                objError.ErrorMsg = exceptionContext.Exception.Message;
                if (exceptionContext.Exception.InnerException != null)
                {
                    objError.InnerException = exceptionContext.Exception.InnerException.Message;
                }
                //objError.chrLineNumber = ((System.Web.HttpParseException)model.Exception).Line.ToString();
                objError.Controller = strControllerName;
                objError.Action = strActionName;
                objError.IpAddress = CommonFunctions.GetUserAddress();
                System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(exceptionContext.Exception, true);

                IUnitOfWork db = new UnitOfWork();
                db.Common.InsertErrorData(objError);

                //ErrorDAL.Insert_Error(objError);
                //[Start]For Ajax Request
                if (exceptionContext != null)
                {
                    if (exceptionContext.HttpContext.Request.IsAjaxRequest())
                    {
                        exceptionContext.HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        exceptionContext.Result = new JsonResult
                        {
                            Data = new
                            {
                                StatusCode = HttpStatusCode.InternalServerError,
                                Error = model.Exception.Message,
                                msg = model.Exception.Message,
                                message = model.Exception.Message,
                                ShowCustomMessage = showCustomMessage
                            },
                            JsonRequestBehavior = JsonRequestBehavior.AllowGet
                        };
                        return;
                    }
                }
                //[End]For Ajax Request

                exceptionContext.Result = new ViewResult
                {
                    ViewName = "Error"
                };
            }
        }

    }
}