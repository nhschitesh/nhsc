﻿using NHSC.Models.Entities;
using NHSC.Models.Common;
using NHSC.Repository;
using NHSC.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NHSC.Security;
using NHSC.Common;
using System.Data;
using System.IO;
using System.Configuration;
using System.Data.OleDb;
using ClosedXML.Excel;

namespace NHSC.Controllers
{
    [CheckSessionOut(Values = (int)Menu.MultidisciplinaryRotaTemplate)]
    [CustomErrorHandling]
    public class MultidisciplinaryRotaTemplateController : BaseController
    {

        #region Private Members
        private IUnitOfWork db;
        CommonFunctions objCommon;
        #endregion

        public MultidisciplinaryRotaTemplateController()
        {
            db = new UnitOfWork();
            objCommon = new CommonFunctions();
        }

        #region Crud Opration
        public ActionResult Index()
        {
            AssignRotaModel objAssignRotaModel = new AssignRotaModel();
            try
            {
                ViewBag.MultiList = new SelectList(db.MultidisciplinaryRota.GetMultidisciplinaryRota(), "Value", "Text");
                SetPermision();
            }
            catch (Exception)
            {
                throw;
            }
            return View(objAssignRotaModel);
        }

        #endregion

        #region Get Method


        public ActionResult GetForm(long Id = 0)
        {
            AssignRotaModel objAssignRotaModel = new AssignRotaModel();
            try
            {
                if (Id > 0)
                {
                    objAssignRotaModel = db.RotaTemplate.GetAssignRotaList(Id,User.UserId);
                    Response.Cookies["Id"].Value = Convert.ToString(Id);
                }

            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_Form", objAssignRotaModel);
        }
        public ActionResult GetView(long Id = 0)
        {
            AssignRotaModel objAssignRotaModel = new AssignRotaModel();
            try
            {
                objAssignRotaModel = db.RotaTemplate.GetAssignRotaList(Id, User.UserId);
            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_View", objAssignRotaModel);
        }

        public ActionResult LoadStaffPopup(long tempid = 0, long assignid = 0, long staffid = 0)
        {
            StaffPopUp objStaffPopUp = new StaffPopUp();
            try
            {
                objStaffPopUp.RotaTemplateId = tempid;
                objStaffPopUp.RotaAssignId = assignid;
                objStaffPopUp.StaffId = staffid;
                ViewBag.StaffList = new SelectList(db.Staff.GetStaff(User.UserId), "Value", "Text");
            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_StaffPopUp", objStaffPopUp);
        }

        public ActionResult ChangeStaff(long assignid = 0, long staffid = 0)
        {
            StaffPopUp objStaffPopUp = new StaffPopUp();
            try
            {
                objStaffPopUp.RotaAssignId = assignid;
                objStaffPopUp.StaffId = staffid;
                db.RotaTemplate.AssignNewStaff(assignid, staffid, User.UserId, CommonFunctions.Datetimesetting());
            }
            catch (Exception)
            {
                throw;
            }
            return GetMessage(1, "Successfully updated staff");
        }


        //public ActionResult DownloadSampleAssignToOther()
        //{
        //    var wb = new XLWorkbook();
        //    // var wsPresentation = wb.Worksheets.Add("Presentation");
        //    var wsData = wb.Worksheets.Add("Data");

        //    // Fill up some data
        //    var assignbydepartmentcls = commonMethods.GetDepartmentsByEmployeeID(userId);
        //    var Priorities = todoRepository.GetAllTodoPriority();
        //    var departments = commonMethods.GetDepartments();
        //    var employeecls = commonMethods.GetAllDeptwithEmployee();
        //    var employees = employeeRepository.GetAllEmployee();


        //    wsData.Cell(1, 1).Value = "My Department";
        //    int i = 1;
        //    foreach (var a in assignbydepartmentcls)
        //    {
        //        wsData.Cell(++i, 1).Value = a.name;
        //    }
        //    wsData.Range("A2:A" + i).AddToNamed("My Department");
        //    wsData.Cell(1, 2).Value = "Priority";
        //    i = 1;
        //    foreach (var a in Priorities)
        //    {
        //        wsData.Cell(++i, 2).Value = a.Name;
        //    }
        //    wsData.Range("B2:B" + i).AddToNamed("Priority");
        //    wsData.Cell(1, 3).Value = "Status";
        //    i = 1;
        //    //foreach (var a in departments)
        //    //{
        //    wsData.Cell(++i, 3).Value = "PENDING";
        //    wsData.Cell(++i, 3).Value = "ON-HOLD";
        //    wsData.Cell(++i, 3).Value = "IN-PROGRESS";
        //    wsData.Cell(++i, 3).Value = "COMPLETED";
        //    //}
        //    wsData.Range("C2:C" + i).AddToNamed("Status");


        //    wsData.Cell(1, 4).Value = "Assign Department";
        //    i = 1;
        //    foreach (var a in departments)
        //    {
        //        wsData.Cell(++i, 4).Value = a.Name;
        //    }
        //    wsData.Range("D2:D" + i).AddToNamed("Assign Department");
        //    int j = 4;
        //    foreach (var a in departments)
        //    {
        //        wsData.Cell(1, ++j).Value = a.Name;
        //        int k = 1;
        //        foreach (var b in employeecls.Where(m => m.DepartmentID == a.ID).ToList())
        //        {
        //            wsData.Cell(++k, j).Value = employees.Where(h => h.ID == b.EmployeeID).Select(m => (m.FirstName + " " + m.LastName + "(Emp. code-" + m.EmpCode + ")")).FirstOrDefault();
        //        }
        //        wsData.Range(wsData.Cell(2, j), wsData.Cell(k, j)).AddToNamed(a.Name);
        //    }


        //    DataTable validationTable = new DataTable();
        //    validationTable.Columns.Add("My Department");
        //    validationTable.Columns.Add("Name");
        //    validationTable.Columns.Add("Description");
        //    validationTable.Columns.Add("Due Date(yyyy-MM-dd)");
        //    validationTable.Columns.Add("Priority");
        //    validationTable.Columns.Add("Assign Department");
        //    validationTable.Columns.Add("Employee");
        //    validationTable.Columns.Add("Status");
        //    validationTable.TableName = "My_Task_Details";
        //    var worksheet = wb.AddWorksheet(validationTable);
        //    //worksheet.Column(3).Cell(1).SetDataType(XLDataType.Text);
        //    //worksheet.Range("C3:C2").SetDataType(XLDataType.DateTime);

        //    worksheet.Column(1).SetDataValidation().List(wsData.Range("My Department"), true);
        //    worksheet.Column(4).SetDataValidation().Date.EqualOrGreaterThan(DateTime.Now.Date);
        //    worksheet.Column(5).SetDataValidation().List(wsData.Range("Priority"), true);
        //    worksheet.Column(6).SetDataValidation().List(wsData.Range("Assign Department"), true);
        //    worksheet.Column(7).SetDataValidation().InCellDropdown = true;
        //    worksheet.Column(7).SetDataValidation().Operator = XLOperator.Between;
        //    worksheet.Column(7).SetDataValidation().AllowedValues = XLAllowedValues.List;
        //    worksheet.Column(7).SetDataValidation().List("=INDIRECT(SUBSTITUTE(F1," + '"' + " " + '"' + "," + '"' + "_" + '"' + "))");
        //    worksheet.Column(8).SetDataValidation().List(wsData.Range("Status"), true);
        //    //wsData.Hide();
        //    //worksheet2.Hide();



        //    //Byte[] workbookBytes;
        //    //MemoryStream ms = GetStream(wb);
        //    //workbookBytes = ms.ToArray();

        //    return File(workbookBytes, "application/ms-excel", $"Assign_Task_Details.xlsx");
        //}

        public void DownloadSampleAssignToOther()
        {
            using (XLWorkbook wb = new XLWorkbook())
            {
                // var wsPresentation = wb.Worksheets.Add("Presentation");
                var wsData = wb.Worksheets.Add("Data");

                // Fill up some data


                wsData.Cell(1, 1).Value = "My Department";
                int i = 1;
                wsData.Cell(++i, 1).Value = "a";
                wsData.Cell(++i, 1).Value = "b";


                wsData.Range("A2:A" + i).AddToNamed("My Department");
                wsData.Cell(1, 2).Value = "Priority";
                i = 1;
                wsData.Cell(++i, 2).Value = "1";
                wsData.Cell(++i, 2).Value = "2";
                wsData.Range("B2:B" + i).AddToNamed("Priority");
                wsData.Cell(1, 3).Value = "Status";
                i = 1;
                //foreach (var a in departments)
                //{
                wsData.Cell(++i, 3).Value = "PENDING";
                wsData.Cell(++i, 3).Value = "ON-HOLD";
                wsData.Cell(++i, 3).Value = "IN-PROGRESS";
                wsData.Cell(++i, 3).Value = "COMPLETED";
                //}
                wsData.Range("C2:C" + i).AddToNamed("Status");


                wsData.Cell(1, 4).Value = "Assign Department";
                i = 1;
                wsData.Cell(++i, 4).Value = "x";
                wsData.Cell(++i, 4).Value = "y";
                wsData.Range("D2:D" + i).AddToNamed("Assign Department");
                int j = 4;

                wsData.Cell(1, ++j).Value = "test";
                int k = 1;
                wsData.Cell(++k, j).Value = "test1";
                wsData.Range(wsData.Cell(2, j), wsData.Cell(k, j)).AddToNamed("test");

                wsData.Cell(1, ++j).Value = "test2";
                k = 1;
                wsData.Cell(++k, j).Value = "test3";
                wsData.Range(wsData.Cell(2, j), wsData.Cell(k, j)).AddToNamed("test2");



                DataTable validationTable = new DataTable();
                validationTable.Columns.Add("My Department");
                validationTable.Columns.Add("Name");
                validationTable.Columns.Add("Description");
                validationTable.Columns.Add("Due Date(yyyy-MM-dd)");
                validationTable.Columns.Add("Priority");
                validationTable.Columns.Add("Assign Department");
                validationTable.Columns.Add("Employee");
                validationTable.Columns.Add("Status");
                validationTable.TableName = "My_Task_Details";
                var worksheet = wb.AddWorksheet(validationTable);
                //worksheet.Column(3).Cell(1).SetDataType(XLDataType.Text);
                //worksheet.Range("C3:C2").SetDataType(XLDataType.DateTime);

                worksheet.Column(1).SetDataValidation().List(wsData.Range("My Department"), true);
                worksheet.Column(4).SetDataValidation().Date.EqualOrGreaterThan(DateTime.Now.Date);
                worksheet.Column(5).SetDataValidation().List(wsData.Range("Priority"), true);
                worksheet.Column(6).SetDataValidation().List(wsData.Range("Assign Department"), true);
                worksheet.Column(7).SetDataValidation().InCellDropdown = true;
                worksheet.Column(7).SetDataValidation().Operator = XLOperator.Between;
                worksheet.Column(7).SetDataValidation().AllowedValues = XLAllowedValues.List;
                worksheet.Column(7).SetDataValidation().List("=INDIRECT(SUBSTITUTE(F1," + '"' + " " + '"' + "," + '"' + "_" + '"' + "))");
                worksheet.Column(8).SetDataValidation().List(wsData.Range("Status"), true);
                //wsData.Hide();
                //worksheet2.Hide();
                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/ms-excel";
                Response.AddHeader("content-disposition", "attachment;filename=MultiRotaTemplate.xlsx");
                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }

            //Byte[] workbookBytes;
            //MemoryStream ms = GetStream(wb);
            //workbookBytes = ms.ToArray();

            //return File(workbookBytes, "application/ms-excel", $"Assign_Task_Details.xlsx");
        }

        public ActionResult ExportMultidisciplinaryRota()
        {
            DataTable dt = new DataTable();
            long Id = 0;
            try
            {

                Id = CommonFunctions.ValidlongValue(Request.Cookies["Id"].Value);

                dt = db.RotaTemplate.GetAssignRotaListExport(Id);


                //var grid = new System.Web.UI.WebControls.GridView();
                //grid.DataSource = dt;
                //grid.DataBind();
                //grid.HeaderRow.Style.Add("background-color", "#87ceeb");

                DataTable dt1 = db.Staff.GetStaffDDL();




                using (XLWorkbook wb = new XLWorkbook())
                {

                    var worksheet = wb.AddWorksheet(dt, "Rota Template");
                    var worksheet1 = wb.AddWorksheet(dt1, "Staff Name List");

                    //int i = 2;
                    //worksheet.Cell(++i, 1).Value = "MR Deepu1 Hitesh1";
                    //worksheet.Cell(++i, 1).Value = "A MS Brano P";
                    //worksheet.Range("A2:A" + i).AddToNamed("StaffName");
                    //worksheet.Column(4).SetDataValidation().List(worksheet.Range("StaffName"), true);

                    Response.Clear();
                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment;filename=MultiRotaTemplate.xlsx");
                    using (MemoryStream MyMemoryStream = new MemoryStream())
                    {
                        wb.SaveAs(MyMemoryStream);
                        MyMemoryStream.WriteTo(Response.OutputStream);
                        Response.Flush();
                        Response.End();
                    }
                }


                //DataTable dt1 = db.Staff.GetStaffDDL();
                //int i = 0;
                //foreach (System.Web.UI.WebControls.GridViewRow row in grid.Rows)
                //{
                //    System.Web.UI.WebControls.DropDownList ddl = new System.Web.UI.WebControls.DropDownList();
                //    ddl = BindStaff(dt1);
                //    ddl.SelectedValue = dt.Rows[i]["StaffName"].ToString();
                //    row.Cells[3].Controls.Add(ddl);
                //    row.Cells[3].Width = 300;
                //    row.Cells[6].Text = CommonFunctions.Encrypt(row.Cells[6].Text);
                //    i++;
                //}

                //System.IO.StringWriter sw = new System.IO.StringWriter();
                //System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(sw);
                //grid.RenderControl(htw);
                //byte[] binddata = System.Text.Encoding.ASCII.GetBytes(sw.ToString());
                //return File(binddata, "application/ms-excel", "MultidisciplinaryRotaTemplate" + "." + AppConstants.XLS);
                return null;
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost]
        public ActionResult ImportExcel(HttpPostedFileBase file)
        {
            string strErrorMsg = string.Empty;
            bool _status = false;
            string strExtionsion = String.Empty;
            string strFileName = String.Empty;
            string strFileFullPath = String.Empty;
            DataTable dt;
            long TotalCount = 0;
            long UserId = 0;
            //long AddedCount = 0;
            //long MisMacthCount = 0;
            //bool ISValid = true;
            DateTime Date;
            try
            {
                string filePath = string.Empty;
                if (file != null)
                {
                    strExtionsion = Path.GetExtension(file.FileName);
                    if (CommonFunctions.ValidImportFileExtionsion(strExtionsion))
                    {
                        //strFileName = CommonFunctions.Datetimesetting().ToString(AppConstants.SaveFileFormate) + strExtionsion;
                        //strFileFullPath = CommonFunctions.GetWebConfigValue("FileSavePath") + "Uploads\\" + strFileName;
                        //CommonFunctions.SaveFile(CommonFunctions.GetWebConfigValue("FileSavePath") + "Uploads", strFileFullPath, file);

                        strFileName = CommonFunctions.Datetimesetting().ToString(AppConstants.SaveFileFormate) + strExtionsion;
                        strFileFullPath = CommonFunctions.MapPathSetting(@"Uploads\" + strFileName);
                        CommonFunctions.SaveFile(@"Uploads\", strFileFullPath, file);

                        dt = GetDataFromExcel(strExtionsion, strFileFullPath);
                        if (CommonFunctions.IsValidDataTable(dt))
                        {
                            if (!IsContainColumn(dt, ref strErrorMsg))
                            {
                                return GetMessage(0, strErrorMsg);
                            }
                            //int Row = 1;

                            TotalCount = dt.Rows.Count;

                            UserId = User.UserId;
                            Date = CommonFunctions.Datetimesetting();

                            //Thread thread = new Thread(delegate ()
                            //{

                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                RotaDayWiseShiftList objRota = new RotaDayWiseShiftList();
                                //objRota.RotaAssignId = CommonFunctions.ValidlongValue(CommonFunctions.Decrypt(dt.Rows[i]["Unique Key"]?.ToString() ?? ""));
                                objRota.RotaAssignId = CommonFunctions.ValidlongValue(dt.Rows[i]["Unique Key"]?.ToString() ?? "");
                                objRota.StaffName = dt.Rows[i]["StaffName"]?.ToString() ?? "";
                                db.RotaTemplate.UpdateStaff(objRota, UserId, Date);
                            }

                            //});

                            //thread.IsBackground = true;
                            //thread.Start();

                            return GetMessage(1, "Successfully imported file.");
                            //_status = true;

                        }
                        else
                        {
                            return GetMessage(0, "No Data Found.");
                        }
                    }
                    else
                    {
                        return GetMessage(0, "Invalid File.");
                    }
                }
                else
                {
                    return GetMessage(0, "Invalid File.");
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return Json(new { msg = "", status = _status }, JsonRequestBehavior.AllowGet);
        }

        public System.Web.UI.WebControls.DropDownList BindStaff(DataTable dt)
        {

            System.Web.UI.WebControls.DropDownList ddl = new System.Web.UI.WebControls.DropDownList();
            try
            {
                if (CommonFunctions.IsValidDataTable(dt))
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        SelectListItem objValue = new SelectListItem();
                        objValue.Value = dt.Rows[i]["StaffId"]?.ToString() ?? "0";
                        objValue.Text = dt.Rows[i]["StaffName"]?.ToString() ?? "";
                        ddl.Items.Add(objValue.Text);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return ddl;
        }


        public bool IsContainColumn(DataTable table, ref string strErrorMsg)
        {
            bool IsValid = true;

            try
            {
                DataColumnCollection columns = table.Columns;
                if (!columns.Contains("StaffName"))
                {
                    strErrorMsg = "Staff Name,";
                }
                if (!columns.Contains("Unique Key"))
                {
                    strErrorMsg = strErrorMsg + "Unique Key,";
                }
                if (!string.IsNullOrEmpty(strErrorMsg))
                {
                    strErrorMsg = strErrorMsg.TrimEnd(',') + " " + "";
                    IsValid = false;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return IsValid;
        }
        public DataTable GetDataFromExcel(string strExtionsion, string strFileFullPath)
        {
            DataTable dt = new DataTable();
            try
            {

                //strFileFullPath = @"E:\InfoSoft\NHSC\Project\nhsc\NHSC\Uploads\1.xls";
                string conString = string.Empty;
                switch (strExtionsion)
                {
                    case ".xls": //Excel 97-03.
                        conString = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                        break;
                    case ".xlsx": //Excel 07 and above.
                        conString = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                        break;
                    case ".csv": //Excel 07 and above.
                        conString = ConfigurationManager.ConnectionStrings["CSVConString"].ConnectionString;
                        break;
                }
                conString = string.Format(conString, strFileFullPath);


                using (OleDbConnection connExcel = new OleDbConnection(conString))
                {
                    using (OleDbCommand cmdExcel = new OleDbCommand())
                    {
                        using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                        {
                            cmdExcel.Connection = connExcel;
                            //Get the name of First Sheet.
                            connExcel.Open();
                            DataTable dtExcelSchema;
                            dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                            string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"]?.ToString() ?? "";
                            connExcel.Close();
                            //Read Data from First Sheet.
                            connExcel.Open();
                            cmdExcel.CommandText = "SELECT * From [" + sheetName + "]";
                            odaExcel.SelectCommand = cmdExcel;
                            odaExcel.Fill(dt);
                            connExcel.Close();
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return dt;
        }
        #endregion

        #region Other Function

        public void SetPermision()
        {
            try
            {
                Session[AppConstants.EditRecord] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.MultidisciplinaryRotaTemplate).FirstOrDefault().Edit;
                Session[AppConstants.DeleteRecord] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.MultidisciplinaryRotaTemplate).FirstOrDefault().Delete;
                Session[AppConstants.AddRecord] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.MultidisciplinaryRotaTemplate).FirstOrDefault().Add;
                Session[AppConstants.IsShowAll] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.MultidisciplinaryRotaTemplate).FirstOrDefault().IsShowAll;
            }
            catch (Exception)
            {
                throw;
            }
        }
        private JsonResult GetMessage(int intStatuscode, string strMessage, string pstrID = "")
        {
            dynamic showMessageString = string.Empty;
            try
            {
                showMessageString = new
                {
                    statusCode = intStatuscode,
                    msg = strMessage,
                    id = pstrID,
                    Error = strMessage,
                    message = strMessage,
                };
                return Json(showMessageString, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
    }
}