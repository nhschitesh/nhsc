﻿using NHSC.Models.Entities;
using NHSC.Models.Common;
using NHSC.Repository;
using NHSC.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NHSC.Security;
using NHSC.Common;

namespace NHSC.Controllers
{
    [CheckSessionOut(Values = (int)Menu.BankHoliday)]
    [CustomErrorHandling]
    public class BankHolidayController : BaseController
    {

        #region Private Members
        private IUnitOfWork db;
        CommonFunctions objCommon;
        #endregion

        public BankHolidayController()
        {
            db = new UnitOfWork();
            objCommon = new CommonFunctions();
        }
        #region Crud Opration
        public ActionResult Index()
        {
            BankHolidayModel objBankHoliday = new BankHolidayModel();
            try
            {
                SetPermision();
            }
            catch (Exception)
            {
                throw;
            }
            return View(objBankHoliday);
        }

        [HttpPost]
        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public ActionResult Index(BankHolidayModel objBankHoliday)
        {
            string strErrorMsg = string.Empty;
            int intStatuscode = 1;
            bool _status = false;
            try
            {
                objBankHoliday.ModifyBy = User.UserId;
                objBankHoliday.ModifyDate = CommonFunctions.Datetimesetting();
                if (!IsValidData(objBankHoliday, ref strErrorMsg))
                {
                    return GetMessage(0, strErrorMsg, objBankHoliday.BankHolidaysId.ToString());
                }
                _status = db.BankHoliday.InsertData(objBankHoliday, ref strErrorMsg);
                if (objBankHoliday.BankHolidaysId == 0)
                {
                    strErrorMsg = Resources.Resource.RecordSaveSuccessfully;
                }
                else
                {
                    strErrorMsg = Resources.Resource.RecordUpdateSuccessfully;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return GetMessage(intStatuscode, strErrorMsg, objBankHoliday.BankHolidaysId.ToString());
        }

        [HttpPost]
        public ActionResult DeleteRecord(long Id)
        {
            string strErrorMsg = string.Empty;
            int intStatuscode = 1;
            try
            {
                bool Status = db.BankHoliday.DeleteData(Id, User.UserId, CommonFunctions.Datetimesetting());
                strErrorMsg = Resources.Resource.RecordDeletedSuccessfully;
            }
            catch (Exception)
            {
                throw;
            }
            return GetMessage(intStatuscode, strErrorMsg, Id.ToString());
        }
        #endregion

        #region Get Method

        public ActionResult GetListJson()
        {
            List<BankHolidayModel> objBankHolidayList = new List<BankHolidayModel>();
            CommonModel objPaging = new CommonModel();
            try
            {
                BindPagingData(objPaging);

                objBankHolidayList = db.BankHoliday.GetList(objPaging);

                return Json(new { draw = objPaging.Page, recordsFiltered = objPaging.TotalCount, recordsTotal = objPaging.TotalCount, data = objBankHolidayList });
            }
            catch (Exception)
            {
                throw;
            }
        }
        public ActionResult GetForm(long Id = 0)
        {
            BankHolidayModel objBankHoliday = new BankHolidayModel();
            try
            {
                objBankHoliday = db.BankHoliday.GetDetails(Id);

            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_Form", objBankHoliday);
        }
        public ActionResult GetView(long Id = 0)
        {
            BankHolidayModel objBankHoliday = new BankHolidayModel();
            try
            {
                objBankHoliday = db.BankHoliday.GetDetails(Id);
            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_View", objBankHoliday);
        }
        public ActionResult GetList()
        {
            BankHolidayModel objBankHolidayModel = new BankHolidayModel();
            try
            {

            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_List", objBankHolidayModel);
        }
        #endregion

        #region Other Function

        public void SetPermision()
        {
            try
            {
                Session[AppConstants.EditRecord] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.BankHoliday).FirstOrDefault().Edit;
                Session[AppConstants.DeleteRecord] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.BankHoliday).FirstOrDefault().Delete;
                Session[AppConstants.AddRecord] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.BankHoliday).FirstOrDefault().Add;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void BindPagingData(CommonModel objPaging)
        {
            try
            {
                objPaging.Page = CommonFunctions.ValidIntValue(Request.Form.GetValues("draw")[0]);
                string order = Request.Form.GetValues("order[0][column]")[0];
                objPaging.SortColumnDirection = Request.Form.GetValues("order[0][dir]")[0];
                objPaging.Skip = CommonFunctions.ValidIntValue(Request.Form.GetValues("start")[0]) + 1;
                objPaging.PageSize = CommonFunctions.ValidIntValue(Request.Form.GetValues("start")[0]) + CommonFunctions.ValidIntValue(Request.Form.GetValues("length")[0]);
                objPaging.ShortColumnName = Request.Form.GetValues("columns[" + order + "][name]")[0];
                objPaging.ShortColumnName = objPaging.ShortColumnName + " " + objPaging.SortColumnDirection;
                objPaging.searchValue = Request.Form.GetValues("search[value]")[0];
                if (!string.IsNullOrEmpty(objPaging.searchValue))
                {
                    objPaging.searchValue = CommonFunctions.CreateFilterQuery("HolidayName", objPaging.searchValue, "CreatedDate", "", "HolidayDate");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        private JsonResult GetMessage(int intStatuscode, string strMessage, string pstrID = "")
        {
            dynamic showMessageString = string.Empty;
            try
            {
                showMessageString = new
                {
                    statusCode = intStatuscode,
                    msg = strMessage,
                    id = pstrID,
                    Error = strMessage,
                    message = strMessage,
                };
                return Json(showMessageString, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                throw;
            }
        }
        private bool IsValidData(BankHolidayModel objBankHoliday, ref string strErrorMsg)
        {
            try
            {
                if (string.IsNullOrEmpty(objBankHoliday.HolidayName))
                {
                    strErrorMsg = Resources.Resource.EnterHoliDayName;
                    return false;
                }
                if (objBankHoliday.HolidayDate==null)
                {
                    strErrorMsg = Resources.Resource.EnterHoliDayDate;
                    return false;
                }
                //else if (!CommonFunctions.IsValidString(objBankHoliday.HolidayName))
                //{
                //    strErrorMsg = "Please enter valid holiday name.";
                //    return false;
                //}
                else
                {
                    if (db.BankHoliday.ExistName(objBankHoliday))
                    {
                        strErrorMsg = Resources.Resource.HoliDayExists;
                        return false;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return true;
        }

        #endregion
    }
}