﻿using NHSC.Models.Entities;
using NHSC.Models.Common;
using NHSC.Repository;
using NHSC.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NHSC.Security;
using NHSC.Common;

namespace NHSC.Controllers
{
    [CheckSessionOut(Values = (int)Menu.MultidisciplinaryRota)]
    [CustomErrorHandling]
    public class MultidisciplinaryRotaController : BaseController
    {

        #region Private Members
        private IUnitOfWork db;
        CommonFunctions objCommon;
        #endregion

        public MultidisciplinaryRotaController()
        {
            db = new UnitOfWork();
            objCommon = new CommonFunctions();
        }
        #region Crud Opration
        public ActionResult Index()
        {
            MultidisciplinaryRotaModel objMultidisciplinaryRota = new MultidisciplinaryRotaModel();
            try
            {
                SetPermision();
            }
            catch (Exception)
            {
                throw;
            }
            return View(objMultidisciplinaryRota);
        }

        [HttpPost]
        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public ActionResult Index(MultidisciplinaryRotaModel objMultidisciplinaryRota)
        {
            string strErrorMsg = string.Empty;
            int intStatuscode = 1;
            bool _status = false;
            try
            {
                objMultidisciplinaryRota.ModifyBy = User.UserId;
                objMultidisciplinaryRota.ModifyDate = CommonFunctions.Datetimesetting();
                if (!IsValidData(objMultidisciplinaryRota, ref strErrorMsg))
                {
                    return GetMessage(0, strErrorMsg, objMultidisciplinaryRota.MultidisciplinaryId.ToString());
                }
                _status = db.MultidisciplinaryRota.InsertData(objMultidisciplinaryRota, ref strErrorMsg);
                if (objMultidisciplinaryRota.MultidisciplinaryId == 0)
                {
                    strErrorMsg = Resources.Resource.RecordSaveSuccessfully;
                }
                else
                {
                    strErrorMsg = Resources.Resource.RecordUpdateSuccessfully;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return GetMessage(intStatuscode, strErrorMsg, objMultidisciplinaryRota.MultidisciplinaryId.ToString());
        }

        [HttpPost]
        public ActionResult DeleteRecord(long Id)
        {
            string strErrorMsg = string.Empty;
            int intStatuscode = 1;
            try
            {
                if (!db.Common.IsAssignToAnyModule(AppConstants.MultidisciplinaryRota, Id, ref strErrorMsg))
                {
                    bool Status = db.MultidisciplinaryRota.DeleteData(Id, User.UserId, CommonFunctions.Datetimesetting());
                    strErrorMsg = Resources.Resource.RecordDeletedSuccessfully;
                }
                else
                {
                    intStatuscode = 0;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return GetMessage(intStatuscode, strErrorMsg, Id.ToString());
        }

        #endregion

        #region Get Method

        public ActionResult GetListJson()
        {
            List<MultidisciplinaryRotaModel> objMultidisciplinaryRotaList = new List<MultidisciplinaryRotaModel>();
            CommonModel objPaging = new CommonModel();
            try
            {
                BindPagingData(objPaging);
                objMultidisciplinaryRotaList = db.MultidisciplinaryRota.GetList(objPaging);

                return Json(new { draw = objPaging.Page, recordsFiltered = objPaging.TotalCount, recordsTotal = objPaging.TotalCount, data = objMultidisciplinaryRotaList });
            }
            catch (Exception)
            {
                throw;
            }
        }
        public ActionResult GetForm(long Id = 0)
        {
            MultidisciplinaryRotaModel objMultidisciplinaryRota = new MultidisciplinaryRotaModel();
            try
            {
                ViewBag.UserList = new SelectList(db.User.UserList(), "Value", "Text");
                ViewBag.RotaTemplateList = new SelectList(db.RotaTemplate.GetRotaTemplate(""), "RotaTemplateId", "RotaTemplateName");
                objMultidisciplinaryRota = db.MultidisciplinaryRota.GetDetails(Id);
            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_Form", objMultidisciplinaryRota);
        }
        public ActionResult GetView(long Id = 0)
        {
            MultidisciplinaryRotaModel objMultidisciplinaryRota = new MultidisciplinaryRotaModel();
            try
            {
                ViewBag.UserList = new SelectList(db.User.UserList(), "Value", "Text");
                ViewBag.RotaTemplateList = new SelectList(db.RotaTemplate.GetRotaTemplate(""), "RotaTemplateId", "RotaTemplateName");
                objMultidisciplinaryRota = db.MultidisciplinaryRota.GetDetails(Id);
            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_View", objMultidisciplinaryRota);
        }
        public ActionResult GetList()
        {
            MultidisciplinaryRotaModel objMultidisciplinaryRotaModel = new MultidisciplinaryRotaModel();
            try
            {

            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_List", objMultidisciplinaryRotaModel);
        }

        #endregion

        #region Other Function

        public void SetPermision()
        {
            try
            {
                Session[AppConstants.EditRecord] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.MultidisciplinaryRota).FirstOrDefault().Edit;
                Session[AppConstants.DeleteRecord] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.MultidisciplinaryRota).FirstOrDefault().Delete;
                Session[AppConstants.AddRecord] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.MultidisciplinaryRota).FirstOrDefault().Add;
                Session[AppConstants.IsShowAll] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.MultidisciplinaryRota).FirstOrDefault().IsShowAll;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void BindPagingData(CommonModel objPaging)
        {
            try
            {
                objPaging.Page = CommonFunctions.ValidIntValue(Request.Form.GetValues("draw")[0]);
                string order = Request.Form.GetValues("order[0][column]")[0];
                objPaging.SortColumnDirection = Request.Form.GetValues("order[0][dir]")[0];
                objPaging.Skip = CommonFunctions.ValidIntValue(Request.Form.GetValues("start")[0]) + 1;
                objPaging.PageSize = CommonFunctions.ValidIntValue(Request.Form.GetValues("start")[0]) + CommonFunctions.ValidIntValue(Request.Form.GetValues("length")[0]);
                objPaging.ShortColumnName = Request.Form.GetValues("columns[" + order + "][name]")[0];
                objPaging.ShortColumnName = objPaging.ShortColumnName + " " + objPaging.SortColumnDirection;
                objPaging.searchValue = Request.Form.GetValues("search[value]")[0];
                if (!string.IsNullOrEmpty(objPaging.searchValue))
                {
                    objPaging.searchValue = CommonFunctions.CreateFilterQuery("MultidisciplinaryName,RotaTemplateName,UserName", objPaging.searchValue, "CreatedDate", "", "");
                }

                objPaging.Id = User.UserId;

            }
            catch (Exception)
            {

                throw;
            }
        }
        private JsonResult GetMessage(int intStatuscode, string strMessage, string pstrID = "")
        {
            dynamic showMessageString = string.Empty;
            try
            {
                showMessageString = new
                {
                    statusCode = intStatuscode,
                    msg = strMessage,
                    id = pstrID,
                    Error = strMessage,
                    message = strMessage,
                };
                return Json(showMessageString, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                throw;
            }
        }
        private bool IsValidData(MultidisciplinaryRotaModel objMultidisciplinaryRota, ref string strErrorMsg)
        {
            try
            {
                if (string.IsNullOrEmpty(objMultidisciplinaryRota.MultidisciplinaryName))
                {
                    strErrorMsg = "Enter Rota Template Name";
                    return false;
                }
                else if (string.IsNullOrEmpty(objMultidisciplinaryRota.RotaTemplateIds))
                {
                    strErrorMsg = "Select atleast one rota template";
                    return false;
                }
                else if (string.IsNullOrEmpty(objMultidisciplinaryRota.UserIds))
                {
                    strErrorMsg = "Select atleast one user";
                    return false;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return true;
        }

        #endregion
    }
}