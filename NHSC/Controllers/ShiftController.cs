﻿using NHSC.Models.Entities;
using NHSC.Models.Common;
using NHSC.Repository;
using NHSC.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NHSC.Security;
using NHSC.Common;

namespace NHSC.Controllers
{
    [CheckSessionOut(Values = (int)Menu.Shift)]
    [CustomErrorHandling]
    public class ShiftController : BaseController
    {

        #region Private Members
        private IUnitOfWork db;
        CommonFunctions objCommon;
        #endregion

        public ShiftController()
        {
            db = new UnitOfWork();
            objCommon = new CommonFunctions();
        }
        #region Crud Opration
        public ActionResult Index()
        {
            ShiftModel objShift = new ShiftModel();
            try
            {
                SetPermision();
            }
            catch (Exception)
            {
                throw;
            }
            return View(objShift);
        }

        [HttpPost]
        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public ActionResult Index(ShiftModel objShift)
        {
            string strErrorMsg = string.Empty;
            int intStatuscode = 1;
            bool _status = false;
            try
            {
                objShift.ModifyBy = User.UserId;
                objShift.ModifyDate = CommonFunctions.Datetimesetting();
                if (!IsValidData(objShift, ref strErrorMsg))
                {
                    return GetMessage(0, strErrorMsg, objShift.ShiftId.ToString());
                }
                _status = db.Shift.InsertData(objShift, ref strErrorMsg);
                if (objShift.ShiftId == 0)
                {
                    strErrorMsg = Resources.Resource.RecordSaveSuccessfully;
                }
                else
                {
                    strErrorMsg = Resources.Resource.RecordUpdateSuccessfully;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return GetMessage(intStatuscode, strErrorMsg, objShift.ShiftId.ToString());
        }

        [HttpPost]
        public ActionResult DeleteRecord(long Id)
        {
            string strErrorMsg = string.Empty;
            int intStatuscode = 1;
            try
            {
                if (!db.Common.IsAssignToAnyModule(AppConstants.Shift, Id, ref strErrorMsg))
                {
                    bool Status = db.Shift.DeleteData(Id, User.UserId, CommonFunctions.Datetimesetting());
                    strErrorMsg = Resources.Resource.RecordDeletedSuccessfully;
                }
                else
                {
                    intStatuscode = 0;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return GetMessage(intStatuscode, strErrorMsg, Id.ToString());
        }
        #endregion

        #region Get Method

        public ActionResult GetListJson()
        {
            List<ShiftModel> objShiftList = new List<ShiftModel>();
            CommonModel objPaging = new CommonModel();
            try
            {
                BindPagingData(objPaging);

                objShiftList = db.Shift.GetList(objPaging);

                return Json(new { draw = objPaging.Page, recordsFiltered = objPaging.TotalCount, recordsTotal = objPaging.TotalCount, data = objShiftList });
            }
            catch (Exception)
            {
                throw;
            }
        }
        public ActionResult GetForm(long Id = 0)
        {
            ShiftModel objShift = new ShiftModel();
            try
            {
                ViewBag.StaffList = new SelectList(db.Staff.GetStaff(User.UserId), "Value", "Text");
                ViewBag.SkillSetList = new SelectList(db.SkillSet.GetSkillSet(), "Value", "Text");
                ViewBag.ColorList = new SelectList(db.Color.GetColor(), "Value", "Text");
                ViewBag.DepartmentList = new SelectList(db.Departments.GetDepartments(), "Value", "Text");
                objShift = db.Shift.GetDetails(Id);
            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_Form", objShift);
        }
        public ActionResult GetView(long Id = 0)
        {
            ShiftModel objShift = new ShiftModel();
            try
            {
                ViewBag.StaffList = new SelectList(db.Staff.GetStaff(User.UserId), "Value", "Text");
                ViewBag.SkillSetList = new SelectList(db.SkillSet.GetSkillSet(), "Value", "Text");
                ViewBag.ColorList = new SelectList(db.Color.GetColor(), "Value", "Text");
                ViewBag.DepartmentList = new SelectList(db.Departments.GetDepartments(), "Value", "Text");
                objShift = db.Shift.GetDetails(Id);
            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_View", objShift);
        }
        public ActionResult GetList()
        {
            ShiftModel objShiftModel = new ShiftModel();
            try
            {

            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_List", objShiftModel);
        }
        #endregion

        #region Other Function

        public void SetPermision()
        {
            try
            {
                Session[AppConstants.EditRecord] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.Shift).FirstOrDefault().Edit;
                Session[AppConstants.DeleteRecord] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.Shift).FirstOrDefault().Delete;
                Session[AppConstants.AddRecord] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.Shift).FirstOrDefault().Add;
                Session[AppConstants.IsShowAll] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.Shift).FirstOrDefault().IsShowAll;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void BindPagingData(CommonModel objPaging)
        {
            try
            {
                objPaging.Page = CommonFunctions.ValidIntValue(Request.Form.GetValues("draw")[0]);
                string order = Request.Form.GetValues("order[0][column]")[0];
                objPaging.SortColumnDirection = Request.Form.GetValues("order[0][dir]")[0];
                objPaging.Skip = CommonFunctions.ValidIntValue(Request.Form.GetValues("start")[0]) + 1;
                objPaging.PageSize = CommonFunctions.ValidIntValue(Request.Form.GetValues("start")[0]) + CommonFunctions.ValidIntValue(Request.Form.GetValues("length")[0]);
                objPaging.ShortColumnName = Request.Form.GetValues("columns[" + order + "][name]")[0];
                if (objPaging.ShortColumnName == "CreatedDate")
                {
                    objPaging.ShortColumnName = "sf." + objPaging.ShortColumnName + " " + objPaging.SortColumnDirection;
                }
                else if (objPaging.ShortColumnName == "SkillSetName")
                {
                    objPaging.ShortColumnName = "ss." + objPaging.ShortColumnName + " " + objPaging.SortColumnDirection;
                }
                else
                {
                    objPaging.ShortColumnName = objPaging.ShortColumnName + " " + objPaging.SortColumnDirection;
                }
                objPaging.searchValue = Request.Form.GetValues("search[value]")[0];
                if (!string.IsNullOrEmpty(objPaging.searchValue))
                {
                    objPaging.searchValue = CommonFunctions.CreateFilterQuery("sf.ShiftName,s.TitleName,s.Forename,c.ColorName,ss.SkillSetName", objPaging.searchValue, "sf.CreatedDate", "sf.StartTime,sf.EndTime");
                }
                objPaging.Id = objCommon.LoginUserDetails.UserId;
            }
            catch (Exception)
            {

                throw;
            }
        }
        private JsonResult GetMessage(int intStatuscode, string strMessage, string pstrID = "")
        {
            dynamic showMessageString = string.Empty;
            try
            {
                showMessageString = new
                {
                    statusCode = intStatuscode,
                    msg = strMessage,
                    id = pstrID,
                    Error = strMessage,
                    message = strMessage,
                };
                return Json(showMessageString, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                throw;
            }
        }
        private bool IsValidData(ShiftModel objShift, ref string strErrorMsg)
        {
            try
            {
                System.TimeSpan diff = objShift.EndTime.Subtract(objShift.StartTime);

                if (string.IsNullOrEmpty(objShift.ShiftName))
                {
                    strErrorMsg = "Enter Shif Name";
                    return false;
                }
                else if (objShift.SkillSetId == 0)
                {
                    strErrorMsg = "Select atleast one skillset";
                    return false;
                }
                else if (objShift.ColorId == 0)
                {
                    strErrorMsg = "Select atleast one color";
                    return false;
                }
                //else if (objShift.StaffId == 0)
                //{
                //    strErrorMsg = "Select atleast one staff";
                //    return false;
                //}
                //else if (CommonFunctions.ValidlongValue(diff.TotalMinutes.ToString().Replace("-","31")) < AppConstants.MinutedifferenceTime)
                //{
                //    strErrorMsg = "Alteast " + AppConstants.MinutedifferenceTime + " difference required between start and end time";
                //    return false;
                //}
                else
                {
                    //if (db.Shift.ExistStaffInSameShif(objShift))
                    //{
                    //    strErrorMsg = "Staff is already assigned as default in another shift that covers the same time period";
                    //    return false;
                    //}
                }
            }
            catch (Exception)
            {
                throw;
            }
            return true;
        }

        #endregion
    }
}