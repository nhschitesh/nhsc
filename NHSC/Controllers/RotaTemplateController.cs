﻿using NHSC.Models.Entities;
using NHSC.Models.Common;
using NHSC.Repository;
using NHSC.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NHSC.Security;
using NHSC.Common;

namespace NHSC.Controllers
{
    [CheckSessionOut(Values = (int)Menu.TemplateRota)]
    [CustomErrorHandling]
    public class RotaTemplateController : BaseController
    {

        #region Private Members
        private IUnitOfWork db;
        CommonFunctions objCommon;
        #endregion

        public RotaTemplateController()
        {
            db = new UnitOfWork();
            objCommon = new CommonFunctions();
        }
        #region Crud Opration
        public ActionResult Index()
        {
            RotaTemplateModel objRotaTemplate = new RotaTemplateModel();
            try
            {
                SetPermision();
            }
            catch (Exception)
            {
                throw;
            }
            return View(objRotaTemplate);
        }

        [HttpPost]
        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public ActionResult Index(RotaTemplateModel objRotaTemplate)
        {
            string strErrorMsg = string.Empty;
            int intStatuscode = 1;
            bool _status = false;
            try
            {
                objRotaTemplate.ModifyBy = User.UserId;
                objRotaTemplate.ModifyDate = CommonFunctions.Datetimesetting();
                if (!IsValidData(objRotaTemplate, ref strErrorMsg))
                {
                    return GetMessage(0, strErrorMsg, objRotaTemplate.RotaTemplateId.ToString());
                }
                _status = db.RotaTemplate.InsertData(objRotaTemplate, ref strErrorMsg);
                if (objRotaTemplate.RotaTemplateId == 0)
                {
                    strErrorMsg = Resources.Resource.RecordSaveSuccessfully;
                }
                else
                {
                    strErrorMsg = Resources.Resource.RecordUpdateSuccessfully;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return GetMessage(intStatuscode, strErrorMsg, objRotaTemplate.RotaTemplateId.ToString());
        }

        [HttpPost]
        public ActionResult DeleteRecord(long Id)
        {
            string strErrorMsg = string.Empty;
            int intStatuscode = 1;
            try
            {
                if (!db.Common.IsAssignToAnyModule(AppConstants.RotaTemplate, Id, ref strErrorMsg))
                {
                    bool Status = db.RotaTemplate.DeleteData(Id, User.UserId, CommonFunctions.Datetimesetting());
                    strErrorMsg = Resources.Resource.RecordDeletedSuccessfully;
                }
                else
                {
                    intStatuscode = 0;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return GetMessage(intStatuscode, strErrorMsg, Id.ToString());
        }

        [HttpPost]
        public ActionResult CheckIsAssignNewStaff(long Id)
        {
            string strErrorMsg = string.Empty;
            int intStatuscode = 1;
            try
            {
                bool Status = db.RotaTemplate.IsAssignNewStaff(Id);
                if (Status)
                {
                    intStatuscode = 0;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return GetMessage(intStatuscode, strErrorMsg, Id.ToString());
        }

        #endregion

        #region Get Method

        public ActionResult GetListJson()
        {
            List<RotaTemplateModel> objRotaTemplateList = new List<RotaTemplateModel>();
            CommonModel objPaging = new CommonModel();
            try
            {
                BindPagingData(objPaging);
                objRotaTemplateList = db.RotaTemplate.GetList(objPaging);

                return Json(new { draw = objPaging.Page, recordsFiltered = objPaging.TotalCount, recordsTotal = objPaging.TotalCount, data = objRotaTemplateList });
            }
            catch (Exception)
            {
                throw;
            }
        }
        public ActionResult GetForm(long Id = 0)
        {
            RotaTemplateModel objRotaTemplate = new RotaTemplateModel();
            try
            {
                ViewBag.ShiftList = new SelectList(db.Shift.GetShift(Id), "Value", "Text");
                ViewBag.SiteList = new SelectList(db.Sites.GetSites(), "Value", "Text");
                ViewBag.DepartmentList = new SelectList(db.Departments.GetDepartments(), "Value", "Text");
                ViewBag.UserList = new SelectList(db.User.UserDepartmentList(), "Value", "Text");
                ViewBag.StartDayOfWeekList = new SelectList(SelectListControl.GetWeekName(), "Value", "Text");


                objRotaTemplate = db.RotaTemplate.GetDetails(Id);
            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_Form", objRotaTemplate);
        }
        public ActionResult GetView(long Id = 0)
        {
            RotaTemplateModel objRotaTemplate = new RotaTemplateModel();
            try
            {
                ViewBag.ShiftList = new SelectList(db.Shift.GetShift(Id), "Value", "Text");
                ViewBag.SiteList = new SelectList(db.Sites.GetSites(), "Value", "Text");
                ViewBag.DepartmentList = new SelectList(db.Departments.GetDepartments(), "Value", "Text");
                ViewBag.UserList = new SelectList(db.User.UserDepartmentList(), "Value", "Text");
                ViewBag.StartDayOfWeekList = new SelectList(SelectListControl.GetWeekName(), "Value", "Text");
                objRotaTemplate = db.RotaTemplate.GetDetails(Id);
            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_View", objRotaTemplate);
        }
        public ActionResult GetList()
        {
            RotaTemplateModel objRotaTemplateModel = new RotaTemplateModel();
            try
            {

            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_List", objRotaTemplateModel);
        }
        public ActionResult GetRotaTemplate(string SearchValue)
        {
            List<RotaTemplateModel> RotaTemplateList = new List<RotaTemplateModel>();
            try
            {
                RotaTemplateList = db.RotaTemplate.GetRotaTemplate(SearchValue);
                return Json(RotaTemplateList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ActionResult GetDateForm(long Id = 0, int DayWeek = 0)
        {
            RotaTemplateModel objRotaTemplate = new RotaTemplateModel();
            string WeekDays = "0,1,2,3,4,5,6";
            string[] arryweekday;
            try
            {
                int num = (int)CommonFunctions.Datetimesetting().DayOfWeek;
                if (DayWeek == num)
                {
                    objRotaTemplate.GeneratedUpToDate = CommonFunctions.Datetimesetting().AddDays(7);
                    objRotaTemplate.GeneratedUpToDates = CommonFunctions.Datetimesetting().AddDays(7).ToString(AppConstants.DateShowFormate);
                }
                else if (DayWeek > num)
                {
                    objRotaTemplate.GeneratedUpToDate = CommonFunctions.Datetimesetting().AddDays((DayWeek - num));
                    objRotaTemplate.GeneratedUpToDates = CommonFunctions.Datetimesetting().AddDays((DayWeek - num)).ToString(AppConstants.DateShowFormate);
                }
                else if (DayWeek < num)
                {
                    objRotaTemplate.GeneratedUpToDate = CommonFunctions.Datetimesetting().AddDays(7).AddDays((DayWeek - num));
                    objRotaTemplate.GeneratedUpToDates = CommonFunctions.Datetimesetting().AddDays(7).AddDays((DayWeek - num)).ToString(AppConstants.DateShowFormate);
                }

                arryweekday = WeekDays.Split(',');

                arryweekday = arryweekday.Where(val => val != DayWeek.ToString()).ToArray();
                objRotaTemplate.WeekDays = String.Join(",", arryweekday);
                objRotaTemplate.StartDayOfWeekName = objRotaTemplate.GeneratedUpToDate.DayOfWeek.ToString();
                objRotaTemplate.StartDayOfWeek = DayWeek;
                objRotaTemplate.RotaTemplateId = Id;
            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_DateForm", objRotaTemplate);
        }


        public ActionResult GenerateActualRota(long Id = 0, int DayWeek = 0, string GenerateDate = "")
        {
            RotaTemplateModel objRotaTemplate = new RotaTemplateModel();
            try
            {
                objRotaTemplate.StartDayOfWeek = DayWeek;
                objRotaTemplate.RotaTemplateId = Id;
                objRotaTemplate.GeneratedUpToDates = GenerateDate;
                objRotaTemplate.ModifyBy = User.UserId;
                objRotaTemplate.ModifyDate = CommonFunctions.Datetimesetting();
                db.ActualRota.InsertActualRota(objRotaTemplate);
            }
            catch (Exception)
            {
                throw;
            }
            return GetMessage(1, "", GenerateDate);
        }

        public ActionResult GetOrderForm()
        {
            RotaShiftOrderModel objShift = new RotaShiftOrderModel();
            try
            {
                ViewBag.RotaTemplateList = new SelectList(db.RotaTemplate.GetRotaTemplate(""), "RotaTemplateId", "RotaTemplateName");
            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_OrderForm", objShift);
        }

        public ActionResult GetOrderData(long Id)
        {
            RotaShiftOrderModel objShift = new RotaShiftOrderModel();
            try
            {
                objShift.ShiftOrderList = db.Shift.GetShiftByRota(Id);
            }
            catch (Exception)
            {
                throw;
            }
            return Json(objShift, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public ActionResult UpdateShiftOrder(RotaShiftOrderModel objShift)
        {
            string strErrorMsg = string.Empty;
            int intStatuscode = 1;
            bool _status = false;
            try
            {
                objShift.ModifyBy = User.UserId;
                objShift.ModifyDate = CommonFunctions.Datetimesetting();
             
                _status = db.Shift.InsertDataShiftOrder(objShift, ref strErrorMsg);
                strErrorMsg = Resources.Resource.RecordUpdateSuccessfully;
            }
            catch (Exception)
            {
                throw;
            }
            return GetMessage(intStatuscode, strErrorMsg, objShift.RotaTemplateId.ToString());
        }

        #endregion

        #region Other Function

        public void SetPermision()
        {
            try
            {
                Session[AppConstants.EditRecord] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.TemplateRota).FirstOrDefault().Edit;
                Session[AppConstants.DeleteRecord] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.TemplateRota).FirstOrDefault().Delete;
                Session[AppConstants.AddRecord] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.TemplateRota).FirstOrDefault().Add;
                Session[AppConstants.IsShowAll] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.TemplateRota).FirstOrDefault().IsShowAll;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void BindPagingData(CommonModel objPaging)
        {
            try
            {
                objPaging.Page = CommonFunctions.ValidIntValue(Request.Form.GetValues("draw")[0]);
                string order = Request.Form.GetValues("order[0][column]")[0];
                objPaging.SortColumnDirection = Request.Form.GetValues("order[0][dir]")[0];
                objPaging.Skip = CommonFunctions.ValidIntValue(Request.Form.GetValues("start")[0]) + 1;
                objPaging.PageSize = CommonFunctions.ValidIntValue(Request.Form.GetValues("start")[0]) + CommonFunctions.ValidIntValue(Request.Form.GetValues("length")[0]);
                objPaging.ShortColumnName = Request.Form.GetValues("columns[" + order + "][name]")[0];
                objPaging.ShortColumnName = objPaging.ShortColumnName + " " + objPaging.SortColumnDirection;
                if (objPaging.ShortColumnName == "CreatedDate")
                {
                    objPaging.ShortColumnName = "rt." + objPaging.ShortColumnName + " " + objPaging.SortColumnDirection;
                }
                objPaging.searchValue = Request.Form.GetValues("search[value]")[0];
                if (!string.IsNullOrEmpty(objPaging.searchValue))
                {
                    objPaging.searchValue = CommonFunctions.CreateFilterQuery("RotaTemplateName,DepartmentName,SiteName,StartDayOfWeek,DurationInDays,ShiftName,UserName", objPaging.searchValue, "rt.CreatedDate", "", "GeneratedUpToDate");
                }

                objPaging.Id = objCommon.LoginUserDetails.UserId;

            }
            catch (Exception)
            {

                throw;
            }
        }
        private JsonResult GetMessage(int intStatuscode, string strMessage, string pstrID = "")
        {
            dynamic showMessageString = string.Empty;
            try
            {
                showMessageString = new
                {
                    statusCode = intStatuscode,
                    msg = strMessage,
                    id = pstrID,
                    Error = strMessage,
                    message = strMessage,
                };
                return Json(showMessageString, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                throw;
            }
        }
        private bool IsValidData(RotaTemplateModel objRotaTemplate, ref string strErrorMsg)
        {
            try
            {
                if (string.IsNullOrEmpty(objRotaTemplate.RotaTemplateName))
                {
                    strErrorMsg = "Enter Rota Template Name";
                    return false;
                }
                else if (objRotaTemplate.SiteId == 0)
                {
                    strErrorMsg = "Select atleast one site";
                    return false;
                }
                else if (objRotaTemplate.DepartmentId == 0)
                {
                    strErrorMsg = "Select atleast one department";
                    return false;
                }
                else if (objRotaTemplate.StartDayOfWeek == 0)
                {
                    strErrorMsg = "Select atleast one day of week";
                    return false;
                }
                else if (string.IsNullOrEmpty(objRotaTemplate.ShiftIds))
                {
                    strErrorMsg = "Select atleast one shift";
                    return false;
                }
                else if (string.IsNullOrEmpty(objRotaTemplate.UserIds))
                {
                    strErrorMsg = "Select atleast one user";
                    return false;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return true;
        }

        #endregion
    }
}