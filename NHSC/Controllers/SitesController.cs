﻿using NHSC.Models.Entities;
using NHSC.Models.Common;
using NHSC.Repository;
using NHSC.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NHSC.Security;
using NHSC.Common;

namespace NHSC.Controllers
{
    [CheckSessionOut(Values = (int)Menu.Sites)]
    [CustomErrorHandling]
    public class SitesController : BaseController
    {

        #region Private Members
        private IUnitOfWork db;
        CommonFunctions objCommon;
        #endregion

        public SitesController()
        {
            db = new UnitOfWork();
            objCommon = new CommonFunctions();
        }
        #region Crud Opration
        public ActionResult Index()
        {
            SitesModel objSites = new SitesModel();
            try
            {
                SetPermision();
            }
            catch (Exception)
            {
                throw;
            }
            return View(objSites);
        }

        [HttpPost]
        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public ActionResult Index(SitesModel objSites)
        {
            string strErrorMsg = string.Empty;
            int intStatuscode = 1;
            bool _status = false;
            try
            {
                objSites.ModifyBy = User.UserId;
                objSites.ModifyDate = CommonFunctions.Datetimesetting();
                if (!IsValidData(objSites, ref strErrorMsg))
                {
                    return GetMessage(0, strErrorMsg, objSites.SiteId.ToString());
                }
                _status = db.Sites.InsertData(objSites, ref strErrorMsg);
                if (objSites.SiteId == 0)
                {
                    strErrorMsg = Resources.Resource.RecordSaveSuccessfully;
                }
                else
                {
                    strErrorMsg = Resources.Resource.RecordUpdateSuccessfully;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return GetMessage(intStatuscode, strErrorMsg, objSites.SiteId.ToString());
        }

        [HttpPost]
        public ActionResult DeleteRecord(long Id)
        {
            string strErrorMsg = string.Empty;
            int intStatuscode = 1;
            try
            {
                if (!db.Common.IsAssignToAnyModule(AppConstants.Site, Id, ref strErrorMsg))
                {
                    bool Status = db.Sites.DeleteData(Id, User.UserId, CommonFunctions.Datetimesetting());
                    strErrorMsg = Resources.Resource.RecordDeletedSuccessfully;
                }
                else
                {
                    intStatuscode = 0;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return GetMessage(intStatuscode, strErrorMsg, Id.ToString());
        }
        #endregion

        #region Get Method

        public ActionResult GetListJson()
        {
            List<SitesModel> objSitesList = new List<SitesModel>();
            CommonModel objPaging = new CommonModel();
            try
            {
                BindPagingData(objPaging);

                objSitesList = db.Sites.GetList(objPaging);

                return Json(new { draw = objPaging.Page, recordsFiltered = objPaging.TotalCount, recordsTotal = objPaging.TotalCount, data = objSitesList });
            }
            catch (Exception)
            {
                throw;
            }
        }
        public ActionResult GetForm(long Id = 0)
        {
            SitesModel objSites = new SitesModel();
            try
            {
                objSites = db.Sites.GetDetails(Id);
            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_Form", objSites);
        }
        public ActionResult GetView(long Id = 0)
        {
            SitesModel objSites = new SitesModel();
            try
            {
                objSites = db.Sites.GetDetails(Id);
            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_View", objSites);
        }
        public ActionResult GetList()
        {
            SitesModel objSitesModel = new SitesModel();
            try
            {

            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_List", objSitesModel);
        }
        #endregion

        #region Other Function

        public void SetPermision()
        {
            try
            {
                Session[AppConstants.EditRecord] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.Sites).FirstOrDefault().Edit;
                Session[AppConstants.DeleteRecord] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.Sites).FirstOrDefault().Delete;
                Session[AppConstants.AddRecord] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.Sites).FirstOrDefault().Add;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void BindPagingData(CommonModel objPaging)
        {
            try
            {
                objPaging.Page = CommonFunctions.ValidIntValue(Request.Form.GetValues("draw")[0]);
                string order = Request.Form.GetValues("order[0][column]")[0];
                objPaging.SortColumnDirection = Request.Form.GetValues("order[0][dir]")[0];
                objPaging.Skip = CommonFunctions.ValidIntValue(Request.Form.GetValues("start")[0]) + 1;
                objPaging.PageSize = CommonFunctions.ValidIntValue(Request.Form.GetValues("start")[0]) + CommonFunctions.ValidIntValue(Request.Form.GetValues("length")[0]);
                objPaging.ShortColumnName = Request.Form.GetValues("columns[" + order + "][name]")[0];
                objPaging.ShortColumnName = objPaging.ShortColumnName + " " + objPaging.SortColumnDirection;
                objPaging.searchValue = Request.Form.GetValues("search[value]")[0];
                if (!string.IsNullOrEmpty(objPaging.searchValue))
                {
                    objPaging.searchValue = CommonFunctions.CreateFilterQuery("SiteName", objPaging.searchValue, "CreatedDate", "");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        private JsonResult GetMessage(int intStatuscode, string strMessage, string pstrID = "")
        {
            dynamic showMessageString = string.Empty;
            try
            {
                showMessageString = new
                {
                    statusCode = intStatuscode,
                    msg = strMessage,
                    id = pstrID,
                    Error = strMessage,
                    message = strMessage,
                };
                return Json(showMessageString, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                throw;
            }
        }
        private bool IsValidData(SitesModel objSites, ref string strErrorMsg)
        {
            try
            {
                if (string.IsNullOrEmpty(objSites.SiteName))
                {
                    strErrorMsg = Resources.Resource.EnterSiteName;
                    return false;
                }
                else if (!CommonFunctions.IsValidString(objSites.SiteName))
                {
                    strErrorMsg = Resources.Resource.ValidSiteName;
                    return false;
                }
                else
                {
                    if (db.Sites.ExistName(objSites))
                    {
                        strErrorMsg = Resources.Resource.SiteNameExists;
                        return false;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return true;
        }

        #endregion
    }
}