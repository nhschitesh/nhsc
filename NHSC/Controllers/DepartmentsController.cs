﻿using NHSC.Models.Entities;
using NHSC.Models.Common;
using NHSC.Repository;
using NHSC.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NHSC.Security;
using NHSC.Common;

namespace NHSC.Controllers
{
    [CheckSessionOut(Values = (int)Menu.Departments)]
    [CustomErrorHandling]
    public class DepartmentsController : BaseController
    {

        #region Private Members
        private IUnitOfWork db;
        CommonFunctions objCommon;
        #endregion

        public DepartmentsController()
        {
            db = new UnitOfWork();
            objCommon = new CommonFunctions();
        }
        #region Crud Opration
        public ActionResult Index()
        {
            DepartmentsModel objDepartments = new DepartmentsModel();
            try
            {
                SetPermision();
            }
            catch (Exception)
            {
                throw;
            }
            return View(objDepartments);
        }

        [HttpPost]
        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public ActionResult Index(DepartmentsModel objDepartments)
        {
            string strErrorMsg = string.Empty;
            int intStatuscode = 1;
            bool _status = false;
            try
            {
                objDepartments.ModifyBy = User.UserId;
                objDepartments.ModifyDate = CommonFunctions.Datetimesetting();
                if (!IsValidData(objDepartments, ref strErrorMsg))
                {
                    return GetMessage(0, strErrorMsg, objDepartments.DepartmentId.ToString());
                }
                _status = db.Departments.InsertData(objDepartments, ref strErrorMsg);
                if (objDepartments.DepartmentId == 0)
                {
                    strErrorMsg = Resources.Resource.RecordSaveSuccessfully;
                }
                else
                {
                    strErrorMsg = Resources.Resource.RecordUpdateSuccessfully;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return GetMessage(intStatuscode, strErrorMsg, objDepartments.DepartmentId.ToString());
        }

        [HttpPost]
        public ActionResult DeleteRecord(long Id)
        {
            string strErrorMsg = string.Empty;
            int intStatuscode = 1;
            try
            {
                if (!db.Common.IsAssignToAnyModule(AppConstants.Department, Id, ref strErrorMsg))
                {
                    bool Status = db.Departments.DeleteData(Id, User.UserId, CommonFunctions.Datetimesetting());
                    strErrorMsg = Resources.Resource.RecordDeletedSuccessfully;
                }
                else
                {
                    intStatuscode = 0;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return GetMessage(intStatuscode, strErrorMsg, Id.ToString());
        }
        #endregion

        #region Get Method

        public ActionResult GetListJson()
        {
            List<DepartmentsModel> objDepartmentsList = new List<DepartmentsModel>();
            CommonModel objPaging = new CommonModel();
            try
            {
                BindPagingData(objPaging);

                objDepartmentsList = db.Departments.GetList(objPaging);

                return Json(new { draw = objPaging.Page, recordsFiltered = objPaging.TotalCount, recordsTotal = objPaging.TotalCount, data = objDepartmentsList });
            }
            catch (Exception)
            {
                throw;
            }
        }
        public ActionResult GetForm(long Id = 0)
        {
            DepartmentsModel objDepartments = new DepartmentsModel();
            try
            {
                ViewBag.SiteList = new SelectList(db.Sites.GetSites(), "Value", "Text");
                objDepartments = db.Departments.GetDetails(Id);
            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_Form", objDepartments);
        }
        public ActionResult GetView(long Id = 0)
        {
            DepartmentsModel objDepartments = new DepartmentsModel();
            try
            {
                ViewBag.SiteList = new SelectList(db.Sites.GetSites(), "Value", "Text");
                objDepartments = db.Departments.GetDetails(Id);
            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_View", objDepartments);
        }
        public ActionResult GetList()
        {
            DepartmentsModel objDepartmentsModel = new DepartmentsModel();
            try
            {

            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_List", objDepartmentsModel);
        }
        #endregion

        #region Other Function

        public void SetPermision()
        {
            try
            {
                Session[AppConstants.EditRecord] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.Departments).FirstOrDefault().Edit;
                Session[AppConstants.DeleteRecord] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.Departments).FirstOrDefault().Delete;
                Session[AppConstants.AddRecord] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.Departments).FirstOrDefault().Add;
                Session[AppConstants.IsShowAll] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.Departments).FirstOrDefault().IsShowAll;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void BindPagingData(CommonModel objPaging)
        {
            try
            {
                objPaging.Page = CommonFunctions.ValidIntValue(Request.Form.GetValues("draw")[0]);
                string order = Request.Form.GetValues("order[0][column]")[0];
                objPaging.SortColumnDirection = Request.Form.GetValues("order[0][dir]")[0];
                objPaging.Skip = CommonFunctions.ValidIntValue(Request.Form.GetValues("start")[0]) + 1;
                objPaging.PageSize = CommonFunctions.ValidIntValue(Request.Form.GetValues("start")[0]) + CommonFunctions.ValidIntValue(Request.Form.GetValues("length")[0]);
                objPaging.ShortColumnName = Request.Form.GetValues("columns[" + order + "][name]")[0];
                if (objPaging.ShortColumnName == "CreatedDate")
                {
                    objPaging.ShortColumnName = "d." + objPaging.ShortColumnName + " " + objPaging.SortColumnDirection;
                }
                else
                {
                    objPaging.ShortColumnName = objPaging.ShortColumnName + " " + objPaging.SortColumnDirection;
                }
                objPaging.searchValue = Request.Form.GetValues("search[value]")[0];
                if (!string.IsNullOrEmpty(objPaging.searchValue))
                {
                    objPaging.searchValue = CommonFunctions.CreateFilterQuery("DepartmentName,s.SiteName", objPaging.searchValue, "d.CreatedDate", "");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        private JsonResult GetMessage(int intStatuscode, string strMessage, string pstrID = "")
        {
            dynamic showMessageString = string.Empty;
            try
            {
                showMessageString = new
                {
                    statusCode = intStatuscode,
                    msg = strMessage,
                    id = pstrID,
                    Error = strMessage,
                    message = strMessage,
                };
                return Json(showMessageString, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                throw;
            }
        }
        private bool IsValidData(DepartmentsModel objDepartments, ref string strErrorMsg)
        {
            try
            {
                if (string.IsNullOrEmpty(objDepartments.DepartmentName))
                {
                    strErrorMsg = Resources.Resource.EnterDepartmentName;
                    return false;
                }
                //else if (!CommonFunctions.IsValidString(objDepartments.DepartmentName))
                //{
                //    strErrorMsg = Resources.Resource.ValidDepartment;
                //    return false;
                //}
               else if (objDepartments.SiteId == 0)
                {
                    strErrorMsg = Resources.Resource.SelectSite;
                    return false;
                }
                else
                {
                    if (db.Departments.ExistName(objDepartments))
                    {
                        strErrorMsg = Resources.Resource.DepartmentExists;
                        return false;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return true;
        }

        #endregion
    }
}