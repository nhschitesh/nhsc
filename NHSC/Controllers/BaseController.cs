﻿using NHSC.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NHSC.Models.Common;
namespace NHSC.Controllers
{
    public class BaseController : Controller
    {
        protected virtual new CustomPrincipal User
        {
            get
            {
                return Session[AppConstants.UserSession] as CustomPrincipal;
            }
        }

        protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, object state)
        {
            try
            {

                if (User != null)
                {
                    CultureHelper.CurrentCulture = "en-US";
                }
                else
                {
                    CultureHelper.CurrentCulture = "en-US";
                }

                return base.BeginExecuteCore(callback, state);
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            base.OnException(filterContext);
        }
    }
}