﻿using NHSC.Models.Entities;
using NHSC.Models.Common;
using NHSC.Repository;
using NHSC.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NHSC.Security;
using NHSC.Common;

namespace NHSC.Controllers
{
    [CheckSessionOut(Values = (int)Menu.UserGroup)]
    [CustomErrorHandling]
    public class UserGroupController : BaseController
    {

        #region Private Members
        private IUnitOfWork db;
        CommonFunctions objCommon;
        #endregion

        public UserGroupController()
        {
            db = new UnitOfWork();
            objCommon = new CommonFunctions();
        }
        #region Crud Opration
        public ActionResult Index()
        {
            UserGroupModel objUserGroup = new UserGroupModel();
            try
            {
                SetPermision();
            }
            catch (Exception)
            {
                throw;
            }
            return View(objUserGroup);
        }

        [HttpPost]
        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public ActionResult Index(UserGroupModel objUserGroup)
        {
            string strErrorMsg = string.Empty;
            int intStatuscode = 1;
            bool _status = false;
            try
            {
                objUserGroup.ModifyBy = User.UserId;
                objUserGroup.ModifyDate = CommonFunctions.Datetimesetting();
                if (!IsValidData(objUserGroup, ref strErrorMsg))
                {
                    return GetMessage(0, strErrorMsg, objUserGroup.UserGroupId.ToString());
                }
                _status = db.UserGroup.InsertData(objUserGroup, ref strErrorMsg);
                if (objUserGroup.UserGroupId == 0)
                {
                    strErrorMsg = Resources.Resource.RecordSaveSuccessfully;
                }
                else
                {
                    strErrorMsg = Resources.Resource.RecordUpdateSuccessfully;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return GetMessage(intStatuscode, strErrorMsg, objUserGroup.UserGroupId.ToString());
        }

        [HttpPost]
        public ActionResult DeleteRecord(long Id)
        {
            string strErrorMsg = string.Empty;
            int intStatuscode = 1;
            try
            {
                if (!db.Common.IsAssignToAnyModule(AppConstants.UserGroup, Id, ref strErrorMsg))
                {
                    bool Status = db.UserGroup.DeleteData(Id, User.UserId, CommonFunctions.Datetimesetting());
                    strErrorMsg = Resources.Resource.RecordDeletedSuccessfully;
                }
                else
                {
                    intStatuscode = 0;
                }
               
            }
            catch (Exception)
            {
                throw;
            }
            return GetMessage(intStatuscode, strErrorMsg, Id.ToString());
        }
        #endregion

        #region Get Method

        public ActionResult GetListJson()
        {
            List<UserGroupModel> objUserGroupList = new List<UserGroupModel>();
            CommonModel objPaging = new CommonModel();
            try
            {
                BindPagingData(objPaging);

                objUserGroupList = db.UserGroup.GetList(objPaging);

                return Json(new { draw = objPaging.Page, recordsFiltered = objPaging.TotalCount, recordsTotal = objPaging.TotalCount, data = objUserGroupList });
            }
            catch (Exception)
            {
                throw;
            }
        }
        public ActionResult GetForm(long Id = 0)
        {
            UserGroupModel objUserGroup = new UserGroupModel();
            try
            {
                objUserGroup = db.UserGroup.GetDetails(Id);
            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_Form", objUserGroup);
        }
        public ActionResult GetView(long Id = 0)
        {
            UserGroupModel objUserGroup = new UserGroupModel();
            try
            {
                objUserGroup = db.UserGroup.GetDetails(Id);
            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_View", objUserGroup);
        }
        public ActionResult GetList()
        {
            UserGroupModel objUserGroupModel = new UserGroupModel();
            try
            {

            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_List", objUserGroupModel);
        }
        #endregion

        #region Other Function

        public void SetPermision()
        {
            try
            {
                Session[AppConstants.EditRecord] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.UserGroup).FirstOrDefault().Edit;
                Session[AppConstants.DeleteRecord] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.UserGroup).FirstOrDefault().Delete;
                Session[AppConstants.AddRecord] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.UserGroup).FirstOrDefault().Add;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void BindPagingData(CommonModel objPaging)
        {
            try
            {
                objPaging.Page = CommonFunctions.ValidIntValue(Request.Form.GetValues("draw")[0]);
                string order = Request.Form.GetValues("order[0][column]")[0];
                objPaging.SortColumnDirection = Request.Form.GetValues("order[0][dir]")[0];
                objPaging.Skip = CommonFunctions.ValidIntValue(Request.Form.GetValues("start")[0]) + 1;
                objPaging.PageSize = CommonFunctions.ValidIntValue(Request.Form.GetValues("start")[0]) + CommonFunctions.ValidIntValue(Request.Form.GetValues("length")[0]);
                objPaging.ShortColumnName = Request.Form.GetValues("columns[" + order + "][name]")[0];
                objPaging.ShortColumnName = objPaging.ShortColumnName + " " + objPaging.SortColumnDirection;
                objPaging.searchValue = Request.Form.GetValues("search[value]")[0];
                if (!string.IsNullOrEmpty(objPaging.searchValue))
                {
                    objPaging.searchValue = CommonFunctions.CreateFilterQuery("UserGroupName", objPaging.searchValue, "CreatedDate", "");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        private JsonResult GetMessage(int intStatuscode, string strMessage, string pstrID = "")
        {
            dynamic showMessageString = string.Empty;
            try
            {
                showMessageString = new
                {
                    statusCode = intStatuscode,
                    msg = strMessage,
                    id = pstrID,
                    Error = strMessage,
                    message = strMessage,
                };
                return Json(showMessageString, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                throw;
            }
        }
        private bool IsValidData(UserGroupModel objUserGroup, ref string strErrorMsg)
        {
            try
            {
                if (string.IsNullOrEmpty(objUserGroup.UserGroupName))
                {
                    strErrorMsg = Resources.Resource.RequiredGroupName;
                    return false;
                }
                else if (!CommonFunctions.IsValidString(objUserGroup.UserGroupName))
                {
                    strErrorMsg = Resources.Resource.ValidGroupNameString;
                    return false;
                }
                else
                {
                    if (db.UserGroup.ExistName(objUserGroup))
                    {
                        strErrorMsg = Resources.Resource.ExistsGroupName;
                        return false;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return true;
        }

        #endregion
    }
}