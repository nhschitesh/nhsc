﻿using NHSC.Common;
using NHSC.Models.Common;
using NHSC.Models.Entities;
using NHSC.Repository;
using NHSC.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NHSC.Controllers
{
    [CustomErrorHandling]
    [CheckSessionOut(Values = (int)Menu.MultidisciplinaryDashboard)]
    public class MultidisciplinaryDashboardController : BaseController
    {

        #region Private Members
        private IUnitOfWork db;
        CommonFunctions objCommon;
        #endregion

        public MultidisciplinaryDashboardController()
        {
            db = new UnitOfWork();
            objCommon = new CommonFunctions();
        }
        public ActionResult Index()
        {
            OnCallModel objOnCall = new OnCallModel();
            try
            {
                ViewBag.SiteList = new SelectList(db.Sites.GetSites(), "Value", "Text");
                ViewBag.DepartmentList = new SelectList(db.Departments.GetDepartments(), "Value", "Text");
                objOnCall.OnCallDates = CommonFunctions.Datetimesetting().ToString(AppConstants.DateShowFormate);
                //objOnCall = db.OnCall.GetOnCallActualMultiRotaList(objOnCall.OnCallDates, "", "",0);
                objOnCall.OnCallDate = CommonFunctions.Datetimesetting();
                objOnCall.OnCallDates = CommonFunctions.Datetimesetting().ToString(AppConstants.DateShowFormate);
                objOnCall.OnCallFullDates = CommonFunctions.Datetimesetting().ToString(AppConstants.DateTimeShowFormate);
                ViewBag.MultiList = new SelectList(db.MultidisciplinaryRota.GetMultidisciplinaryRota(), "Value", "Text");
            }
            catch (Exception)
            {
                throw;
            }
            return View(objOnCall);
        }

        public ActionResult GetForm(string Date = "", string Department = "", string Sites = "", int MultiId = 0)
        {
            OnCallModel objOnCall = new OnCallModel();
            try
            {
                if (MultiId > 0)
                {
                    objOnCall = db.OnCall.GetOnCallActualMultiRotaList(Date, Department, Sites, MultiId,User.UserId);
                }
                objOnCall.OnCallFullDates = CommonFunctions.Datetimesetting().ToString(AppConstants.DateTimeShowFormate);
            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_Form", objOnCall);
        }
    }


}