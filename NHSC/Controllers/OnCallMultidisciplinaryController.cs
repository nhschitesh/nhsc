﻿using NHSC.Common;
using NHSC.Models.Common;
using NHSC.Models.Entities;
using NHSC.Repository;
using NHSC.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NHSC.Controllers
{
    [CustomErrorHandling]
    //[CheckSessionOut(Values = (int)Menu.Dashboard)]
    public class OnCallMultidisciplinaryController : BaseController
    {

        #region Private Members
        private IUnitOfWork db;
        CommonFunctions objCommon;
        #endregion

        public OnCallMultidisciplinaryController()
        {
            db = new UnitOfWork();
            objCommon = new CommonFunctions();
        }
        public ActionResult Index()
        {
            OnCallMultidisciplinaryModel  objOnCall = new OnCallMultidisciplinaryModel();
            try
            {
                //ViewBag.SiteList = new SelectList(db.Sites.GetSites(), "Value", "Text");
                //ViewBag.DepartmentList = new SelectList(db.Departments.GetDepartments(), "Value", "Text");
                objOnCall.OnCallDates = CommonFunctions.Datetimesetting().ToString(AppConstants.DateShowFormate);
                objOnCall = db.OnCall.GetOnCallMultidisciplinary(objOnCall.OnCallDates, "", "");
                //objOnCall.OnCallDate = CommonFunctions.Datetimesetting();
                //objOnCall.OnCallDates = CommonFunctions.Datetimesetting().ToString(AppConstants.DateShowFormate);
                //objOnCall.OnCallFullDates = CommonFunctions.Datetimesetting().ToString(AppConstants.DateTimeShowFormate);
            }
            catch (Exception)
            {
                throw;
            }
            return View(objOnCall);
        }

        public ActionResult GetForm(string Date = "", string Department = "", string Sites = "")
        {
            OnCallMultidisciplinaryModel objOnCall = new OnCallMultidisciplinaryModel();
            try
            {
                objOnCall = db.OnCall.GetOnCallMultidisciplinary(Date, Department, Sites);
                //objOnCall.OnCallFullDates = CommonFunctions.Datetimesetting().ToString(AppConstants.DateTimeShowFormate);
            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_Form", objOnCall);
        }
    }


}