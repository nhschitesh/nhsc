﻿using NHSC.Common;
using NHSC.Models.Common;
using NHSC.Models.Entities;
using NHSC.Repository;
using NHSC.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NHSC.Controllers
{
    [CustomErrorHandling]
    //[CheckSessionOut(Values = (int)Menu.Dashboard)]
    public class DashboardController : BaseController
    {

        #region Private Members
        private IUnitOfWork db;
        CommonFunctions objCommon;
        #endregion

        public DashboardController()
        {
            db = new UnitOfWork();
            objCommon = new CommonFunctions();
        }
        public ActionResult Index()
        {
            OnCallModel objOnCall = new OnCallModel();
            try
            {
                ViewBag.SiteList = new SelectList(db.Sites.GetSites(), "Value", "Text");
                ViewBag.DepartmentList = new SelectList(db.Departments.GetDepartments(), "Value", "Text");
                objOnCall.OnCallDates = CommonFunctions.Datetimesetting().ToString(AppConstants.DateShowFormate);
                if (objCommon.LoginUserDetails != null)
                {
                    SetPermision();
                    objOnCall = db.OnCall.GetOnCallActualRotaList(objOnCall.OnCallDates, "", "", User.UserId);
                }
                else
                {
                    objOnCall = db.OnCall.GetOnCallActualRotaList(objOnCall.OnCallDates, "", "", 0);
                }

                objOnCall.OnCallDate = CommonFunctions.Datetimesetting();
                objOnCall.OnCallDates = CommonFunctions.Datetimesetting().ToString(AppConstants.DateShowFormate);
                objOnCall.OnCallFullDates = CommonFunctions.Datetimesetting().ToString(AppConstants.DateTimeShowFormate);
            }
            catch (Exception)
            {
                throw;
            }
            return View(objOnCall);
        }

        public ActionResult GetForm(string Date = "", string Department = "", string Sites = "")
        {
            OnCallModel objOnCall = new OnCallModel();
            try
            {
                if (objCommon.LoginUserDetails != null)
                {
                    objOnCall = db.OnCall.GetOnCallActualRotaList(Date, Department, Sites,User.UserId);
                }
                else
                {
                    objOnCall = db.OnCall.GetOnCallActualRotaList(Date, Department, Sites,0);
                }
                objOnCall.OnCallFullDates = CommonFunctions.Datetimesetting().ToString(AppConstants.DateTimeShowFormate);
            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_Form", objOnCall);
        }

        public void SetPermision()
        {
            try
            {
                Session[AppConstants.EditRecord] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.Dashboard).FirstOrDefault().Edit;
                Session[AppConstants.DeleteRecord] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.Dashboard).FirstOrDefault().Delete;
                Session[AppConstants.AddRecord] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.Dashboard).FirstOrDefault().Add;
                Session[AppConstants.IsShowAll] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.Dashboard).FirstOrDefault().IsShowAll;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }


}