﻿using NHSC.Models.Entities;
using NHSC.Models.Common;
using NHSC.Repository;
using NHSC.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NHSC.Security;
using NHSC.Common;

namespace NHSC.Controllers
{
    [CheckSessionOut(Values = (int)Menu.ErrorLog)]
    [CustomErrorHandling]
    public class ErrorLogController : BaseController
    {

        #region Private Members
        private IUnitOfWork db;
        CommonFunctions objCommon;
        #endregion

        public ErrorLogController()
        {
            db = new UnitOfWork();
            objCommon = new CommonFunctions();
        }
        #region Crud Opration
        public ActionResult Index()
        {
            ErrorLogModel objErrorLog = new ErrorLogModel();
            try
            {
            }
            catch (Exception)
            {
                throw;
            }
            return View(objErrorLog);
        }
        #endregion

        #region Get Method

        public ActionResult GetListJson()
        {
            List<ErrorLogModel> objErrorLogList = new List<ErrorLogModel>();
            CommonModel objPaging = new CommonModel();
            try
            {
                BindPagingData(objPaging);

                objErrorLogList = db.Common.GetErrorLogList(objPaging);

                return Json(new { draw = objPaging.Page, recordsFiltered = objPaging.TotalCount, recordsTotal = objPaging.TotalCount, data = objErrorLogList });
            }
            catch (Exception)
            {
                throw;
            }
        }
        public ActionResult GetView(long Id = 0)
        {
            ErrorLogModel objErrorLog = new ErrorLogModel();
            try
            {
                objErrorLog = db.Common.GetErrorLogDetails(Id);
            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_View", objErrorLog);
        }
        public ActionResult GetList()
        {
            ErrorLogModel objErrorLogModel = new ErrorLogModel();
            try
            {

            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_List", objErrorLogModel);
        }
        #endregion

        #region Other Function

        public void BindPagingData(CommonModel objPaging)
        {
            try
            {
                objPaging.Page = CommonFunctions.ValidIntValue(Request.Form.GetValues("draw")[0]);
                string order = Request.Form.GetValues("order[0][column]")[0];
                objPaging.SortColumnDirection = Request.Form.GetValues("order[0][dir]")[0];
                objPaging.Skip = CommonFunctions.ValidIntValue(Request.Form.GetValues("start")[0]) + 1;
                objPaging.PageSize = CommonFunctions.ValidIntValue(Request.Form.GetValues("start")[0]) + CommonFunctions.ValidIntValue(Request.Form.GetValues("length")[0]);
                objPaging.ShortColumnName = Request.Form.GetValues("columns[" + order + "][name]")[0];
                if (objPaging.ShortColumnName == "CreatedDate")
                {
                    objPaging.ShortColumnName = "el." + objPaging.ShortColumnName + " " + objPaging.SortColumnDirection;
                }
                else
                {
                    objPaging.ShortColumnName = objPaging.ShortColumnName + " " + objPaging.SortColumnDirection;
                }

                objPaging.searchValue = Request.Form.GetValues("search[value]")[0];
                if (!string.IsNullOrEmpty(objPaging.searchValue))
                {
                    objPaging.searchValue = CommonFunctions.CreateFilterQuery("ErrorMsg,u.Email,u.FName", objPaging.searchValue, "el.CreatedDate", "");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        private JsonResult GetMessage(int intStatuscode, string strMessage, string pstrID = "")
        {
            dynamic showMessageString = string.Empty;
            try
            {
                showMessageString = new
                {
                    statusCode = intStatuscode,
                    msg = strMessage,
                    id = pstrID,
                    Error = strMessage,
                    message = strMessage,
                };
                return Json(showMessageString, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}