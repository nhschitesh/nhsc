﻿using NHSC.Models.Entities;
using NHSC.Models.Common;
using NHSC.Repository;
using NHSC.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NHSC.Security;
using NHSC.Common;

namespace NHSC.Controllers
{
    [CheckSessionOut(Values = (int)Menu.Staff)]
    [CustomErrorHandling]
    public class StaffController : BaseController
    {

        #region Private Members
        private IUnitOfWork db;
        CommonFunctions objCommon;
        #endregion

        public StaffController()
        {
            db = new UnitOfWork();
            objCommon = new CommonFunctions();
        }
        #region Crud Opration
        public ActionResult Index()
        {
            StaffModel objStaff = new StaffModel();
            try
            {
                SetPermision();
            }
            catch (Exception)
            {
                throw;
            }
            return View(objStaff);
        }

        [HttpPost]
        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public ActionResult Index(StaffModel objStaff)
        {
            string strErrorMsg = string.Empty;
            int intStatuscode = 1;
            bool _status = false;
            try
            {
                objStaff.ModifyBy = User.UserId;
                objStaff.ModifyDate = CommonFunctions.Datetimesetting();
                if (!IsValidData(objStaff, ref strErrorMsg))
                {
                    return GetMessage(0, strErrorMsg, objStaff.StaffId.ToString());
                }
                _status = db.Staff.InsertData(objStaff, ref strErrorMsg);
                if (objStaff.StaffId == 0)
                {
                    strErrorMsg = Resources.Resource.RecordSaveSuccessfully;
                }
                else
                {
                    strErrorMsg = Resources.Resource.RecordUpdateSuccessfully;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return GetMessage(intStatuscode, strErrorMsg, objStaff.StaffId.ToString());
        }

        [HttpPost]
        public ActionResult DeleteRecord(long Id)
        {
            string strErrorMsg = string.Empty;
            int intStatuscode = 1;
            try
            {
                if (!db.Common.IsAssignToAnyModule(AppConstants.Staff, Id, ref strErrorMsg))
                {
                    bool Status = db.Staff.DeleteData(Id, User.UserId, CommonFunctions.Datetimesetting());
                    strErrorMsg = Resources.Resource.RecordDeletedSuccessfully;
                }
                else
                {
                    intStatuscode = 0;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return GetMessage(intStatuscode, strErrorMsg, Id.ToString());
        }

        [HttpPost]
        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public ActionResult StaffOrder(StaffModel objStaff)
        {
            string strErrorMsg = string.Empty;
            int intStatuscode = 1;
            bool _status = false;
            try
            {
                objStaff.ModifyBy = User.UserId;
                objStaff.ModifyDate = CommonFunctions.Datetimesetting();

                _status = db.Staff.InsertStaffOrderData(objStaff);
                strErrorMsg = Resources.Resource.RecordSaveSuccessfully;
            }
            catch (Exception)
            {
                throw;
            }
            return GetMessage(intStatuscode, strErrorMsg, objStaff.StaffId.ToString());
        }

        [HttpPost]
        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public ActionResult AssignBulkDepSkill(StaffModel objStaff)
        {
            string strErrorMsg = string.Empty;
            int intStatuscode = 1;
            bool _status = false;
            try
            {
                objStaff.ModifyBy = User.UserId;
                objStaff.ModifyDate = CommonFunctions.Datetimesetting();
                _status = db.Staff.AssignBukDepSkillData(objStaff, ref strErrorMsg);
                if (objStaff.StaffId == 0)
                {
                    strErrorMsg = Resources.Resource.RecordSaveSuccessfully;
                }
                else
                {
                    strErrorMsg = Resources.Resource.RecordUpdateSuccessfully;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return GetMessage(intStatuscode, strErrorMsg, objStaff.StaffId.ToString());
        }
        #endregion

        #region Get Method

        public ActionResult GetListJson()
        {
            List<StaffModel> objStaffList = new List<StaffModel>();
            CommonModel objPaging = new CommonModel();
            try
            {
                BindPagingData(objPaging);

                objStaffList = db.Staff.GetList(objPaging);

                return Json(new { draw = objPaging.Page, recordsFiltered = objPaging.TotalCount, recordsTotal = objPaging.TotalCount, data = objStaffList });
            }
            catch (Exception)
            {
                throw;
            }
        }
        public ActionResult GetForm(long Id = 0)
        {
            StaffModel objStaff = new StaffModel();
            try
            {
                ViewBag.TitleList = new SelectList(db.Title.GetTitle(), "Value", "Text");
                ViewBag.SkillSetList = new SelectList(db.SkillSet.GetSkillSet(), "Value", "Text");
                ViewBag.DepartmentList = new SelectList(db.Departments.GetDepartments(), "Value", "Text");

                objStaff = db.Staff.GetDetails(Id);
            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_Form", objStaff);
        }
        public ActionResult GetView(long Id = 0)
        {
            StaffModel objStaff = new StaffModel();
            try
            {
                ViewBag.TitleList = new SelectList(db.Title.GetTitle(), "Value", "Text");
                ViewBag.SkillSetList = new SelectList(db.SkillSet.GetSkillSet(), "Value", "Text");
                ViewBag.DepartmentList = new SelectList(db.Departments.GetDepartments(), "Value", "Text");


                objStaff = db.Staff.GetDetails(Id);
            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_View", objStaff);
        }
        public ActionResult GetList()
        {
            StaffModel objStaffModel = new StaffModel();
            try
            {

            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_List", objStaffModel);
        }
        public ActionResult GetOrderForm()
        {
            StaffModel objStaff = new StaffModel();
            try
            {
                ViewBag.StaffList = new SelectList(db.Staff.GetStaff(User.UserId), "Value", "Text");
            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_OrderForm", objStaff);
        }
        public ActionResult GetOrderData(long Id)
        {
            StaffModel objStaff = new StaffModel();
            try
            {
                objStaff = db.Staff.GetStaffOrder(Id);
            }
            catch (Exception)
            {
                throw;
            }
            return Json(objStaff, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAssignForm()
        {
            StaffModel objStaff = new StaffModel();
            try
            {
                ViewBag.SkillSetList = new SelectList(db.SkillSet.GetSkillSet(), "Value", "Text");
                ViewBag.DepartmentList = new SelectList(db.Departments.GetDepartments(), "Value", "Text");
            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_AssignForm", objStaff);
        }


        #endregion

        #region Other Function

        public void SetPermision()
        {
            try
            {
                Session[AppConstants.EditRecord] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.Staff).FirstOrDefault().Edit;
                Session[AppConstants.DeleteRecord] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.Staff).FirstOrDefault().Delete;
                Session[AppConstants.AddRecord] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.Staff).FirstOrDefault().Add;
                Session[AppConstants.IsShowAll] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.Staff).FirstOrDefault().IsShowAll;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void BindPagingData(CommonModel objPaging)
        {
            try
            {
                objPaging.Page = CommonFunctions.ValidIntValue(Request.Form.GetValues("draw")[0]);
                string order = Request.Form.GetValues("order[0][column]")[0];
                objPaging.SortColumnDirection = Request.Form.GetValues("order[0][dir]")[0];
                objPaging.Skip = CommonFunctions.ValidIntValue(Request.Form.GetValues("start")[0]) + 1;
                objPaging.PageSize = CommonFunctions.ValidIntValue(Request.Form.GetValues("start")[0]) + CommonFunctions.ValidIntValue(Request.Form.GetValues("length")[0]);
                objPaging.ShortColumnName = Request.Form.GetValues("columns[" + order + "][name]")[0];
                objPaging.ShortColumnName = objPaging.ShortColumnName + " " + objPaging.SortColumnDirection;
                objPaging.searchValue = Request.Form.GetValues("search[value]")[0];
                if (!string.IsNullOrEmpty(objPaging.searchValue))
                {
                    objPaging.searchValue = CommonFunctions.CreateFilterQuery("SkillSetName,DepartmentName,TitleName,Surname,Forename", objPaging.searchValue, "CreatedDate", "");
                }
                objPaging.Id = objCommon.LoginUserDetails.UserId;
            }
            catch (Exception)
            {

                throw;
            }
        }
        private JsonResult GetMessage(int intStatuscode, string strMessage, string pstrID = "")
        {
            dynamic showMessageString = string.Empty;
            try
            {
                showMessageString = new
                {
                    statusCode = intStatuscode,
                    msg = strMessage,
                    id = pstrID,
                    Error = strMessage,
                    message = strMessage,
                };
                return Json(showMessageString, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                throw;
            }
        }
        private bool IsValidData(StaffModel objStaff, ref string strErrorMsg)
        {
            try
            {
                if (string.IsNullOrEmpty(objStaff.Surname))
                {
                    strErrorMsg = "Enter surname";
                    return false;
                }
                else if (string.IsNullOrEmpty(objStaff.Forename))
                {
                    strErrorMsg = "Enter valid surname";
                    return false;
                }
                else if (objStaff.TitleId == 0)
                {
                    strErrorMsg = "Select atleast one title";
                    return false;
                }
                else if (string.IsNullOrEmpty(objStaff.SkillSetIds))
                {
                    strErrorMsg = "Select atleast one skillset";
                    return false;
                }
                else if (string.IsNullOrEmpty(objStaff.DepartmentIds))
                {
                    strErrorMsg = "Select atleast one department";
                    return false;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return true;
        }

        #endregion
    }
}