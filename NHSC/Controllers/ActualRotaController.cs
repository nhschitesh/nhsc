﻿using NHSC.Models.Entities;
using NHSC.Models.Common;
using NHSC.Repository;
using NHSC.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NHSC.Security;
using NHSC.Common;

namespace NHSC.Controllers
{
    [CheckSessionOut(Values = (int)Menu.ActualRota)]
    [CustomErrorHandling]
    public class ActualRotaController : BaseController
    {

        #region Private Members
        private IUnitOfWork db;
        CommonFunctions objCommon;
        #endregion

        public ActualRotaController()
        {
            db = new UnitOfWork();
            objCommon = new CommonFunctions();
        }

        #region Crud Opration
        public ActionResult Index()
        {
            ActualRotaModel objActualRotaModel = new ActualRotaModel();
            try
            {
                ViewBag.RotaTemplateList = new SelectList(db.RotaTemplate.GetRotaTemplate(""), "RotaTemplateId", "RotaTemplateName");
                SetPermision();
            }
            catch (Exception)
            {
                throw;
            }
            return View(objActualRotaModel);
        }

        #endregion

        #region Get Method


        public ActionResult GetForm(long Id = 0)
        {
            ActualRotaModel objActualRotaModel = new ActualRotaModel();
            try
            {
                if (Id > 0)
                    objActualRotaModel = db.ActualRota.GetActualRotaList(Id,User.UserId);
            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_Form", objActualRotaModel);
        }
        public ActionResult GetView(long Id = 0)
        {
            ActualRotaModel objActualRotaModel = new ActualRotaModel();
            try
            {
                objActualRotaModel = db.ActualRota.GetActualRotaList(Id,User.UserId);
            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_View", objActualRotaModel);
        }

        public ActionResult LoadStaffPopup(long tempid = 0, long assignid = 0, long staffid = 0)
        {
            StaffPopUp objStaffPopUp = new StaffPopUp();
            try
            {

                ViewBag.StaffList = new SelectList(db.Staff.GetStaff(User.UserId), "Value", "Text");
                objStaffPopUp = db.ActualRota.GetActualRota(assignid);

                ViewBag.Note = "You can select only time between " + objStaffPopUp.ActualStartTime + " To " + (objStaffPopUp.ActualEndTime == "23:59" ? "24:00" : objStaffPopUp.ActualEndTime);
                ViewBag.Note1 = "Atleast " + AppConstants.MinutedifferenceTime + " minute difference required";
            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_StaffPopUp", objStaffPopUp);
        }

        public ActionResult LoadStaffPopupMulti(string[] assignid)
        {
            StaffPopUp objStaffPopUp = new StaffPopUp();
            try
            {

                ViewBag.StaffList = new SelectList(db.Staff.GetStaff(User.UserId), "Value", "Text");
                objStaffPopUp = db.ActualRota.GetActualRota(CommonFunctions.ValidlongValue(assignid[0]));
                objStaffPopUp.ActualRotaIds = string.Join(",", assignid);
                ViewBag.Note = "You can select only time between " + objStaffPopUp.ActualStartTime + " To " + (objStaffPopUp.ActualEndTime == "23:59" ? "24:00" : objStaffPopUp.ActualEndTime);
                ViewBag.Note1 = "Atleast " + AppConstants.MinutedifferenceTime + " minute difference required";
            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_StaffPopUpMulti", objStaffPopUp);
        }


        public ActionResult ChangeStaff(StaffPopUp objStaffPopUp)
        {
            string strMessage = "";
            try
            {
                if (db.ActualRota.CheckActualMainSatffExits(objStaffPopUp))
                {
                    objStaffPopUp.msg = "Staff already assgin in another shift with same time";
                    objStaffPopUp.statusCode = 0;
                    return Json(objStaffPopUp, JsonRequestBehavior.AllowGet);
                }

                if (objStaffPopUp.IsSplit)
                {
                    if (!ValidateData(objStaffPopUp, ref strMessage))
                    {
                        objStaffPopUp.statusCode = 0;
                        objStaffPopUp.msg = strMessage;
                        return Json(objStaffPopUp, JsonRequestBehavior.AllowGet);
                    }

                }
                db.ActualRota.AssignNewStaff(objStaffPopUp, User.UserId, CommonFunctions.Datetimesetting());
                objStaffPopUp.statusCode = 1;
                objStaffPopUp.msg = "Successfully updated actual rota";

            }
            catch (Exception)
            {
                throw;
            }
            return Json(objStaffPopUp, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ChangeStaffMulti(StaffPopUp objStaffPopUp)
        {
            string strMessage = "";
            try
            {
                string[] arryids = objStaffPopUp.ActualRotaIds.Split(',');

                foreach (var item in arryids)
                {
                    objStaffPopUp.ActualRotaId = CommonFunctions.ValidlongValue(item);
                    if (db.ActualRota.CheckActualMainSatffExits(objStaffPopUp))
                    {
                        objStaffPopUp.msg = "Staff already assgin in another shift with same time";
                        objStaffPopUp.statusCode = 0;
                        return Json(objStaffPopUp, JsonRequestBehavior.AllowGet);
                    }

                    if (objStaffPopUp.IsSplit)
                    {
                        if (!ValidateData(objStaffPopUp, ref strMessage))
                        {
                            objStaffPopUp.statusCode = 0;
                            objStaffPopUp.msg = strMessage;
                            return Json(objStaffPopUp, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                foreach (var item in arryids)
                {
                    objStaffPopUp.ActualRotaId = CommonFunctions.ValidlongValue(item);
                    db.ActualRota.AssignNewStaff(objStaffPopUp, User.UserId, CommonFunctions.Datetimesetting());
                }
                objStaffPopUp.statusCode = 1;
                objStaffPopUp.msg = "Successfully updated actual rota";

            }
            catch (Exception)
            {
                throw;
            }
            return Json(objStaffPopUp, JsonRequestBehavior.AllowGet);
        }
        public ActionResult UnAssignStaff(long assignid = 0)
        {
            StaffPopUp objStaffPopUp = new StaffPopUp();
            try
            {

                objStaffPopUp.ActualRotaId = assignid;
                db.ActualRota.UnAssignStaff(objStaffPopUp, User.UserId, CommonFunctions.Datetimesetting());
                objStaffPopUp.statusCode = 1;
                objStaffPopUp.msg = "Successfully un assign staff";
            }
            catch (Exception)
            {
                throw;
            }
            return Json(objStaffPopUp, JsonRequestBehavior.AllowGet);
        }
        public ActionResult UnAssignStaffMulti(string assignid = "")
        {
            StaffPopUp objStaffPopUp = new StaffPopUp();
            try
            {
                objStaffPopUp.ActualRotaIds = assignid;
                string[] arryids = assignid.Split(',');
                foreach (var item in arryids)
                {
                    objStaffPopUp.ActualRotaId = CommonFunctions.ValidlongValue(item);
                    db.ActualRota.UnAssignStaff(objStaffPopUp, User.UserId, CommonFunctions.Datetimesetting());
                }
                objStaffPopUp.statusCode = 1;
                objStaffPopUp.msg = "Successfully un assign staff";
            }
            catch (Exception)
            {
                throw;
            }
            return Json(objStaffPopUp, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Other Function

        public void SetPermision()
        {
            try
            {
                Session[AppConstants.EditRecord] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.ActualRota).FirstOrDefault().Edit;
                Session[AppConstants.DeleteRecord] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.ActualRota).FirstOrDefault().Delete;
                Session[AppConstants.AddRecord] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.ActualRota).FirstOrDefault().Add;
                Session[AppConstants.IsShowAll] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.ActualRota).FirstOrDefault().IsShowAll;
            }
            catch (Exception)
            {
                throw;
            }
        }
        private JsonResult GetMessage(int intStatuscode, string strMessage, string pstrID = "")
        {
            dynamic showMessageString = string.Empty;
            try
            {
                showMessageString = new
                {
                    statusCode = intStatuscode,
                    msg = strMessage,
                    id = pstrID,
                    Error = strMessage,
                    message = strMessage,
                };
                return Json(showMessageString, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool ValidateData(StaffPopUp objStaffPopUp, ref string strMessage)
        {
            DateTime aStart;
            DateTime aEnd;
            DateTime sStart;
            DateTime sEnd;
            try
            {
                aStart = Convert.ToDateTime("01-01-0001 " + objStaffPopUp.ActualStartTime);
                aEnd = Convert.ToDateTime("01-01-0001 " + objStaffPopUp.ActualEndTime);
                sStart = Convert.ToDateTime("01-01-0001 " + objStaffPopUp.SplitStartTime);
                sEnd = Convert.ToDateTime("01-01-0001 " + objStaffPopUp.SplitEndTime);

                if (objStaffPopUp.SplitStaffId == 0)
                {
                    strMessage = "Please select split staff id";
                    return false;
                }
                else if (objStaffPopUp.SplitStaffId == objStaffPopUp.StaffId)
                {
                    strMessage = "Staff and Split staff can not be same";
                    return false;
                }
                else if (string.IsNullOrEmpty(objStaffPopUp.SplitStartTime))
                {
                    strMessage = "Please select split start time";
                    return false;
                }
                else if (string.IsNullOrEmpty(objStaffPopUp.SplitEndTime))
                {
                    strMessage = "Please select split end time";
                    return false;
                }
                //else if (objStaffPopUp.SplitStartTime != objStaffPopUp.ActualStartTime && objStaffPopUp.SplitEndTime != objStaffPopUp.ActualEndTime)
                //{
                //    strMessage = "Start or end time atleast one time should be match in split start time or split end time";
                //    return false;
                //}
                else if (objStaffPopUp.SplitStartTime == objStaffPopUp.ActualStartTime)
                {
                    //if (sEnd < sStart.AddMinutes(AppConstants.MinutedifferenceTime))
                    //{
                    //    strMessage = "Atleast " + AppConstants.MinutedifferenceTime + " difference required in split time";
                    //    return false;
                    //}
                    //else if (aEnd.AddMinutes(-AppConstants.MinutedifferenceTime) < sEnd)
                    //{
                    //    strMessage = "Atleast " + AppConstants.MinutedifferenceTime + " difference required in split time";
                    //    return false;
                    //}

                    objStaffPopUp.StartTime = objStaffPopUp.SplitEndTime;
                    objStaffPopUp.EndTime = objStaffPopUp.ActualEndTime;
                }
                else if (objStaffPopUp.SplitEndTime == objStaffPopUp.ActualEndTime)
                {
                    //if (sStart > sEnd.AddMinutes(-AppConstants.MinutedifferenceTime))
                    //{
                    //    strMessage = "Atleast " + AppConstants.MinutedifferenceTime + " difference required in split time";
                    //    return false;
                    //}
                    //else if (aStart.AddMinutes(AppConstants.MinutedifferenceTime) > sStart)
                    //{
                    //    strMessage = "Atleast " + AppConstants.MinutedifferenceTime + " difference required in split time";
                    //    return false;
                    //}
                    objStaffPopUp.StartTime = objStaffPopUp.ActualStartTime;
                    objStaffPopUp.EndTime = objStaffPopUp.SplitStartTime;
                }
                if (db.ActualRota.CheckActualSatffExits(objStaffPopUp))
                {
                    strMessage = "Staff already assgin in another shift with same time";
                    return false;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return true;
        }

        #endregion
    }
}