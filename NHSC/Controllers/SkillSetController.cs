﻿using NHSC.Models.Entities;
using NHSC.Models.Common;
using NHSC.Repository;
using NHSC.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NHSC.Security;
using NHSC.Common;

namespace NHSC.Controllers
{
    [CheckSessionOut(Values = (int)Menu.SkillSet)]
    [CustomErrorHandling]
    public class SkillSetController : BaseController
    {

        #region Private Members
        private IUnitOfWork db;
        CommonFunctions objCommon;
        #endregion

        public SkillSetController()
        {
            db = new UnitOfWork();
            objCommon = new CommonFunctions();
        }
        #region Crud Opration
        public ActionResult Index()
        {
            SkillSetModel objSkillSet = new SkillSetModel();
            try
            {
                SetPermision();
            }
            catch (Exception)
            {
                throw;
            }
            return View(objSkillSet);
        }

        [HttpPost]
        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public ActionResult Index(SkillSetModel objSkillSet)
        {
            string strErrorMsg = string.Empty;
            int intStatuscode = 1;
            bool _status = false;
            try
            {
                objSkillSet.ModifyBy = User.UserId;
                objSkillSet.ModifyDate = CommonFunctions.Datetimesetting();
                if (!IsValidData(objSkillSet, ref strErrorMsg))
                {
                    return GetMessage(0, strErrorMsg, objSkillSet.SkillSetId.ToString());
                }
                _status = db.SkillSet.InsertData(objSkillSet, ref strErrorMsg);
                if (objSkillSet.SkillSetId == 0)
                {
                    strErrorMsg = Resources.Resource.RecordSaveSuccessfully;
                }
                else
                {
                    strErrorMsg = Resources.Resource.RecordUpdateSuccessfully;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return GetMessage(intStatuscode, strErrorMsg, objSkillSet.SkillSetId.ToString());
        }

        [HttpPost]
        public ActionResult DeleteRecord(long Id)
        {
            string strErrorMsg = string.Empty;
            int intStatuscode = 1;
            try
            {
                if (!db.Common.IsAssignToAnyModule(AppConstants.SkillSet, Id, ref strErrorMsg))
                {
                    bool Status = db.SkillSet.DeleteData(Id, User.UserId, CommonFunctions.Datetimesetting());
                    strErrorMsg = Resources.Resource.RecordDeletedSuccessfully;
                }
                else
                {
                    intStatuscode = 0;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return GetMessage(intStatuscode, strErrorMsg, Id.ToString());
        }
        #endregion

        #region Get Method

        public ActionResult GetListJson()
        {
            List<SkillSetModel> objSkillSetList = new List<SkillSetModel>();
            CommonModel objPaging = new CommonModel();
            try
            {
                BindPagingData(objPaging);

                objSkillSetList = db.SkillSet.GetList(objPaging);

                return Json(new { draw = objPaging.Page, recordsFiltered = objPaging.TotalCount, recordsTotal = objPaging.TotalCount, data = objSkillSetList });
            }
            catch (Exception)
            {
                throw;
            }
        }
        public ActionResult GetForm(long Id = 0)
        {
            SkillSetModel objSkillSet = new SkillSetModel();
            try
            {
                objSkillSet = db.SkillSet.GetDetails(Id);
            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_Form", objSkillSet);
        }
        public ActionResult GetView(long Id = 0)
        {
            SkillSetModel objSkillSet = new SkillSetModel();
            try
            {
                objSkillSet = db.SkillSet.GetDetails(Id);
            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_View", objSkillSet);
        }
        public ActionResult GetList()
        {
            SkillSetModel objSkillSetModel = new SkillSetModel();
            try
            {

            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_List", objSkillSetModel);
        }
        #endregion

        #region Other Function

        public void SetPermision()
        {
            try
            {
                Session[AppConstants.EditRecord] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.SkillSet).FirstOrDefault().Edit;
                Session[AppConstants.DeleteRecord] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.SkillSet).FirstOrDefault().Delete;
                Session[AppConstants.AddRecord] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.SkillSet).FirstOrDefault().Add;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void BindPagingData(CommonModel objPaging)
        {
            try
            {
                objPaging.Page = CommonFunctions.ValidIntValue(Request.Form.GetValues("draw")[0]);
                string order = Request.Form.GetValues("order[0][column]")[0];
                objPaging.SortColumnDirection = Request.Form.GetValues("order[0][dir]")[0];
                objPaging.Skip = CommonFunctions.ValidIntValue(Request.Form.GetValues("start")[0]) + 1;
                objPaging.PageSize = CommonFunctions.ValidIntValue(Request.Form.GetValues("start")[0]) + CommonFunctions.ValidIntValue(Request.Form.GetValues("length")[0]);
                objPaging.ShortColumnName = Request.Form.GetValues("columns[" + order + "][name]")[0];
                objPaging.ShortColumnName = objPaging.ShortColumnName + " " + objPaging.SortColumnDirection;
                objPaging.searchValue = Request.Form.GetValues("search[value]")[0];
                if (!string.IsNullOrEmpty(objPaging.searchValue))
                {
                    objPaging.searchValue = CommonFunctions.CreateFilterQuery("SkillSetName", objPaging.searchValue, "CreatedDate", "");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        private JsonResult GetMessage(int intStatuscode, string strMessage, string pstrID = "")
        {
            dynamic showMessageString = string.Empty;
            try
            {
                showMessageString = new
                {
                    statusCode = intStatuscode,
                    msg = strMessage,
                    id = pstrID,
                    Error = strMessage,
                    message = strMessage,
                };
                return Json(showMessageString, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                throw;
            }
        }
        private bool IsValidData(SkillSetModel objSkillSet, ref string strErrorMsg)
        {
            try
            {
                if (string.IsNullOrEmpty(objSkillSet.SkillSetName))
                {
                    strErrorMsg = Resources.Resource.EnterSkillSet;
                    return false;
                }
                //else if (!CommonFunctions.IsValidString(objSkillSet.SkillSetName))
                //{
                //    strErrorMsg = "Please enter valid skill set name.";
                //    return false;
                //}
                else
                {
                    if (db.SkillSet.ExistName(objSkillSet))
                    {
                        strErrorMsg = Resources.Resource.SkillSetExists;
                        return false;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return true;
        }

        #endregion
    }
}