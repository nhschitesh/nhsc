﻿using NHSC.Models.Entities;
using NHSC.Models.Common;
using NHSC.Repository;
using NHSC.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NHSC.Security;
using NHSC.Common;

namespace NHSC.Controllers
{
    [CheckSessionOut(Values = (int)Menu.ActivityLog)]
    [CustomErrorHandling]
    public class ActivityLogController : BaseController
    {

        #region Private Members
        private IUnitOfWork db;
        CommonFunctions objCommon;
        #endregion

        public ActivityLogController()
        {
            db = new UnitOfWork();
            objCommon = new CommonFunctions();
        }
        #region Crud Opration
        public ActionResult Index()
        {
            ActivityLogModel objActivityLog = new ActivityLogModel();
            try
            {
            }
            catch (Exception)
            {
                throw;
            }
            return View(objActivityLog);
        }
        #endregion

        #region Get Method

        public ActionResult GetListJson()
        {
            List<ActivityLogModel> objActivityLogList = new List<ActivityLogModel>();
            CommonModel objPaging = new CommonModel();
            try
            {
                BindPagingData(objPaging);

                objActivityLogList = db.Common.GetActivityLogList(objPaging);

                return Json(new { draw = objPaging.Page, recordsFiltered = objPaging.TotalCount, recordsTotal = objPaging.TotalCount, data = objActivityLogList });
            }
            catch (Exception)
            {
                throw;
            }
        }
        public ActionResult GetView(long Id = 0)
        {
            ActivityLogModel objActivityLog = new ActivityLogModel();
            try
            {
                objActivityLog = db.Common.GetActivityLogDetails(Id);
            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_View", objActivityLog);
        }
        public ActionResult GetList()
        {
            ActivityLogModel objActivityLogModel = new ActivityLogModel();
            try
            {

            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_List", objActivityLogModel);
        }
        #endregion

        #region Other Function

        public void BindPagingData(CommonModel objPaging)
        {
            try
            {
                objPaging.Page = CommonFunctions.ValidIntValue(Request.Form.GetValues("draw")[0]);
                string order = Request.Form.GetValues("order[0][column]")[0];
                objPaging.SortColumnDirection = Request.Form.GetValues("order[0][dir]")[0];
                objPaging.Skip = CommonFunctions.ValidIntValue(Request.Form.GetValues("start")[0]) + 1;
                objPaging.PageSize = CommonFunctions.ValidIntValue(Request.Form.GetValues("start")[0]) + CommonFunctions.ValidIntValue(Request.Form.GetValues("length")[0]);
                objPaging.ShortColumnName = Request.Form.GetValues("columns[" + order + "][name]")[0];
                if (objPaging.ShortColumnName == "CreatedDate")
                {
                    objPaging.ShortColumnName = "al." + objPaging.ShortColumnName + " " + objPaging.SortColumnDirection;
                }
                else
                {
                    objPaging.ShortColumnName = objPaging.ShortColumnName + " " + objPaging.SortColumnDirection;
                }

                objPaging.searchValue = Request.Form.GetValues("search[value]")[0];
                if (!string.IsNullOrEmpty(objPaging.searchValue))
                {
                    objPaging.searchValue = CommonFunctions.CreateFilterQuery("ActivityLog,u.Email,u.FName", objPaging.searchValue, "al.CreatedDate", "", "");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        private JsonResult GetMessage(int intStatuscode, string strMessage, string pstrID = "")
        {
            dynamic showMessageString = string.Empty;
            try
            {
                showMessageString = new
                {
                    statusCode = intStatuscode,
                    msg = strMessage,
                    id = pstrID,
                    Error = strMessage,
                    message = strMessage,
                };
                return Json(showMessageString, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}