﻿using NHSC.Models.Entities;
using NHSC.Models.Common;
using NHSC.Repository;
using NHSC.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NHSC.Security;
using NHSC.Common;

namespace NHSC.Controllers
{
    [CheckSessionOut(Values = (int)Menu.TemplateRota)]
    [CustomErrorHandling]
    public class TemplateRotaController : BaseController
    {

        #region Private Members
        private IUnitOfWork db;
        CommonFunctions objCommon;
        #endregion

        public TemplateRotaController()
        {
            db = new UnitOfWork();
            objCommon = new CommonFunctions();
        }
        #region Crud Opration
        public ActionResult Index()
        {
            AssignRotaModel objAssignRotaModel = new AssignRotaModel();
            try
            {
                ViewBag.RotaTemplateList = new SelectList(db.RotaTemplate.GetRotaTemplate(""), "RotaTemplateId", "RotaTemplateName");
                SetPermision();
            }
            catch (Exception)
            {
                throw;
            }
            return View(objAssignRotaModel);
        }

        #endregion

        #region Get Method


        public ActionResult GetForm(long Id = 0)
        {
            AssignRotaModel objAssignRotaModel = new AssignRotaModel();
            try
            {
                if (Id > 0)
                    objAssignRotaModel = db.RotaTemplate.GetAssignRotaTemplateList(Id,User.UserId);
            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_Form", objAssignRotaModel);
        }
        public ActionResult GetView(long Id = 0)
        {
            AssignRotaModel objAssignRotaModel = new AssignRotaModel();
            try
            {
                objAssignRotaModel = db.RotaTemplate.GetAssignRotaTemplateList(Id,User.UserId);
            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_View", objAssignRotaModel);
        }

        public ActionResult LoadStaffPopup(long tempid = 0, long assignid = 0, long staffid = 0)
        {
            StaffPopUp objStaffPopUp = new StaffPopUp();
            try
            {
                objStaffPopUp.RotaTemplateId = tempid;
                objStaffPopUp.RotaAssignId = assignid;
                objStaffPopUp.StaffId = staffid;
                ViewBag.StaffList = new SelectList(db.Staff.GetStaff(User.UserId), "Value", "Text");
            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_StaffPopUp", objStaffPopUp);
        }

        public ActionResult LoadStaffPopupMulti(string[] assignid)
        {
            StaffPopUp objStaffPopUp = new StaffPopUp();
            try
            {

                ViewBag.StaffList = new SelectList(db.Staff.GetStaff(User.UserId), "Value", "Text");
                objStaffPopUp.RotaAssignIds = string.Join(",", assignid);
            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_StaffPopUpMulti", objStaffPopUp);
        }

        public ActionResult ChangeStaff(long assignid = 0, long staffid = 0)
        {
            StaffPopUp objStaffPopUp = new StaffPopUp();
            try
            {
                objStaffPopUp.RotaAssignId = assignid;
                objStaffPopUp.StaffId = staffid;
                db.RotaTemplate.AssignNewStaff(assignid, staffid,User.UserId,CommonFunctions.Datetimesetting());
            }
            catch (Exception)
            {
                throw;
            }
            return GetMessage(1, "Successfully updated staff");
        }

        public ActionResult ChangeStaffMulti(StaffPopUp objStaffPopUp)
        {
            try
            {
                string[] arryids = objStaffPopUp.RotaAssignIds.Split(',');

               
                foreach (var item in arryids)
                {
                    objStaffPopUp.RotaAssignId = CommonFunctions.ValidlongValue(item);
                    //db.ActualRota.AssignNewStaff(objStaffPopUp, User.UserId, CommonFunctions.Datetimesetting());
                    db.RotaTemplate.AssignNewStaff(objStaffPopUp.RotaAssignId, objStaffPopUp.StaffId, User.UserId, CommonFunctions.Datetimesetting());
                }
                objStaffPopUp.statusCode = 1;
                objStaffPopUp.msg = "Successfully updated template rota";

            }
            catch (Exception)
            {
                throw;
            }
            return Json(objStaffPopUp, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Other Function

        public void SetPermision()
        {
            try
            {
                Session[AppConstants.EditRecord] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.TemplateRota).FirstOrDefault().Edit;
                Session[AppConstants.DeleteRecord] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.TemplateRota).FirstOrDefault().Delete;
                Session[AppConstants.AddRecord] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.TemplateRota).FirstOrDefault().Add;
                Session[AppConstants.IsShowAll] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.TemplateRota).FirstOrDefault().IsShowAll;
            }
            catch (Exception)
            {
                throw;
            }
        }
        private JsonResult GetMessage(int intStatuscode, string strMessage, string pstrID = "")
        {
            dynamic showMessageString = string.Empty;
            try
            {
                showMessageString = new
                {
                    statusCode = intStatuscode,
                    msg = strMessage,
                    id = pstrID,
                    Error = strMessage,
                    message = strMessage,
                };
                return Json(showMessageString, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
    }
}