﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NHSC.Common;
using NHSC.Models.Common;
using NHSC.Models.Entities;
using NHSC.Repository;
using NHSC.Resources;
using NHSC.Security;

namespace NHSC.Controllers
{

    [CustomErrorHandling]
    public class LogInController : Controller
    {
        #region Private Members
        private IUnitOfWork db;
        CommonFunctions objCommon;
        #endregion

        public LogInController()
        {
            db = new UnitOfWork();
            objCommon = new CommonFunctions();
        }

        #region Sign in sign out

        // login get mehtod
        [AllowAnonymous]
        public ActionResult Index(string returnUrl = "")
        {
            LogInModel objLogin = new LogInModel();
            UserModel objuser = new UserModel();
            try
            {
                ViewBag.ReturnUrl = returnUrl;
                ViewBag.IsValid = true;
                var cookie = HttpContext.Request.Cookies[AppConstants.KeepMeLogIn];
                if (cookie != null)
                {
                    string Values = cookie.Value;
                    if (!string.IsNullOrEmpty(Values))
                    {
                        string[] ArryLogin = CommonFunctions.Decrypt(Values).Split(',');
                        if (ArryLogin != null && ArryLogin.Count() == 2)
                        {
                            objLogin.UserName = ArryLogin[0];
                            objLogin.Password = ArryLogin[1];
                            if (!string.IsNullOrEmpty(objLogin.UserName) && !string.IsNullOrEmpty(objLogin.Password))
                            {
                                objuser = db.User.ValidateUser(objLogin);

                                if (objuser != null && objuser.UserId > 0)
                                {
                                    CustomPrincipal serializeModel = new CustomPrincipal("");
                                    BindUserData(serializeModel, objuser);
                                    if (serializeModel.Status == 1)
                                    {
                                        Session[AppConstants.UserSession] = serializeModel;

                                        if (!string.IsNullOrEmpty(returnUrl))
                                        {
                                            return RedirectToLocal(returnUrl);
                                        }
                                        else
                                        {
                                            return RedirectToAction("Index", "Dashboard");
                                        }
                                    }
                                    else
                                    {
                                        ViewBag.ErrorSuccesssMsg = Resources.Resource.DeAcvitedUser;
                                        ViewBag.IsValid = false;
                                        return View(objLogin);
                                    }
                                }
                            }
                        }

                    }
                }
                else
                {
                    ClereSession();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return View(objLogin);
        }

        // login post mehtod
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Index(LogInModel objLogin, string returnUrl)
        {
            UserModel objuser = new UserModel();
            ActivityLogModel objActivity = new ActivityLogModel();
            try
            {
                ViewBag.ReturnUrl = returnUrl;
                if (!string.IsNullOrEmpty(objLogin.UserName) && !string.IsNullOrEmpty(objLogin.Password))
                {
                    //validate user login with email and password
                    objuser = db.User.ValidateUser(objLogin);
                    if (objuser != null && objuser.UserId > 0)
                    {
                        // cehck user is active or not
                        if (objuser.Status == 1)
                        {
                            CustomPrincipal serializeModel = new CustomPrincipal("");
                            //bind user data and menu list
                            BindUserData(serializeModel, objuser);
                            Session[AppConstants.UserSession] = serializeModel;

                            if (objLogin.KeepMeLogin)
                            {
                                var cookie = new HttpCookie(AppConstants.KeepMeLogIn);
                                cookie.Value = CommonFunctions.Encrypt(objLogin.UserName + "," + objLogin.Password);
                                cookie.Expires = DateTime.Now.AddHours(AppConstants.KeepMeSignInExpireHour);
                                Response.Cookies.Add(cookie);
                            }
                            objuser.ModifyDate = CommonFunctions.Datetimesetting();
                            // update last login date
                            db.User.UpdateLastLogIn(objuser);
                            
                            //insert activity
                            #region
                            objActivity.ActivityLog = string.Format(AppConstants.LogIn, objuser.FName);
                            objActivity.ModifyDate = CommonFunctions.Datetimesetting();
                            objActivity.UserId = objuser.UserId;
                            objActivity.ModifyBy = objuser.UserId;
                            objActivity.MenuId = 0;
                            db.Common.InsertActivityLog(objActivity);
                            #endregion

                            if (!string.IsNullOrEmpty(returnUrl))
                            {
                                return RedirectToLocal(returnUrl);
                            }
                            else
                            {
                                return RedirectToAction("Index", "Dashboard");
                            }
                        }
                        else
                        {
                            ViewBag.ErrorSuccesssMsg = Resources.Resource.DeAcvitedUser;
                            ViewBag.IsValid = false;
                            return View(objLogin);
                        }
                    }
                    else
                    {
                        ViewBag.ErrorSuccesssMsg = Resources.Resource.login_MSG_01;
                        ViewBag.IsValid = false;
                        return View(objLogin);
                    }
                }
                else
                {
                    ViewBag.ErrorSuccesssMsg = Resources.Resource.login_MSG_01;
                    ViewBag.IsValid = false;
                    return View(objLogin);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        //logout method
        public ActionResult Logout()
        {
            ActivityLogModel objActivity = new ActivityLogModel();
            try
            {
                if (objCommon.LoginUserDetails != null && objCommon.LoginUserDetails.UserId > 0)
                {
                    //insert activity
                    #region
                    objActivity.ActivityLog = string.Format(AppConstants.LogOut, objCommon.LoginUserDetails.FirstName);
                    objActivity.ModifyDate = CommonFunctions.Datetimesetting();
                    objActivity.UserId = objCommon.LoginUserDetails.UserId;
                    objActivity.ModifyBy = objCommon.LoginUserDetails.UserId;
                    objActivity.MenuId = 0;
                    db.Common.InsertActivityLog(objActivity);
                    #endregion
                }
                ClereSession();
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Forgot Pasword
        // forgot password method
        public ActionResult ForgotPassword()
        {
            ForgotPassword objForgot = new ForgotPassword();
            try
            {

            }
            catch (Exception)
            {
                throw;
            }
            return View(objForgot);

        }

        // forgot password send email
        [HttpPost]
        public ActionResult ForgotPassword(ForgotPassword objForgot)
        {
            UserModel objUser = new UserModel();
            try
            {
                if (!string.IsNullOrEmpty(objForgot.Email))
                {
                    objUser = db.User.GetDetailsByEmail(objForgot.Email);
                    if (objUser != null && objUser.UserId > 0)
                    {
                        if (objUser.Status == 1)
                        {
                            objForgot.UserId = objUser.UserId;
                            objForgot.ModifyDate = CommonFunctions.Datetimesetting();
                            objForgot.ForgotId = db.User.InsertForgotData(objForgot);
                            objForgot.UserName = objUser.FName;
                            objCommon.SendForgotPasswordEmail(objForgot);

                            return GetMessage(1, Resources.Resource.ResetLinkSend, "");
                        }
                        else
                        {
                            return GetMessage(0, Resources.Resource.DeAcvitedUser, "");
                        }

                    }
                    else
                    {
                        return GetMessage(0, Resources.Resource.InvalidEmail, "");
                    }
                }
                else
                {
                    return GetMessage(0, Resources.Resource.EnterEmailAddress, "");
                }

            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion


        #region Change Password

        // change forgot password get
        public ActionResult ChangePassword(string Token)
        {
            ChangePassword objChangePwd = new ChangePassword();
            ForgotPassword objForgot = new ForgotPassword();
            try
            {
                if (!string.IsNullOrEmpty(Token))
                {
                    objChangePwd.ForgotId = Convert.ToInt64(CommonFunctions.Decrypt(Token));
                    objForgot = db.User.GetForgotDetails(objChangePwd.ForgotId);
                    if (objForgot != null && objForgot.ForgotId > 0)
                    {
                        if (!objForgot.Status)
                        {
                            objChangePwd.UserId = objForgot.UserId;
                            objChangePwd.Email = objForgot.Email;
                            return View(objChangePwd);
                        }
                        else
                        {
                            return RedirectToAction("Index", "LogIn");
                        }
                    }
                    else
                    {
                        return RedirectToAction("Index", "LogIn");
                    }
                }
                else
                {
                    return RedirectToAction("Index", "LogIn");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        // change forgot password save
        [HttpPost]
        public ActionResult ChangePassword(ChangePassword objChangePwd)
        {
            string strErrorMsg = string.Empty;
            try
            {
                if (!IsValidDataChangePasword(objChangePwd, ref strErrorMsg))
                {
                    return GetMessage(0, strErrorMsg, "");
                }
                objChangePwd.ModifyDate = CommonFunctions.Datetimesetting();

                db.User.UpdatePassword(objChangePwd);
            }
            catch (Exception)
            {
                throw;
            }
            return GetMessage(1, Resources.Resource.UpdatePasswordSuccessfully, "");
        }
        #endregion

        #region Other Function
        //redirect method
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(HttpUtility.UrlDecode(returnUrl)))
            {
                return Redirect(HttpUtility.UrlDecode(returnUrl));
            }
            return RedirectToAction("Index", "Dashboard");
        }

        //cleare session and cookies method
        public void ClereSession()
        {
            try
            {
                Session[AppConstants.UserSession] = null;
                Session.Clear();
                Session.Abandon();

                string[] myCookies = Request.Cookies.AllKeys;
                foreach (string cookie in myCookies)
                {
                    Response.Cookies[cookie].Expires = DateTime.Now.AddDays(-1);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        // cehck login method
        public ActionResult CheckLogin()
        {
            bool Islogin = true;
            if (Session[AppConstants.UserSession] == null)
            {
                Islogin = false;
            }

            return Json(Islogin, JsonRequestBehavior.AllowGet);
        }

        //json message method
        private JsonResult GetMessage(int intStatuscode, string strMessage, string pstrID = "")
        {
            dynamic showMessageString = string.Empty;
            try
            {
                showMessageString = new
                {
                    statusCode = intStatuscode,
                    msg = strMessage,
                    id = pstrID,
                    Error = strMessage,
                    message = strMessage,
                };
                return Json(showMessageString, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //bind user data and menu list
        public void BindUserData(CustomPrincipal objUserBO, UserModel objUser)
        {
            try
            {
                objUserBO.UserId = objUser.UserId;
                objUserBO.Email = objUser.Email;
                objUserBO.FirstName = objUser.FName;
                objUserBO.LastName = objUser.LName;
                objUserBO.FullName = objUserBO.FirstName + " " + objUser.LName;
                objUserBO.GroupName = objUser.UserGroupName;
                objUserBO.Password = objUser.Password;
                objUserBO.UserGroupId = objUser.UserGroupId;
                objUserBO.Status = objUser.Status;
                objUserBO.MenuList = BindMenuList(objUser.UserGroupId);
            }
            catch (Exception)
            {
                throw;
            }
        }

        // bind menu list
        public List<MenuList> BindMenuList(long UserGroupId)
        {
            DataTable dt;
            List<MenuList> MenuList = new List<MenuList>();
            try
            {
                dt = db.UserGroup.GetRights(UserGroupId);
                if (CommonFunctions.IsValidDataTable(dt))
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        MenuList objRights = new MenuList();
                        objRights.MenuId = CommonFunctions.ValidIntValue(dt.Rows[i]["MenuId"]?.ToString() ?? "0");
                        objRights.ParentId = CommonFunctions.ValidIntValue(dt.Rows[i]["ParentId"]?.ToString() ?? "0");
                        objRights.Sequence = CommonFunctions.ValidIntValue(dt.Rows[i]["Sequence"]?.ToString() ?? "0");
                        objRights.RightId = CommonFunctions.ValidlongValue(dt.Rows[i]["RightId"]?.ToString() ?? "0");
                        objRights.Add = CommonFunctions.ValidBoolValue(dt.Rows[i]["Add"]?.ToString() ?? "0");
                        objRights.Edit = CommonFunctions.ValidBoolValue(dt.Rows[i]["Edit"]?.ToString() ?? "0");
                        objRights.Delete = CommonFunctions.ValidBoolValue(dt.Rows[i]["Delete"]?.ToString() ?? "0");
                        objRights.Print = CommonFunctions.ValidBoolValue(dt.Rows[i]["Print"]?.ToString() ?? "0");
                        objRights.IsShowAll = CommonFunctions.ValidBoolValue(dt.Rows[i]["IsShowAll"]?.ToString() ?? "0");
                        objRights.IsShowInMenu = CommonFunctions.ValidBoolValue(dt.Rows[i]["IsShowInMenu"]?.ToString() ?? "0");
                        objRights.View = CommonFunctions.ValidBoolValue(dt.Rows[i]["View"]?.ToString() ?? "0");
                        objRights.Action = dt.Rows[i]["Action"]?.ToString() ?? "";
                        objRights.MenuName = dt.Rows[i]["MenuName"]?.ToString() ?? "";
                        objRights.Controller = dt.Rows[i]["Controller"]?.ToString() ?? "";
                        MenuList.Add(objRights);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return MenuList;
        }

        //validate forgot password model
        private bool IsValidDataChangePasword(ChangePassword objPassword, ref string strErrorMsg)
        {
            try
            {
                if (string.IsNullOrEmpty(objPassword.Password))
                {
                    strErrorMsg = Resources.Resource.EnterNewpaaword;
                    return false;
                }
                else if (string.IsNullOrEmpty(objPassword.ConfirmPassword))
                {
                    strErrorMsg = Resources.Resource.EnterConfirmPassword;
                    return false;
                }
                else if (objPassword.ConfirmPassword != objPassword.Password)
                {
                    strErrorMsg = Resources.Resource.PasswordNotMatch;
                    return false;
                }
                //else if (objPassword.Password.Length < CommonFunctions.MaxPasswordLenght)
                //{
                //    strErrorMsg = Resource.Resource.PassmustbegreaterequalMsg + GoalBase.MaxPasswordLenght + Resource.Resource.characterMsg;
                //    return false;
                //}
            }
            catch (Exception)
            {
                throw;
            }
            return true;
        }
        #endregion
    }
}