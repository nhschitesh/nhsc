﻿using NHSC.Models.Entities;
using NHSC.Models.Common;
using NHSC.Repository;
using NHSC.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NHSC.Security;
using NHSC.Common;

namespace NHSC.Controllers
{

    [CustomErrorHandling]
    public class UserController : BaseController
    {

        #region Private Members
        private IUnitOfWork db;
        CommonFunctions objCommon;
        #endregion

        public UserController()
        {
            db = new UnitOfWork();
            objCommon = new CommonFunctions();
        }

        #region Crud Opration
        [CheckSessionOut(Values = (int)Menu.User)]
        public ActionResult Index()
        {
            UserModel objUser = new UserModel();
            try
            {
                SetPermision();
            }
            catch (Exception)
            {
                throw;
            }
            return View(objUser);
        }

        [HttpPost]
        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        [CheckSessionOut(Values = (int)Menu.User)]
        public ActionResult Index(UserModel objUser)
        {
            string strErrorMsg = string.Empty;
            int intStatuscode = 1;
            bool _status = false;
            try
            {
                objUser.ModifyBy = User.UserId;
                objUser.ModifyDate = CommonFunctions.Datetimesetting();
                if (!IsValidData(objUser, ref strErrorMsg))
                {
                    return GetMessage(0, strErrorMsg, objUser.UserId.ToString());
                }
                _status = db.User.InsertData(objUser, ref strErrorMsg);
                if (objUser.UserId == 0)
                {
                    strErrorMsg = Resources.Resource.RecordSaveSuccessfully;
                }
                else
                {
                    strErrorMsg = Resources.Resource.RecordUpdateSuccessfully;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return GetMessage(intStatuscode, strErrorMsg, objUser.UserId.ToString());
        }

        [HttpPost]
        [CheckSessionOut(Values = (int)Menu.User)]
        public ActionResult DeleteRecord(long Id)
        {
            string strErrorMsg = string.Empty;
            int intStatuscode = 1;
            try
            {
                if (!db.Common.IsAssignToAnyModule(AppConstants.User, Id, ref strErrorMsg))
                {
                    bool Status = db.User.DeleteData(Id, User.UserId, CommonFunctions.Datetimesetting());
                    strErrorMsg = Resources.Resource.RecordDeletedSuccessfully;
                }
                else
                {
                    intStatuscode = 0;
                }
             
            }
            catch (Exception)
            {
                throw;
            }
            return GetMessage(intStatuscode, strErrorMsg, Id.ToString());
        }
        #endregion

        #region Get Method
        [CheckSessionOut(Values = (int)Menu.User)]
        public ActionResult GetListJson()
        {
            List<UserModel> objUserList = new List<UserModel>();
            CommonModel objPaging = new CommonModel();
            try
            {
                BindPagingData(objPaging);

                objUserList = db.User.GetList(objPaging);

                return Json(new { draw = objPaging.Page, recordsFiltered = objPaging.TotalCount, recordsTotal = objPaging.TotalCount, data = objUserList });
            }
            catch (Exception)
            {
                throw;
            }
        }
        [CheckSessionOut(Values = (int)Menu.User)]
        public ActionResult GetForm(long Id = 0)
        {
            UserModel objUser = new UserModel();
            try
            {
                ViewBag.GroupList = new SelectList(db.UserGroup.GetUserGroup(), "Value", "Text");
                objUser = db.User.GetDetails(Id);
                if (Id == 0)
                {
                    objUser.Status = 1;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_Form", objUser);
        }
        [CheckSessionOut(Values = (int)Menu.User)]
        public ActionResult GetView(long Id = 0)
        {
            UserModel objUser = new UserModel();
            try
            {
                ViewBag.GroupList = new SelectList(db.UserGroup.GetUserGroup(), "Value", "Text");
                objUser = db.User.GetDetails(Id);
                if (Id == 0)
                {
                    objUser.Status = 1;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_View", objUser);
        }
        [CheckSessionOut(Values = (int)Menu.User)]
        public ActionResult GetList()
        {
            UserModel objUserModel = new UserModel();
            try
            {

            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_List", objUserModel);
        }

        #endregion

        #region Asssign department 
        [CheckSessionOut(Values = (int)Menu.User)]
        public ActionResult GetDepartmentAssign(long Id = 0)
        {
            UserModel objUser = new UserModel();
            try
            {
                ViewBag.UserList = new SelectList(db.User.UserDepartmentList(), "Value", "Text");
                objUser.UserId = Id;

            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_AssignDepartment", objUser);
        }
        [CheckSessionOut(Values = (int)Menu.User)]
        public ActionResult GetDepartmentAssignList(long Id = 0)
        {
            List<AssignDepartmentToUserModel> objDepartList = new List<AssignDepartmentToUserModel>();
            CommonModel objPaging = new CommonModel();
            try
            {
                BindPagingForDepartmentData(objPaging);
                objPaging.Id = Id;
                objDepartList = db.Departments.GetDepartmentList(objPaging);
              
                return Json(new { draw = objPaging.Page, recordsFiltered = objPaging.TotalCount, recordsTotal = objPaging.TotalCount, data = objDepartList });
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CheckSessionOut(Values = (int)Menu.User)]
        public ActionResult GetDepartmentAssignById(long Id = 0)
        {
            UserModel objUser = new UserModel();
            string DepartmentIds = "";
            try
            {
                objUser.UserId = Id;
                DepartmentIds = db.Departments.GetDepartmentIds(Id);
                ViewBag.DepartmentIds = DepartmentIds;

            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("_AssignDepartmentList", objUser);
        }

        [CheckSessionOut(Values = (int)Menu.User)]
        [HttpPost]
        public ActionResult SaveAssignDepartment(long Id = 0, string DepartmentIds = "")
        {
            UserModel objUser = new UserModel();
            try
            {
                if (!string.IsNullOrEmpty(DepartmentIds) && Id > 0)
                {
                    objUser.ModifyBy = User.UserId;
                    objUser.ModifyDate = CommonFunctions.Datetimesetting();
                    objUser.UserId = Id;
                    db.Departments.AssignDepartmentToUser(objUser, DepartmentIds);
                }
                else
                {
                    return GetMessage(0, "Select atleast one user or department to assign");
                }
                objUser.UserId = Id;
            }
            catch (Exception)
            {
                throw;
            }
            return GetMessage(1, "Successfully assign department to user");
        }
        #endregion

        #region Change Password
        // change forgot password get
        public ActionResult ChangePassword()
        {
            ChangePassword objChangePwd = new ChangePassword();
            try
            {
                if (objCommon.LoginUserDetails == null || objCommon.LoginUserDetails.UserId == 0)
                {
                    return RedirectToAction("Index", "LogIn");
                }
                objChangePwd.UserId = User.UserId;
            }
            catch (Exception)
            {
                throw;
            }
            return View(objChangePwd);
        }
        // change forgot password save
        [HttpPost]
        public ActionResult ChangePassword(ChangePassword objChangePwd)
        {
            string strErrorMsg = string.Empty;
            try
            {
                if (!IsValidDataChangePasword(objChangePwd, ref strErrorMsg))
                {
                    return GetMessage(0, strErrorMsg, "");
                }
                objChangePwd.ModifyDate = CommonFunctions.Datetimesetting();

                db.User.UpdatePassword(objChangePwd);
            }
            catch (Exception)
            {
                throw;
            }
            return GetMessage(1, Resources.Resource.UpdatePasswordSuccessfully, "");
        }
        #endregion


        #region Change Profile
        public ActionResult MyProfile()
        {
            UserModel objUser = new UserModel();
            try
            {
                if (objCommon.LoginUserDetails == null || objCommon.LoginUserDetails.UserId == 0)
                {
                    return RedirectToAction("Index", "LogIn");
                }
                objUser = db.User.GetDetails(User.UserId);
                ViewBag.GroupList = new SelectList(db.UserGroup.GetUserGroup(), "Value", "Text");
            }
            catch (Exception)
            {
                throw;
            }
            return View(objUser);
        }
        [HttpPost]
        public ActionResult MyProfile(UserModel objUser)
        {
            try
            {
                objUser.ModifyDate = CommonFunctions.Datetimesetting();
                db.User.UpdateUserProfile(objUser);
            }
            catch (Exception)
            {
                throw;
            }
            return GetMessage(1, Resources.Resource.UpdateProfileSuccessfully, "");
        }
        #endregion


        #region Other Function
        public void SetPermision()
        {
            try
            {
                Session[AppConstants.EditRecord] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.User).FirstOrDefault().Edit;
                Session[AppConstants.DeleteRecord] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.User).FirstOrDefault().Delete;
                Session[AppConstants.AddRecord] = User.MenuList.Where(x => x.View == true && x.MenuId == (int)Menu.User).FirstOrDefault().Add;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void BindPagingData(CommonModel objPaging)
        {
            try
            {
                objPaging.Page = CommonFunctions.ValidIntValue(Request.Form.GetValues("draw")[0]);
                string order = Request.Form.GetValues("order[0][column]")[0];
                objPaging.SortColumnDirection = Request.Form.GetValues("order[0][dir]")[0];
                objPaging.Skip = CommonFunctions.ValidIntValue(Request.Form.GetValues("start")[0]) + 1;
                objPaging.PageSize = CommonFunctions.ValidIntValue(Request.Form.GetValues("start")[0]) + CommonFunctions.ValidIntValue(Request.Form.GetValues("length")[0]);
                objPaging.ShortColumnName = Request.Form.GetValues("columns[" + order + "][name]")[0];
                if (objPaging.ShortColumnName == "CreatedDate")
                {
                    objPaging.ShortColumnName = "u." + objPaging.ShortColumnName + " " + objPaging.SortColumnDirection;
                }
                else
                {
                    objPaging.ShortColumnName = objPaging.ShortColumnName + " " + objPaging.SortColumnDirection;
                }
                objPaging.searchValue = Request.Form.GetValues("search[value]")[0];
                if (!string.IsNullOrEmpty(objPaging.searchValue))
                {
                    objPaging.searchValue = CommonFunctions.CreateFilterQuery("UserName,FName,LName,Email,ug.UserGroupName", objPaging.searchValue, "u.CreatedDate", "");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void BindPagingForDepartmentData(CommonModel objPaging)
        {
            try
            {
                objPaging.Page = CommonFunctions.ValidIntValue(Request.Form.GetValues("draw")[0]);
                string order = Request.Form.GetValues("order[0][column]")[0];
                objPaging.SortColumnDirection = Request.Form.GetValues("order[0][dir]")[0];
                objPaging.Skip = CommonFunctions.ValidIntValue(Request.Form.GetValues("start")[0]) + 1;
                objPaging.PageSize = CommonFunctions.ValidIntValue(Request.Form.GetValues("start")[0]) + CommonFunctions.ValidIntValue(Request.Form.GetValues("length")[0]);
                objPaging.ShortColumnName = Request.Form.GetValues("columns[" + order + "][name]")[0];
                if (objPaging.ShortColumnName == "CreatedDate")
                {
                    objPaging.ShortColumnName = "d." + objPaging.ShortColumnName + " " + objPaging.SortColumnDirection;
                }
                else
                {
                    objPaging.ShortColumnName = objPaging.ShortColumnName + " " + objPaging.SortColumnDirection;
                }
                objPaging.searchValue = Request.Form.GetValues("search[value]")[0];
                if (!string.IsNullOrEmpty(objPaging.searchValue))
                {
                    objPaging.searchValue = CommonFunctions.CreateFilterQuery("DepartmentName,SiteName", objPaging.searchValue, "d.CreatedDate", "");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        private JsonResult GetMessage(int intStatuscode, string strMessage, string pstrID = "")
        {
            dynamic showMessageString = string.Empty;
            try
            {
                showMessageString = new
                {
                    statusCode = intStatuscode,
                    msg = strMessage,
                    id = pstrID,
                    Error = strMessage,
                    message = strMessage,
                };
                return Json(showMessageString, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                throw;
            }
        }
        private bool IsValidData(UserModel objUser, ref string strErrorMsg)
        {
            try
            {
                if (string.IsNullOrEmpty(objUser.FName))
                {
                    strErrorMsg = Resources.Resource.EnterFirstName;
                    return false;
                }
                else if (string.IsNullOrEmpty(objUser.Email))
                {
                    strErrorMsg = Resources.Resource.EnterEmail;
                    return false;
                }
                else if (!CommonFunctions.IsValidString(objUser.FName))
                {
                    strErrorMsg = Resources.Resource.ValidFirstName;
                    return false;
                }
                else if (!CommonFunctions.IsValidString(objUser.LName))
                {
                    strErrorMsg = Resources.Resource.ValidLastName;
                    return false;
                }
                else if (string.IsNullOrEmpty(objUser.Password))
                {
                    strErrorMsg = Resources.Resource.EnterPassword;
                    return false;
                }
                else if (objUser.UserGroupId == 0)
                {
                    strErrorMsg = Resources.Resource.SelectGroup;
                    return false;
                }
                else
                {
                    if (db.User.ExistEmail(objUser))
                    {
                        strErrorMsg = Resources.Resource.EmailExists;
                        return false;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return true;
        }
        private bool IsValidDataChangePasword(ChangePassword objPassword, ref string strErrorMsg)
        {
            try
            {
                if (CommonFunctions.Encrypt(objPassword.OldPassword.Trim()) != CommonFunctions.Encrypt(User.Password.Trim()))
                {
                    strErrorMsg = Resources.Resource.InvalidOldPassword;
                    return false;
                }
                else if (string.IsNullOrEmpty(objPassword.Password))
                {
                    strErrorMsg = Resources.Resource.EnterNewpaaword;
                    return false;
                }
                else if (string.IsNullOrEmpty(objPassword.ConfirmPassword))
                {
                    strErrorMsg = Resources.Resource.EnterConfirmPassword;
                    return false;
                }
                else if (objPassword.ConfirmPassword != objPassword.Password)
                {
                    strErrorMsg = Resources.Resource.PasswordNotMatch;
                    return false;
                }
                //else if (objPassword.Password.Length < CommonFunctions.MaxPasswordLenght)
                //{
                //    strErrorMsg = Resource.Resource.PassmustbegreaterequalMsg + GoalBase.MaxPasswordLenght + Resource.Resource.characterMsg;
                //    return false;
                //}
            }
            catch (Exception)
            {
                throw;
            }
            return true;
        }

        #endregion
    }
}