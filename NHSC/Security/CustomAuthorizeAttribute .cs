﻿using Newtonsoft.Json;
using NHSC.Security;
using NHSC.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using NHSC.Common;

namespace NHSC.Security
{
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false, Inherited = true)]
    public class CheckSessionOutAttribute : ActionFilterAttribute
    {
        public int Values { get; set; }
        protected virtual CustomPrincipal CurrentUser
        {
            get { return HttpContext.Current.User as CustomPrincipal; }
        }
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            CommonFunctions objCommon = new CommonFunctions();
            var RouteData = HttpContext.Current.Request.RequestContext.RouteData;
            string CurrentAction = RouteData.GetRequiredString("action");
            string CurrentController = RouteData.GetRequiredString("controller");
            HttpContext context = HttpContext.Current;
            if (objCommon.LoginUserDetails == null || objCommon.LoginUserDetails.UserId == 0)
            {

                filterContext.Result = new RedirectToRouteResult(
                                    new System.Web.Routing.RouteValueDictionary {
                            { "Controller", "LogIn" },
                            { "Action", "Index" },
                           { "returnUrl",HttpUtility.UrlEncode(context.Request.RawUrl)}
                        });
            }
            else
            {
                if (objCommon.LoginUserDetails.MenuList != null)
                {
                    if (objCommon.LoginUserDetails.MenuList.Where(x => x.View == true && x.MenuId == Values).Count() == 0)
                    {
                        //filterContext.Result = new RedirectToRouteResult(
                        //              new System.Web.Routing.RouteValueDictionary {
                        //    { "Controller", "Login" },
                        //    { "Action", "Index" },
                        //     { "returnUrl",HttpUtility.UrlEncode(context.Request.RawUrl)}
                        //  });
                        filterContext.Result = new ViewResult
                        {
                            ViewName = "PermissionError"
                        };
                    }
                }
            }
            base.OnActionExecuting(filterContext);
        }
    }
}