﻿using NHSC.Security;
using System.Web;
using System.Web.Mvc;

namespace NHSC.Security
{
    public abstract class BaseViewPage : WebViewPage
    {
        protected virtual new CustomPrincipal User
        {
            get
            {
                return HttpContext.Current.User as CustomPrincipal;
            }
        }
    }

    public abstract class BaseViewPage<TModel> : WebViewPage<TModel>
    {
        protected virtual new CustomPrincipal User
        {
            get
            {
                return HttpContext.Current.User as CustomPrincipal;
            }
        }
    }
}